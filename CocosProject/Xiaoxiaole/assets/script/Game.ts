const {ccclass, property} = cc._decorator;
@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Prefab)
    pic:cc.Prefab=null;
    private arr=[];
    xiaoxiaole(event){
        let Point:cc.Vec2 = event.getLocation()
        //转换后的坐标，局部坐标
        let temp_position=this.node.convertToNodeSpaceAR(Point)
        let x=Math.round((temp_position.x+25)/50)
        let y=Math.round((temp_position.y+25)/50)
        //左下角为(1,1)
        let myPos=cc.v2(x,y)
        // cc.log(myPos.x,myPos.y)
        this.judge(myPos)
        for(let i=1;i<11;i++){
            for(let j=1;j<11;j++){
                if(this.arr[i][j]==null){
                    let n:number=1
                    let a:number=1
                    while(this.arr[i][j+a]==null && j+a<11){
                        //记录空格
                        n++
                        a++
                    }
                    // cc.log(n) 
                    this.scheduleOnce(()=>{
                        for(let k=0;k<(11-n-j);k++){
                            this.node.children[(i-1)*10+j-1+k].getComponent("NewBlock").index
                            =this.node.children[(i-1)*10+j-1+k+n].getComponent("NewBlock").index
                            this.arr[i][j+k]=this.arr[i][j+k+n]
                            this.arr[i][j+k+n]=null
                            this.node.children[(i-1)*10+j-1+k+n].getComponent("NewBlock").index=null
                        } 
                    }, 0.4); 
                    //放开此处break不生成新图片            
                    // break
                    //上面生成新图片
                    setTimeout(function () {
                        for(let k=0;k<n;k++){
                            this.node.children[(i-1)*10+9-k].getComponent("NewBlock").random_block()
                            this.arr[i][10-k]=this.node.children[(i-1)*10+9-k].getComponent("NewBlock").index
                        }
                    }.bind(this),950)    
                    break                
                }
            }
        }
    }
    judge(myPos:cc.Vec2){
        //来个变量记住一开始的图片
        let temp= this.arr[myPos.x][myPos.y]
        //减去1的表示点击的节点，处理数组越界整体加了一
        //this.node.children[(myPos.x-1)*10+myPos.y-1]找到对应孩子节点 执行其函数
        if(temp==null || (this.arr[myPos.x-1][myPos.y]!=temp && this.arr[myPos.x+1][myPos.y]!=temp
            && this.arr[myPos.x][myPos.y-1]!=temp && this.arr[myPos.x][myPos.y+1]!=temp)){
                //上面有一个都不成立
                return    
        }
        this.node.children[(myPos.x-1)*10+myPos.y-1].getComponent("NewBlock").xiaoxiaole()  
        this.arr[myPos.x][myPos.y]=null
        
        if(this.arr[myPos.x-1][myPos.y]==temp){
            this.node.children[(myPos.x-2)*10+myPos.y-1].getComponent("NewBlock").xiaoxiaole()
            this.judge(cc.v2(myPos.x-1,myPos.y)) 
            this.arr[myPos.x-1][myPos.y]=null
        }
        if(this.arr[myPos.x+1][myPos.y]==temp){
            this.node.children[myPos.x*10+myPos.y-1].getComponent("NewBlock").xiaoxiaole()
            this.judge(cc.v2(myPos.x+1,myPos.y))  
            this.arr[myPos.x+1][myPos.y]=null  
        }
        if(this.arr[myPos.x][myPos.y-1]==temp){
            this.node.children[(myPos.x-1)*10+myPos.y-2].getComponent("NewBlock").xiaoxiaole()  
            this.judge(cc.v2(myPos.x,myPos.y-1))   
            this.arr[myPos.x][myPos.y-1]=null 
        }
        if(this.arr[myPos.x][myPos.y+1]==temp){
            this.node.children[(myPos.x-1)*10+myPos.y].getComponent("NewBlock").xiaoxiaole() 
            this.judge(cc.v2(myPos.x,myPos.y+1))   
            this.arr[myPos.x][myPos.y+1]=null
        }
    }
    onLoad () {
        this.initGame()
        this.node.on("mousedown",this.xiaoxiaole,this)
    }
    initGame(){      
        for(let i=0;i<12;i++){
            this.arr[i]=[]
            for(let j=0;j<12;j++){
                this.arr[i][j]=null
            }
        }
        for(let i=1;i<11;i++){
            this.arr[i]=[]
            for(let j=1;j<11;j++){
                let block:cc.Node = cc.instantiate(this.pic)
                this.node.addChild(block)
                // cc.log(block.getComponent("NewBlock").index)
                // cc.log(block.name)
                block.setPosition(cc.v2(50*i-25,50*j-25))
                this.arr[i][j]=block.getComponent("NewBlock").index
            }
        }
    }
    // start () {}
     update (dt) {    
     }
}
