
const {ccclass, property} = cc._decorator;
const picArr=cc.Class({
    name:"pic_List",
    properties:{
        a:{
            default:null,
            type:cc.SpriteFrame
        },
        b:{
            default:null,
            type:cc.SpriteFrame
        },
        c:{
            default:null,
            type:cc.SpriteFrame
        },
        d:{
            default:null,
            type:cc.SpriteFrame
        },
        e:{
            default:null,
            type:cc.SpriteFrame
        },
        f:{
            default:null,
            type:cc.SpriteFrame
        },
    },
});
@ccclass
export default class NewBlock extends cc.Component {
    @property({
        type:picArr,
        displayName:"图片数组"
    })
    pic_List=null;
    index=this.myRandom(0,5)     
    arr=['a','b','c','d','e','f']
    xiaoxiaole(){
        this.index=null
    }
    myRandom(min:number,max:number){
        //向下取整
        return Math.floor(Math.random()*(max-min+1)+min)
    }
    random_block(){
        this.index=this.myRandom(0,5)
    }
    judge_block(){
        if(this.arr[this.index]=='a'){
            this.node.getComponent(cc.Sprite).spriteFrame=this.pic_List.a
        }
        else if(this.arr[this.index]=='b'){
            this.node.getComponent(cc.Sprite).spriteFrame=this.pic_List.b
        }
        else if(this.arr[this.index]=='c'){
            this.node.getComponent(cc.Sprite).spriteFrame=this.pic_List.c
        }
        else if(this.arr[this.index]=='d'){
            this.node.getComponent(cc.Sprite).spriteFrame=this.pic_List.d
        }
        else if(this.arr[this.index]=='e'){
            this.node.getComponent(cc.Sprite).spriteFrame=this.pic_List.e
        }
        else if(this.arr[this.index]=='f'){
            this.node.getComponent(cc.Sprite).spriteFrame=this.pic_List.f
        }
        else if(this.arr[this.index]==null){
            this.node.getComponent(cc.Sprite).spriteFrame=null
        }
    }
    // onLoad () { }
    // start () { }
    update (dt) {
        this.judge_block()
    }
}
