const {ccclass, property} = cc._decorator;
@ccclass
export default class Player extends cc.Component {
     // 主角跳跃高度
     @property(cc.Integer)
     private jumpHeight:number=0;
     // 主角跳跃持续时间
     @property(cc.Integer)
     private jumpDuration:number=0;
     // 最大移动速度
     @property(cc.Integer)
     private maxMoveSpeed:number= 0;
     // 加速度
     @property(cc.Integer)
     private accel:number= 0;
     @property(cc.AudioClip)
     private jumpAudio:cc.AudioClip=null;
     //速度
     private xSpeed:number=0;
     private accLeft:boolean=false;
     private accRight:boolean=false;
     private setJumpAction(){
        // 跳跃上升
        let jumpUp = cc.tween().by(this.jumpDuration, {y: this.jumpHeight}, {easing: 'sineOut'})
        // 下落
        let jumpDown = cc.tween().by(this.jumpDuration, {y: -this.jumpHeight}, {easing: 'sineIn'})
        // 创建一个缓动，按 jumpUp、jumpDown 的顺序执行动作
        let tween = cc.tween()
        .sequence(jumpUp, jumpDown)
        // 添加一个回调函数，在前面的动作都结束时调用我们定义的 playJumpSound() 方法
        .call(this.playJumpSound, this)
        // 不断重复
        return cc.tween().repeatForever(tween)
    }
    onKeyDown (event:cc.Event.EventKeyboard) {
        // set a flag when key pressed
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.accLeft = true
                break
            case cc.macro.KEY.d:
                this.accRight = true
                break
        }
    }
    onKeyUp (event) {
        // unset a flag when key released
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.accLeft = false
                break
            case cc.macro.KEY.d:
                this.accRight = false
                break
        }
    }
    playJumpSound() {
        // 调用声音引擎播放声音
        cc.audioEngine.playEffect(this.jumpAudio, false);
    }
    onLoad() {
        let jumpAction = this.setJumpAction()
        cc.tween(this.node).then(jumpAction).start()
        // 加速度方向开关
        this.accLeft = false
        this.accRight = false
        // 主角当前水平方向速度
        this.xSpeed = 0
        // 初始化键盘输入监听
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)
    }
    start () {}
    update(dt) {
        // 根据当前加速度方向每帧更新速度
        if (this.accLeft) {
            this.xSpeed -= this.accel * dt
        }
        else if (this.accRight) {
            this.xSpeed += this.accel * dt
        }
        // 限制主角的速度不能超过最大值
        if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
            // if speed reach limit, use max speed with current direction
            this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed)
        }
        if(this.node.x<-880){
            //this.xSpeed=-this.xSpeed
            this.xSpeed=20;   
        }
        else if(this.node.x>880){
            this.xSpeed=-20;
        }
        // 根据当前速度更新主角的位置
        this.node.x += this.xSpeed * dt
    }
    onDestroy(){
        // 取消键盘输入监听
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)
    }
}
