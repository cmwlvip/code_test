const {ccclass, property} = cc._decorator;
export class Global{
    static isRestart:boolean=false;
}
@ccclass
export  class Start extends cc.Component {

    playGame(){
        this.node.parent.destroy()  
        cc.director.loadScene("game")
    }
    onLoad () {

        if(Global.isRestart){
            //cc.find("Canvas/startButton/Background/start").getComponent(cc.Label).string="重新开始"
            this.node.getChildByName("Background").getChildByName("start").getComponent(cc.Label).string="重新开始"
        }
        // this.node.on('mousedown',this.playGame,this)
        this.node.on("touchstart",this.playGame,this)
    }

    start () {

    }
    // update (dt) {}
}
