const {ccclass, property} = cc._decorator;
@ccclass
export default class ChessBoard extends cc.Component {
    //使用棋子预制体
    @property(cc.Prefab)
    private chessPrefab_black:cc.Prefab=null
    @property(cc.Prefab)
    private chessPrefab_white:cc.Prefab=null
    //重新开始图片
    @property(cc.SpriteFrame)
    private restart:cc.SpriteFrame=null
    //游戏结算面板
    @property(cc.Prefab)
    private start_board:cc.Prefab=null
    //边界落子提示面板
    @property(cc.Prefab)
    private Tip_board:cc.Prefab=null
    //开始节点
    private startGame:cc.Node=null
    //用于计数
    private num:number=0;
    //保存实例化之后的，不是0的值，0表示没有棋子，1黑棋，2白棋
    private _board=[]
    //戏是否进行游
    private isPlayGame:boolean=false
    //是否重新开始
    private isRestart:boolean=false
    //有18根线，设置21用于处理边界问题
    initBoard(){
        for(let i=0;i<18;i++){
            this._board[i]=[]
            for(let j=0;j<18;j++){
                this._board[i][j]=0
            }
        }   
    }
    playGame(){
        if(this.isRestart){
            cc.director.loadScene("game")
        }
        else{
            this.startGame.active=false
            this.isPlayGame=true
        }
    }
    restartGame(){
        this.startGame.active=true
        this.startGame.getComponent(cc.Sprite).spriteFrame=this.restart
        this.startGame.removeAllChildren();
        this.startGame.scale=0.5
        this.startGame.opacity=200
        this.isRestart=true
    }
    //是否可以下
    isPlayChess(i:number,j:number){
        if(this._board[i][j]!=0){
            cc.log("此处已有棋子")
            return false
        }
        // 边界不落子提示
        if(i==0 || j==0 || i==17 || j==17){
            let tip:cc.Node = cc.instantiate(this.Tip_board)
            this.node.addChild(tip)
            tip.setPosition(cc.v2(425,500))
            setTimeout(function () {
                tip.destroy()
              }.bind(this), 500)
              return false
        }  
        return true
    }
    setNewChess(event){
        cc.log(this.isPlayGame)
        if(!this.isPlayGame){
            return
        }
        let startPoint:cc.Vec2 = event.getLocation()
        //转换后的坐标，局部坐标
        let temp_position=this.node.convertToNodeSpaceAR(startPoint)
        let x=Math.round(temp_position.x/50)
        let y=Math.round(temp_position.y/50)
        //确保棋子下在线上
        let position:cc.Vec2=cc.v2(x*50,y*50)  
        //记录转换坐标（索引）整体加一解决数组越界问题
        //现在边界不落子就不用处理越界，如左下角[0][0]是不下棋的，flag===0
        let myPos=cc.v2(x,y)
        if(this.num%2==0 && this.isPlayChess(myPos.x,myPos.y)){
            let newChess_black:cc.Node = cc.instantiate(this.chessPrefab_black)
            this.node.addChild(newChess_black)
            newChess_black.setPosition(position)  
            this._board[myPos.x][myPos.y]=1
            if(this.judge(1,myPos)){
                cc.log("黑胜")
                let newGame:cc.Node = cc.instantiate(this.start_board)
                this.node.addChild(newGame)
                newGame.children[1].color=cc.Color.BLACK
                //设置面板位置
                newGame.setPosition(cc.v2(425,325)) 
                this.restartGame() 
            }
            this.num+=1
            return
        }
        if(this.num%2==1 && this.isPlayChess(myPos.x,myPos.y)){
            let newChess_white:cc.Node = cc.instantiate(this.chessPrefab_white)
            this.node.addChild(newChess_white)
            newChess_white.setPosition(position)
            this._board[myPos.x][myPos.y]=2
            if(this.judge(2,myPos)){
                cc.log("白胜")                
                let newGame:cc.Node = cc.instantiate(this.start_board)       
                newGame.children[1].getComponent(cc.Label).string="白棋"
                this.node.addChild(newGame)
                newGame.setPosition(cc.v2(425,325))
                this.restartGame() 
            }
            this.num+=1 
            return        
        }            
    }
    //判断函数，用于判读输赢
    judge(flag:number,myPos:cc.Vec2){
        if(this.judge_row(flag,myPos) || this.judge_list(flag,myPos) || this.judge_left(flag,myPos) || this.judge_right(flag,myPos)){
            this.isPlayGame=false
            return true
        }
        return false
    }
     //判断横向
    judge_row(flag:number,myPos:cc.Vec2){
        //用于计数
        let count=0
        let i=1
        //右界已处理
        while(this._board[myPos.x+i][myPos.y]==flag){
            count+=1
            i+=1
        }
        i=1
        while(this._board[myPos.x-i][myPos.y]==flag){
            count+=1
            i+=1
        }
        if(count<4){
            return false
        }
        return true
    }
    //判断纵向
    judge_list(flag:number,myPos:cc.Vec2){
        //用于计数
        let count=0
        let i=1
        //上界已处理
        while(this._board[myPos.x][myPos.y+i]==flag){
            count+=1
            i+=1
        }
        i=1
        while(this._board[myPos.x][myPos.y-i]==flag){
            count+=1
            i+=1
        }
        if(count<4){
            return false
        }
        return true
    }
    //[0][0] [1][1] [2][2] 
    judge_left(flag:number,myPos:cc.Vec2){     
        //用于计数
        let count=0
        let i=1
        //右上脚已经处理
        while(this._board[myPos.x+i][myPos.y+i]==flag){
            count+=1
            i+=1
        }
        i=1
        while(this._board[myPos.x-i][myPos.y-i]==flag){
            count+=1
            i+=1
        }
        if(count<4){
            return false
        }
        return true
    }
    judge_right(flag:number,myPos:cc.Vec2){
        
        //用于计数
        let count=0
        let i=1
        while(this._board[myPos.x-i][myPos.y+i]==flag){
            count+=1
            i+=1
        } 
        i=1
        while(this._board[myPos.x+i][myPos.y-i]==flag){
            count+=1
            i+=1
        }
        if(count<4){
            return false
        }
        return true
    }

    onLoad () {
        this.startGame=cc.find("Canvas/btn_start")
        // this.node.on('mousedown',this.setNewChess,this)
        this.node.on("touchstart",this.setNewChess, this)
    }
    start () {
        this.initBoard()
    }
    // update (dt) {}
}