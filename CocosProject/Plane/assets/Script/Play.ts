const {ccclass, property} = cc._decorator;

@ccclass
export default class Play extends cc.Component {
    //玩家子弹预制体 
    @property(cc.Prefab)
    bullet:cc.Prefab=null;

    playAgain:cc.Node=null;
    // LIFE-CYCLE CALLBACKS:

    move(event){
        let offset = event.getDelta();
        this.node.x += offset.x;
        this.node.y += offset.y;
        if (this.node.x >= 500) {
            this.node.x = 500;
        }
        if (this.node.x <= -500) {
            this.node.x = -500;
        }
        if (this.node.y <= -800) {
            this.node.y = -800;
        }     
    }
    onLoad () {
        //获得触摸移动事件
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.move,this)
        this.playAgain = cc.find("Canvas/main");
        // cc.log(this.playAgain.name)
    }
    // 碰撞 -- 被子弹打中
    onCollisionEnter(other, self) {
        //console.log("===============碰撞成功");
        //停止发射子弹
        this.unscheduleAllCallbacks();
        // 播放死亡动画
        this.node.removeComponent(cc.BoxCollider)
        this.getComponent(cc.Animation).play();
        let myself=this
        this.scheduleOnce(function () {
            myself.node.removeFromParent();
            myself.playAgain.active = true;
        }, 0.65);
    }

    // 发射多枚子弹
    shootMoreBullet() {
        let bullet:cc.Node[] = [];

        for (let i = 0; i < 4; i++) {
            bullet[i] = cc.instantiate(this.bullet);
            this.node.parent.addChild(bullet[i]);
        }

        bullet[0].x = this.node.x;
        bullet[0].y = this.node.y;

        bullet[1].x = this.node.x - 45;
        bullet[1].y = this.node.y;
        bullet[1].getComponent("Bullet").xSpeed = -65;

        bullet[2].x = this.node.x + 45;
        bullet[2].y = this.node.y;
        bullet[2].getComponent("Bullet").xSpeed= 65;

        bullet[3].x = this.node.x;
        bullet[3].y = this.node.y;
        bullet[3].getComponent("Bullet").ySpeed = 1200;
    }
    playShootMoreBullet() {
        this.schedule(this.shootMoreBullet.bind(this), 0.20);
    }
    start () {

    }

    // update (dt) {}
}
