const {ccclass, property} = cc._decorator;
// 玩家子弹对象
@ccclass
export default class Bullet extends cc.Component {

    //子弹发射音效
    @property(cc.AudioClip)
    bulletAudio:cc.AudioClip=null

    xSpeed:number=0;
    ySpeed:number=0;


    // LIFE-CYCLE CALLBACKS:

     // 碰撞事件
     onCollisionEnter(other, self:cc.BoxCollider) {
        this.node.removeFromParent();
        cc.audioEngine.playEffect(this.bulletAudio, false);
    }
    onLoad () {
        this.xSpeed=0
        this.ySpeed=1200
    }
    // 
    start () {

    }
    update (dt) {
        this.node.x += this.xSpeed * dt;
        this.node.y += this.ySpeed * dt;
        
        if (this.node.y >= 960 || this.node.x >= 540 || this.node.x <= -540) {
            this.node.removeFromParent();
            return;
        }
    }
}
