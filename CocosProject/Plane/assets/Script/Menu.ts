const {ccclass, property} = cc._decorator;

export class Global{
    public static maxScore:number=0;
}

@ccclass
export class Menu extends cc.Component {
    //@property

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.find("MaxScore",this.node).getComponent(cc.Label).string="当前最高分："+Global.maxScore;
    }

    startGame(){
        cc.director.loadScene("game");
    }
    start () {

    }

    // update (dt) {}
}

