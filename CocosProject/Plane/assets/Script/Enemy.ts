//敌机脚本
const {ccclass, property} = cc._decorator;

@ccclass
export default class Enemy extends cc.Component {
    @property(cc.SpriteFrame)
    enemy_skin:cc.SpriteFrame[]=[];

    //敌机子弹
    @property(cc.Prefab)
    enemyBullet:cc.Prefab=null;

    //敌机横纵速度
    xSpeed=0;
    ySpeed=0;

    flag=0;

    //游戏场景脚本
    gameScene=null;

    // LIFE-CYCLE CALLBACKS:

    // 随机产生
    setEnemy() {
        let skin_type = Math.random() * this.enemy_skin.length + 1;
        skin_type = Math.floor(skin_type);
        if (skin_type >= this.enemy_skin.length+1) {
            skin_type = this.enemy_skin.length;
        }
        this.getComponent(cc.Sprite).spriteFrame = this.enemy_skin[skin_type - 1];
    }
    //发射子弹
    shoot_enemy_bullet () {
        let enemy_bullet = cc.instantiate(this.enemyBullet);
        this.node.parent.addChild(enemy_bullet);
        enemy_bullet.x = this.node.x;
        enemy_bullet.y = this.node.y;
    }

    shoot_forever () {
        this.schedule(this.shoot_enemy_bullet.bind(this), 0.5)
    }

    //敌机被玩家子弹打中
    onCollisionEnter (other, self) {
        //cc.warn("other.name = ", other.node.name, other.node.group, other.node.groupIndex);
        // 敌机消失动画---删除敌机---添加分数
        this.getComponent(cc.Animation).play();
        this.scheduleOnce(function () {
            this.node.removeFromParent();
        }, 0.25);
        this.gameScene.addScore();
        
    }

    onLoad () {
        this.xSpeed = 0;
        this.ySpeed = -200;
        this.gameScene = cc.find("Canvas").getComponent("Game");
        this.flag = 0;
    }

    start () {
        this.setEnemy();
        this.schedule(this.shoot_enemy_bullet.bind(this), 0.6);
    }

    update (dt) {
        this.node.x += this.xSpeed * dt;
        this.node.y += this.ySpeed * dt;

        if (this.node.y < -1920) {
            this.node.removeFromParent();
        }
    }
}


    
   

