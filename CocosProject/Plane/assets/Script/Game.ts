//场景脚本
const {ccclass, property} = cc._decorator;
import { Global } from "./Menu";
@ccclass
export default class Game extends cc.Component {

    // 敌机组预制体
    @property(cc.Prefab)
    groups:cc.Prefab[]=[];

    // 打开物理碰撞区域调试开关
    @property(cc.Boolean)
    is_debug:boolean=false;    
    // 分数和级别
    @property(cc.Label)
    scoreLabel: cc.Label=null;       // 分数

    @property(cc.Label)
    levelLabel: cc.Label=null;       // 级别

    gameLevel=1;

    //玩家脚本
    player=null;
    playagain:cc.Node=null;
    
    //分数
    killNum:number;
    lv:number;

    // LIFE-CYCLE CALLBACKS:

     // 点击重玩按钮
     playAgain() {
        cc.director.loadScene("game");
    }
    //返回主菜单
    returnMenu(){
        cc.director.loadScene("start")
    }
    // 随机&无限的产生一组敌人
    randomGroup() {
        let groupType = Math.random() * this.groups.length + 1;
        groupType = Math.floor(groupType);

        if (groupType >= this.groups.length) {
            groupType = this.groups.length;
        }
        

        try{
            let g:cc.Node = cc.instantiate(this.groups[groupType- 1])
            this.node.addChild(g)
            let x = Math.random() * 800-540;
            let y = (Math.random()) * 300 + 700;
            g.setPosition(cc.v2(x,y))
        }
        catch{
            cc.log("出错了")
        }
        

        //[2, 4) 秒时间间隔
        this.scheduleOnce(this.randomGroup.bind(this), Math.random() * 2 + 2);
    }
    // 击杀一个玩家得一分
    addScore() {
        this.killNum ++;
        this.scoreLabel.string = "分数：" + this.killNum;
        if (this.killNum >= this.lv) {
            this.lv += 20;    // 击杀的玩家越多， 等级越高

            // 击杀的玩家越多， 等级越高
            this.gameLevel ++;
            this.levelLabel.string = "等级：" + this.gameLevel;
        }
    }

    onLoad () {
        // 使能物理碰撞
        let manager = cc.director.getCollisionManager();
        manager.enabled = true; // 开启碰撞
        if (this.is_debug) {
            manager.enabledDebugDraw = true; // 调试绘制状态
        }
        this.killNum = 0;
        this.lv = 25;
        this.player = cc.find("Canvas/ship").getComponent("Play");
        this.playagain = cc.find("main", this.node);
        this.playagain.zIndex = 100;//拥有更高 zIndex 的节点将被排在后面
        this.playagain.active = false;
    }


    start () {
        //随机产生一组敌人
        this.randomGroup();
        // 玩家发射子弹
        this.player.playShootMoreBullet();
    }

    update (dt) {
        if(this.killNum>Global.maxScore){
            Global.maxScore=this.killNum
            let temp:cc.Node=new cc.Node()
            let str:cc.Label=temp.addComponent(cc.Label)
            str.string="恭喜你打破记录！"
            str.fontSize=80
            str.lineHeight=80
            this.node.addChild(temp)
            temp.setPosition(0,500)
            this.schedule(()=>{
                temp.destroy()
            },1);
        }
    }
}

