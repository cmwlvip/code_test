// 敌人发射的子弹
const {ccclass, property} = cc._decorator;

@ccclass
export default class enemyBullet extends cc.Component {
    @property(cc.AudioClip)
    bulletAudio:cc.AudioClip=null;

    xSpeed:number=0;
    ySpeed:number=0;


    // LIFE-CYCLE CALLBACKS:

    onCollisionEnter(other, self) {
        this.node.removeFromParent();
        cc.audioEngine.playEffect(this.bulletAudio, false);
    }

    onLoad () {
        this.xSpeed=0;
        this.ySpeed=-700;
    }

    start () {

    }

    update (dt) {
        this.node.x += this.xSpeed * dt;
        this.node.y += this.ySpeed* dt;
        //console.log(this.node.x, this.node.y);
        if (this.node.y < -1920) {
            this.node.removeFromParent();
        }
    }
}
