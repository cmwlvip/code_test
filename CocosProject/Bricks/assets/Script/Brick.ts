const {ccclass, property} = cc._decorator;
@ccclass
export default class Brick extends cc.Component {
    onLoad () {
        let red=Math.random()*255
        let green =Math.random()*255
        let blue =Math.random()*255
        let color=cc.color(red,green,blue)
        this.node.color=color
    }
    // start () {}
    // update (dt) {}
}