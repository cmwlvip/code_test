const {ccclass, property} = cc._decorator;
@ccclass
export default class Game extends cc.Component {
    @property(cc.Prefab)
    brick:cc.Prefab=null;
    @property(cc.Node)
    player:cc.Node=null;
    Left:boolean=false;
    Right:boolean=false;
    xSpeed:number=11;
    initGame(){
        for(let i=0;i<9;i++){
            for(let j=0;j<15;j++){
                let node:cc.Node=cc.instantiate(this.brick)
                this.node.addChild(node)
                node.setPosition(cc.v2(-480+i*120,800-j*60))
            }
        }
    }
    onKeyDown (event:cc.Event.EventKeyboard) {
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.Left = true
                break
            case cc.macro.KEY.d:
                this.Right = true
                break
        }
    }
    onKeyUp (event) {
        switch(event.keyCode) {
            case cc.macro.KEY.a:
                this.Left = false
                break
            case cc.macro.KEY.d:
                this.Right = false
                break
        }
    }
    move(event){
        let offset = event.getDelta();
        this.player.x += offset.x;
        if(this.player.x<-450){
            this.player.x=-445
        }else if(this.player.x>450){
            this.player.x=445
        }  
    }
    onLoad () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.move,this)
        this.initGame()
        let node=new cc.Node()
        node.color=cc.Color.BLACK
        let label=node.addComponent(cc.Label)
        label.string="调整球位置后\n按下空格(或者点击小球)\n开始消除砖块"
        label.fontSize=70
        label.lineHeight=70
        this.node.parent.addChild(node)
        node.setPosition(0,-200)
        setTimeout(() => {
            node.destroy()
        }, 2000);
    }
    // start () {}
    update (dt) {
        if (this.Left) {
        this.player.x-= dt*this.xSpeed*100
        }
        if (this.Right) {
        this.player.x+=dt*this.xSpeed*100
        }
        if(this.player.x<-450){
            this.player.x=-445
        }else if(this.player.x>450){
            this.player.x=445
        }
    }
}