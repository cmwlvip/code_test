const {ccclass, property} = cc._decorator;
@ccclass
export default class NewClass extends cc.Component {
    //通关提示
    @property(cc.SpriteFrame)
    win:cc.SpriteFrame=null;
    //得分面板
    @property(cc.Node)
    scoreBoard:cc.Node=null;
    //等级面板
    @property(cc.Node)
    levelBoard:cc.Node=null;
    //重新开始面板
    restart:cc.Node=null;
    xSpeed:number=0;
    ySpeed:number=0;
    score:number=0;
    level:number=0;
    gameLevel:number=0;
    isLunch:boolean=false;
    judge(){
        let tempNode:cc.Node=cc.find("Canvas/bg")
        for(let i=0;i<tempNode.children.length;i++){
            if(tempNode.children[i].name=="brick"){
                return false
            }
        }
        return true
    }
    restartGame(){
        cc.director.loadScene("game")
    }
    fire(event){
        switch(event.keyCode) {
            case cc.macro.KEY.space:
                this.isLunch=true
                this.changeLevel()
                break
        }  
    }
    changeLevel(){
        if(this.score>=this.level){
            this.level+=10+this.gameLevel*10;
            this.gameLevel+=1;
            this.levelBoard.getComponent(cc.Label).string="等级：" + this.gameLevel.toString() 
            switch(this.gameLevel){
                case 1:
                    this.xSpeed=(this.xSpeed>=0)?3:-3
                    this.ySpeed=(this.ySpeed>=0)?7:-7
                    break
                case 2:
                    this.xSpeed=(this.xSpeed>0)?4:-4
                    this.ySpeed=(this.ySpeed>0)?9:-9
                    break
                case 3:
                    this.xSpeed=(this.xSpeed>0)?4:-4
                    this.ySpeed=(this.ySpeed>0)?10:-10
                    break
                case 4:
                    this.xSpeed=(this.xSpeed>0)?5:-5
                    this.ySpeed=(this.ySpeed>0)?11:-11
                    break
                case 5:
                    this.xSpeed=(this.xSpeed>0)?5:-5
                    this.ySpeed=(this.ySpeed>0)?13:-13
                case 6:
                    this.xSpeed=(this.xSpeed>0)?6:-6
                    this.ySpeed=(this.ySpeed>0)?15:-15
                case 7:
                    this.xSpeed=(this.xSpeed>0)?6:-6
                    this.ySpeed=(this.ySpeed>0)?17:-17
                default:
                    break
            }
        }   
    }
    onLoad () {
        let manager = cc.director.getCollisionManager()
        manager.enabled=true
        manager.enabledDebugDraw=true
        this.restart=cc.find("Canvas/restart")
        this.restart.active=false
        this.node.getComponent(cc.AudioSource).preload=true
        this.node.on("touchstart",()=>{this.isLunch=true;this.changeLevel()},this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.fire, this)
    }
    onCollisionEnter(other,self){
        if(other.tag==1){
            this.ySpeed=-Math.abs(this.ySpeed)
        }
        else if(other.tag==5){
            this.ySpeed=Math.abs(this.ySpeed)
        }
        else if(other.tag==2){
            this.xSpeed=-this.xSpeed
        }
        else if(other.tag==3){
            this.xSpeed=-this.xSpeed
        }
        else if(other.tag==4){
            this.ySpeed=-this.ySpeed
        }
    }
    onCollisionExit(other, self) {
        if(other.tag==1 || other.tag==2 || other.tag==5){
            other.node.destroy()
            this.score+=1
            this.scoreBoard.getComponent(cc.Label).string="得分：" + this.score.toString()     
            this.node.getComponent(cc.AudioSource).play()
            this.changeLevel()
        }
    }
    // start () {}
    update (dt) {
        if(this.judge()){
            this.restart.getChildByName("tip").getComponent(cc.Sprite).spriteFrame=this.win
            this.restart.active=true
        }
        this.node.x+=this.xSpeed*dt*100
        this.node.y+=this.ySpeed*dt*100
        if(this.node.y<-960){
            this.restart.active=true
        }
        if(!this.isLunch){
            this.node.x=cc.find("Canvas/bg/board").x
        }
        if(this.node.x>530){
            this.node.x=514
        }
        else if(this.node.x<-530){
            this.node.x=-514
        }
    }
       
}