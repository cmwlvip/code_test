use wordCount;

-- 文件预加载
create table textLines
(
    text string comment "文本行"
);
drop table textLines;

create table wordCount.count
(
    word string comment "单词"
);

drop table count;

-- 加载数据到textLines中
load data local inpath '/root/data/LICENSE.txt' into table textLines;

select * from textLines;

-- split(text," |,|\\.|;|\""有效测试
insert overwrite  table  count  select explode(split(text," |,|\\.|;|\"")) as word from textLines;
insert overwrite  table  count  select explode(split(text,'[ ,\:/.;"()]')) as word from textLines;
-- regexp_replace(text, '[^\\w\\s]+', '') regexp_replace(text, '\\w+', lower)
insert overwrite table count select  explode(split(regexp_replace(lower(text), '[^\\w\\s]+', ' '),' ')) as word from textLines;

select * from count;

select word, count(*) as num from count where word<>'' group by word order by num desc;

show functions;
describe function extended explode;