create database traffic;

use traffic;

show tables;

-- 卡口表
-- 道路编号,设备编号,设备名称,部门编码,部门名称,x坐标,y坐标,启用时间,,
create table transport(
    roadId string comment "道路编号",
    equipmentId string comment "设备编号",
    equipmentName string comment "设备名称",
    partId string comment "部门编码",
    partName string comment  "部门名称",
    x double comment "x坐标",
    y double comment "y坐标",
    startTime date comment "启用时间",
    other string comment "其他"
)
row format delimited
fields terminated by ",";

select * from transport limit 10;

drop table transport_occasionally;
-- 偶尔通行车辆分析
create table transport_occasionally(
    roadId string comment "道路编号",
    num integer comment "计数"
)
row format delimited
fields terminated by ",";

select * from transport_occasionally limit 5;

-- 创建可疑车辆搜索结果表
create table transport_search(
    carId string comment "车牌号",
    passTime string comment "过车时间",
    equipmentId string comment "设备编号"
)
row format delimited
fields terminated by "\t";

select * from transport_search limit 10;

select substr("201909010000",1,8);
drop table traffic.transport_search_result;

-- 创建可疑车辆搜索结果表20290901
create table transport_search_result(
    carId string comment "车牌号",
    passTime string comment "过车时间",
    equipmentId string comment "设备编号"
)
row format delimited
fields terminated by "\t";
-- 筛选9月1日的车辆
insert into table transport_search_result
select * from traffic.transport_search where substr(passtime,1,8)='20190901';


create table transport_search_result_new
as
select carId,from_unixtime(unix_timestamp(passTime,'yyyyMMddHHmmss')) newtime,transport_search_result.equipmentId,x,y
from transport_search_result left join transport on transport_search_result.equipmentId=transport.equipmentId;

select * from transport_search_result_new;

-- 时间段过车量分析
create table transport_sum(
    equipmentId string comment "卡口编号",
    passDate string comment "过车时间日期",
    timeSection string comment "时间段",
    sum integer comment "数量"
)
row format delimited
fields terminated by "\t";

select * from transport_sum limit 10;

-- 流量分析
create table transport_daytimeComposition(
    day string comment "日期",
    hour string comment "小时",
    category string comment "类型",
    sum integer comment "计数"
)
row format delimited
fields terminated by ",";

select * from transport_daytimeComposition limit 5;