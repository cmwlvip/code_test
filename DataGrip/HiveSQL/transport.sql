create database transport;

use transport;
show tables;

drop table transport;

-- 道路编号,设备编号,设备名称,部门编码,部门名称,x坐标,y坐标,启用时间,,
create table transport(
    roadId string comment "道路编号",
    equipmentId string comment "设备编号",
    equipmentName string comment "设备名称",
    partId string comment "部门编码",
    partName string comment  "部门名称",
    x double comment "x坐标",
    y double comment "y坐标",
    startTime date comment "启用时间",
    other string comment "其他"
)
row format delimited
fields terminated by ",";

drop  table transport_search;
create table transport_search(
    carId string comment "车牌号",
    passTime string comment "过车时间",
    equipmentId string comment "设备编号"
)
row format delimited
fields terminated by "\t";

load data inpath '/outdata/part-r-00000' into table transport_search;

select from_unixtime(unix_timestamp(passTime,'yyyyMMddHHmmss')) from transport_search limit 10;

drop table  transport_search_result;

create table transport_search_result
as
select carId,from_unixtime(unix_timestamp(passTime,'yyyyMMddHHmmss')),transport_search.equipmentId,x,y
from transport_search,transport where transport_search.carId='川A7J52T' and transport_search.equipmentId=transport.equipmentId;

select * from transport_search_result;

create table transport_search_resultall
as
select carId,from_unixtime(unix_timestamp(passTime,'yyyyMMddHHmmss')),transport_search.equipmentId,x,y
from transport_search,transport where transport_search.equipmentId=transport.equipmentId;



create table test(
    partName string comment  "部门名称",
    year string comment "年",
    num double comment "占比"
)
row format delimited
fields terminated by ",";

load data inpath '/transport/transport.csv' into table transport;
load data local inpath '/root/data/transport.csv' into table transport;
select * from transport;

drop table test;

select * from test;

select * from test order by value desc;

select * from test1;




