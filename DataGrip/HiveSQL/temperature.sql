use weather;

show tables;

select * from weatherOne where recordDate like "201801%" and temperature=-452;

create table weather.weatherOne
(
    USAF string comment "观测点标识",
    recordDate string comment "日期",
    recordTime        string comment "时间",
    latitude    int comment "纬度",
    longitude   int comment "经度",
    elevation   int comment "海拔",
    temperature int comment "空气温度",
    pressure    int comment "大气压"
)
row format delimited
fields terminated by "\t";

desc formatted weatherOne;

drop table weatherOne;

select * from weatherOne;

select * from weatherOne where recordDate like "201802%";

create table weatherPartition
(
    USAF string comment "观测点标识",
    recordDate string comment "日期",
    recordTime        string comment "时间",
    latitude    int comment "纬度",
    longitude   int comment "经度",
    elevation   int comment "海拔",
    temperature int comment "空气温度",
    pressure    int comment "大气压"
)
partitioned by (datePartition string)
row format delimited
fields terminated by "\t";

alter table weatherPartition add
partition (datePartition == '201801')
partition (datePartition == '201802')
partition (datePartition == '201803')
partition (datePartition == '201804')
partition (datePartition == '201805')
partition (datePartition == '201806')
partition (datePartition == '201807')
partition (datePartition == '201808')
partition (datePartition == '201809')
partition (datePartition == '201810')
partition (datePartition == '201811')
partition (datePartition == '201812');


load data  inpath '/outdata/weather/part-r-00000' into table weatherOne;

select USAF,recordDate,temperature/10 from weatherone where temperature<>9999 order by temperature desc limit 100;


insert into table weatherPartition partition(datePartition=='201802') select * from weatherOne where recordDate like "201802%";

select * from weatherPartition where datePartition='201801';

drop table weatherPartition;

create table weather.weatherTwo
(
    USAF        string comment "观测点标识",
    longitude   int comment "经度",
    latitude    int comment "纬度",
    maxTemperature int comment "最高温",
    maxTemperatureDate string comment "最高温日期",
    minTemperature int comment "最低温",
    minTemperatureDate string comment "最低温日期",
    avgTemperature int comment "平均温度",
    num int comment "全年观测次数"
)
row format delimited
fields terminated by "\t";

select * from  weatherTwo;

select USAF,num from weatherTwo order by num desc;

select  longitude,latitude,maxTemperature,maxTemperatureDate from weatherTwo order by  maxTemperature desc limit 10;

show databases ;