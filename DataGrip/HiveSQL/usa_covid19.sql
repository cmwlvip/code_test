use test;

--创建表t_usa_covid19
drop table if exists t_usa_covid19;

CREATE TABLE t_usa_covid19(
    count_date string comment "日期",
    county string comment "县",
    state string comment "洲",
    fips int comment "编码",
    cases int comment "病例",
    deaths int comment "死亡病例"
)
row format delimited fields terminated by ",";

--将源数据load加载到t_usa_covid19表对应的路径下
load data local inpath '/root/data/us-covid19-counties.dat' into table t_usa_covid19;

--1、select_expr
--查询所有字段或者指定字段
select * from t_usa_covid19;
select county, cases, deaths from t_usa_covid19;
--查询当前数据库
select current_database(); --省去from关键字

--2、ALL DISTINCT
--返回所有匹配的行
select state from t_usa_covid19;
--相当于
select all state from t_usa_covid19;
--返回所有匹配的行去除重复的结果
select distinct state from t_usa_covid19;
--多个字段distinct 整体（字段都一样）去重
select distinct county,state from t_usa_covid19;

--3、WHERE
select * from t_usa_covid19 where 1 > 2; --1 > 2 返回false
select * from t_usa_covid19 where 1 = 1; --1 = 1 返回true
--找出来自于California州的疫情数据
select * from t_usa_covid19 where state = "California";
--where条件中使用函数找出州名字母长度超过10位的有哪些
select * from t_usa_covid19 where length(state) >10 ;
--注意：where条件中不能使用聚合函数
--报错SemanticException:Not yet supported place for UDAF ‘sum'
--聚合函数要使用它的前提是结果集已经确定。
--而where子句还处于“确定”结果集的过程中，因而不能使用聚合函数。
--select state,sum(deaths) from t_usa_covid19 where sum(deaths) >100 group by state; --报错
--可以使用Having实现
select state,sum(deaths) from t_usa_covid19 group by state having sum(deaths) > 100;

--4、聚合操作
--统计美国总共有多少个县county
select count(county) from t_usa_covid19;
--统计美国加州有多少个县
select count(county) from t_usa_covid19 where state = "California";
--统计德州总死亡病例数
select sum(deaths) from t_usa_covid19 where state = "Texas";
--统计出美国最高确诊病例数是哪个县
select max(cases) from t_usa_covid19;

--5、GROUP BY
--根据state州进行分组统计每个州有多少个县county
select count(county) from t_usa_covid19 where count_date = "2021-01-28" group by state;
--想看一下统计的结果是属于哪一个州的
select state,count(county) from t_usa_covid19 where count_date = "2021-01-28" group by state;
--再想看一下每个县的死亡病例数，我们猜想很简单呀把deaths字段加上返回真实情况如何呢？
select state,count(county),deaths from t_usa_covid19 where count_date = "2021-01-28" group by state;
--很尴尬sql报错了org.apache.hadoop.hive.ql.parse.SemanticException:Line 1:27 Expression not in GROUP BY key 'deaths'--为什么会报错？？group by的语法限制
select * from t_usa_covid19 group by county; --报错
--结论：出现在GROUP BY中select_expr的字段：要么是GROUP BY分组的字段；要么是被聚合函数应用的字段。
--deaths不是分组字段报错
--state是分组字段可以直接出现在select_expr中

--被聚合函数应用
select state,count(county),sum(deaths) from t_usa_covid19 where count_date = "2021-01-28" group by state;

--6、having
--统计2021-01-28死亡病例数大于10000的州
select state,sum(deaths) from t_usa_covid19 where count_date = "2021-01-28" and sum(deaths) >10000 group by state; --报错
--where语句中不能使用聚合函数语法报错
--先where分组前过滤，再进行group by分组，分组后每个分组结果集确定再使用having过滤
select state,sum(deaths) from t_usa_covid19 where count_date = "2021-01-28" group by state having sum(deaths) > 10000;
--这样写更好即在group by的时候聚合函数已经作用得出结果having直接引用结果过滤不需要再单独计算一次了
select state,sum(deaths) as cnts from t_usa_covid19 where count_date = "2021-01-28" group by state having cnts> 10000;

--7、order by
--根据确诊病例数升序排序查询返回结果
select * from t_usa_covid19 order by cases;
--不写排序规则默认就是asc升序
select * from t_usa_covid19 order by cases asc;
--根据死亡病例数倒序排序查询返回加州每个县的结果
select * from t_usa_covid19 where state = "California" order by cases desc;

--8、limit
--没有限制返回2021.1.28 加州的所有记录
select * from t_usa_covid19 where count_date = "2021-01-28" and state ="California";
--返回结果集的前5条
select * from t_usa_covid19 where count_date = "2021-01-28" and state ="California" limit 5;
--返回结果集从第3行开始共3行
select * from t_usa_covid19 where count_date = "2021-01-28" and state ="California" limit 2,3;
--注意第一个参数偏移量是从0开始的

--执行顺序
select state,sum(deaths) as cnts from t_usa_covid19 where count_date = "2021-01-28" group by state having cnts> 10000 limit 2;