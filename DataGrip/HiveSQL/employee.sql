--table1: 员工表
CREATE TABLE employee
(
    id int comment "员工编号",
    name string comment "名字",
    deg string comment "职位",
    salary int comment "薪水",
    dept string comment "部门"
)
row format delimited
fields terminated by ',';
--table2:员工家庭住址信息表
CREATE TABLE employee_address
(
    id int comment "员工编号",
    hno string comment "地址编号",
    street string comment "街道",
    city string comment "城市"
)
row format delimited
fields terminated by ',';
--table3:员工联系方式信息表
CREATE TABLE employee_connection
(
    id int comment "员工编号",
    phno string comment "手机号",
    email string comment "电子邮件"
)
row format delimited
fields terminated by ',';

--加载数据到表中
load data local inpath '/root/hivedata/employee.txt' into table employee;
load data local inpath '/root/hivedata/employee_address.txt' into table employee_address;
load data local inpath '/root/hivedata/employee_connection.txt' into table employee_connection;

--1、inner join
select e.id,e.name,e_a.city,e_a.street
from employee e inner join employee_address e_a
on e.id =e_a.id;
--等价于inner join=join
select e.id,e.name,e_a.city,e_a.street
from employee e join employee_address e_a
on e.id =e_a.id;

--等价于隐式连接表示法
select e.id,e.name,e_a.city,e_a.street from employee e , employee_address e_a where e.id =e_a.id;

--2、left join
select e.id,e.name,e_conn.phno,e_conn.email
from employee e left join employee_connection e_conn
on e.id =e_conn.id;
--等价于left outer join
select e.id,e.name,e_conn.phno,e_conn.email
from employee e left outer join employee_connection e_conn
on e.id =e_conn.id;