create database if not exists test;

use test;

--建表
--建表student_local 用于演示从本地加载数据
create table student_local
(
    num int,
    name string,
    sex string,
    age int,
    dept string
)
row format delimited
fields terminated by ',';

--建表student_HDFS 用于演示从HDFS加载数据
create external table student_HDFS
(
    num int,
    name string,
    sex string,
    age int,
    dept string
)
row format delimited
fields terminated by ',';

-- 建议使用beeline客户端可以显示出加载过程日志信息
-- 加载数据
--
-- 从本地加载数据数据位于（node1）本地文件系统本质是hadoop fs -put上传操作
load data local inpath '/root/data/students.txt' into table student_local;

--从HDFS加载数据数据位于HDFS文件系统根目录下本质是hadoop fs -mv 移动操作
--先把数据上传到HDFS上 hadoop fs -put /root/data/students.txt /
load data inpath '/students.txt' into table student_HDFS;

select * from student_local;
select * from student_hdfs;

--创建一张目标表只有两个字段
create table student_from_insert(sno int,sname string);
--使用insert+select插入数据到新表中
insert into table student_from_insert select num,name from student_local;

select *from student_from_insert;

