import numpy as np
import cv2
from matplotlib import pyplot as plt

# 读取图像
img = cv2.imread('./resource/test.jpg', 0)


# 理想低通滤波器
def ideal_lowpass_filter(image, d):
    # 获取图像大小
    M, N = image.shape
    # 构建理想低通滤波器模板
    h = np.zeros((M, N))
    for i in range(M):
        for j in range(N):
            if np.sqrt((i - M / 2) ** 2 + (j - N / 2) ** 2) <= d:
                h[i, j] = 1
    # 傅里叶变换
    f = np.fft.fft2(image)
    # 中心化零频分量
    fshift = np.fft.fftshift(f)
    # 滤波
    g = fshift * h
    # 反傅里叶变换
    ig = np.fft.ifft2(np.fft.ifftshift(g)).real
    return ig


# 高斯低通滤波器
def gaussian_lowpass_filter(image, d):
    # 获取图像大小
    M, N = image.shape
    # 构建高斯低通滤波器模板
    h = np.zeros((M, N))
    for i in range(M):
        for j in range(N):
            h[i, j] = np.exp(-((i - M / 2) ** 2 + (j - N / 2) ** 2) / (2 * d ** 2))
    # 傅里叶变换
    f = np.fft.fft2(image)
    # 中心化零频分量
    fshift = np.fft.fftshift(f)
    # 滤波
    g = fshift * h
    # 反傅里叶变换
    ig = np.fft.ifft2(np.fft.ifftshift(g)).real
    return ig


# 理想高通滤波器
def ideal_highpass_filter(image, d):
    # 获取图像大小
    M, N = image.shape
    # 构建理想高通滤波器模板
    h = np.ones((M, N))
    for i in range(M):
        for j in range(N):
            if np.sqrt((i - M / 2) ** 2 + (j - N / 2) ** 2) <= d:
                h[i, j] = 0
    # 傅里叶变换
    f = np.fft.fft2(image)
    # 中心化零频分量
    fshift = np.fft.fftshift(f)
    # 滤波
    g = fshift * h
    # 反傅里叶变换
    ig = np.fft.ifft2(np.fft.ifftshift(g)).real
    return ig


# 高斯高通滤波器
def gaussian_highpass_filter(image, d):
    # 获取图像大小
    M, N = image.shape
    # 构建高斯高通滤波器模板
    h = np.zeros((M, N))
    for i in range(M):
        for j in range(N):
            h[i, j] = 1 - np.exp(-((i - M / 2) ** 2 + (j - N / 2) ** 2) / (2 * d ** 2))
    # 傅里叶变换
    f = np.fft.fft2(image)
    # 中心化零频分量
    fshift = np.fft.fftshift(f)
    # 滤波
    g = fshift * h
    # 反傅里叶变换
    ig = np.fft.ifft2(np.fft.ifftshift(g)).real
    return ig


# 低通滤波
ideal_lp_img = ideal_lowpass_filter(img, 30)
gaussian_lp_img = gaussian_lowpass_filter(img, 30)

# 高通滤波
ideal_hp_img = ideal_highpass_filter(img, 30)
gaussian_hp_img = gaussian_highpass_filter(img, 30)

# 显示图像
plt.subplot(2, 3, 1), plt.imshow(img, cmap='gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 2), plt.imshow(ideal_lp_img, cmap='gray')
plt.title('Ideal Lowpass Filtered Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 3), plt.imshow(gaussian_lp_img, cmap='gray')
plt.title('Gaussian Lowpass Filtered Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 4), plt.imshow(img, cmap='gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 5), plt.imshow(ideal_hp_img, cmap='gray')
plt.title('Ideal Highpass Filtered Image'), plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 6), plt.imshow(gaussian_hp_img, cmap='gray')
plt.title('Gaussian Highpass Filtered Image'), plt.xticks([]), plt.yticks([])
plt.show()