with open('E:/part-r-00000', 'r', encoding='utf-8') as f1, \
        open('E:/part-r-000001', 'r', encoding='utf-8') as f2, \
        open('H:/out', 'w', encoding='utf-8') as out:
    # 从两个输入文件中逐行读取数据
    line1 = f1.readline()
    line2 = f2.readline()
    num = 0

    # 循环比较并写入结果
    while line1 and line2:
        time1 = int(line1.split('\t')[0])
        time2 = int(line2.split('\t')[0])

        if time1 <= time2:
            out.write(line1)
            line1 = f1.readline()
        else:
            out.write(line2)
            line2 = f2.readline()
        num += 1
        if num % 500000 == 0:
            print("已写", num, "条数据")

    # 处理剩余的记录
    while line1:
        out.write(line1)
        line1 = f1.readline()

    while line2:
        out.write(line2)
        line2 = f2.readline()
