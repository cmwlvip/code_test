from PIL import Image

# 打开要调整大小的图像
img = Image.open('./resource/testImg.jpg')

# 获取原始图像尺寸
width, height = img.size

# 如果最长边超过1000像素，则进行缩放
if max(width, height) > 1000:
    ratio = 1000 / max(width, height)
    width = int(width * ratio)
    height = int(height * ratio)
    img = img.resize((width, height))

# 保存调整后的图像
img.save('./resource/resized_image.jpg')

