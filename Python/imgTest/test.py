from elasticsearch import Elasticsearch

# 连接到Elasticsearch集群
es = Elasticsearch([{'host': 'localhost', 'port': 9200, 'scheme': 'http'}])

# # Elasticsearch配置
# es_config = {'host': 'localhost', 'port': 9200}

# 数据源配置
data_file_path = 'D:/0000000/000000_0/000000_0'

# 添加一条记录到索引
index_name = 'vehicle-index'

arr = []
# vehicle_id表示车辆的唯一标识符，checkpoint_id表示卡口的唯一标识符，timestamp表示车辆经过该卡口的时间戳（过车时间）
with open(data_file_path, 'r', encoding='utf-8') as f:
    for line in f:
        arr = line.strip().split('\001')
        if arr[9] == "":
            arr[9] = arr[12]  # 后车牌号没有，就找前车牌号
        data = {
            'checkpoint_id': arr[4],  # 道路编号
            'timestamp': arr[7],  # 过车时间
            'vehicle_id': arr[9],  # 车牌号
        }
        es.index(index=index_name, document=data)
        print(data, " 添加成功！")

# # 检查是否连接
# is_connected = es.ping()
# if is_connected:
#     print('Connected to Elasticsearch cluster')
# else:
#     print('Could not connect to Elasticsearch')
