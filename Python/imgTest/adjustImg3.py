import cv2
import numpy as np

# 读取图像
img = cv2.imread('./resource/resized_image2.jpg')

# 定义3x3和7x7的滤波器
kernel_3 = np.ones((3, 3), np.float32) / 9
kernel_7 = np.ones((7, 7), np.float32) / 49

# 进行均值滤波
mean_3 = cv2.filter2D(img, -1, kernel_3)
mean_7 = cv2.filter2D(img, -1, kernel_7)

# 进行中值滤波
median_3 = cv2.medianBlur(img, 3)
median_7 = cv2.medianBlur(img, 7)

# 显示处理后的图像
cv2.imshow('Mean Filter (3x3)', mean_3)
cv2.imshow('Mean Filter (7x7)', mean_7)
cv2.imshow('Median Filter (3x3)', median_3)
cv2.imshow('Median Filter (7x7)', median_7)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.waitKey(0)
cv2.destroyAllWindows()

# 保存调整后的图像
cv2.imwrite('./resource/mean_3.jpg', mean_3)
cv2.imwrite('./resource/mean_7.jpg.jpg', mean_7)
cv2.imwrite('./resource/median_3.jpg', median_3)
cv2.imwrite('./resource/median_7.jpg.jpg', median_7)
