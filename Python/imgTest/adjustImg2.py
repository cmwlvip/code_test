from PIL import Image, ImageOps

# 读取彩色图片
img = Image.open('./resource/resized_image.jpg')

# 将图像转换为YCbCr颜色空间
ycbcr = img.convert('YCbCr')

# 对亮度通道进行直方图均衡化
y, cb, cr = ycbcr.split()
y = ImageOps.equalize(y)
ycbcr = Image.merge('YCbCr', (y, cb, cr))

# 将图像转换回RGB颜色空间
result = ycbcr.convert('RGB')

# 显示处理后的图像
result.show()

# 保存调整后的图像
result.save('./resource/resized_image2.jpg')
