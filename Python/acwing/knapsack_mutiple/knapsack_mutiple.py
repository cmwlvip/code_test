N,V=list(map(int,input().split()))
v=[0 for i in range(N+1)]
w=[0 for i in range(N+1)]
s=[0 for i in range(N+1)]

for i in range(1,N+1):
    v[i],w[i],s[i]=list(map(int,input().split()))

cell=[[0]*(V+1) for i in range(N+1)]

def complete_knapsack(i,j):
    if(j>=v[i] and (cell[i][j-v[i]]+w[i]>cell[i-1][j])):
        cell[i][j]=cell[i][j-v[i]]+w[i]
    else:
        cell[i][j]=cell[i-1][j]
def findMax(i,j):
    max=cell[i-1][j]
    for k in range(s[i]+1):
        if( j>=k*v[i] and cell[i-1][j-k*v[i]]+k*w[i]>max):
            max=cell[i-1][j-k*v[i]]+k*w[i]
    cell[i][j]=max

for i in range(1,N+1):
    for j in range(1,V+1):
        if(s[i]*v[i]>=V):
            #完全背包问题
            complete_knapsack(i,j)
        else:
            #不完全背包问题
            findMax(i,j)
print(cell)
