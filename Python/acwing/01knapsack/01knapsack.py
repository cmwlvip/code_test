N,V=list(map(int,input().split()))
things=[[0]*2 for i in range(N+1)]
for i in range(1,N+1):
    things[i][0],things[i][1]=list(map(int,input().split()))

#cell=[[0]*(V+1) for i in range(N+1)]

#for i in range(1,N+1):
#    for j in range(1,V+1):
#        #j空间大的背包，j大于物品体积的时候才有剩余空间价值
#        if(j>=things[i][0]):
#            if(cell[i-1][j]>things[i][1]+cell[i-1][j-things[i][0]]):
#               cell[i][j]=cell[i-1][j]
#            else:
#                cell[i][j]=things[i][1]+cell[i-1][j-things[i][0]]
#        else:
#            cell[i][j]=cell[i-1][j]

#print(cell[N][V])

dp=[0 for i in range(V+1)]

for i in range(1,N+1):
    j=V
    while(j>=things[i][0]):
        if(dp[j-things[i][0]]+things[i][1]>dp[j]):
            dp[j]=dp[j-things[i][0]]+things[i][1]
        j-=1
print(dp[V])