N,V=list(map(int,input().split()))
v=[0]*(N+1)
w=[0]*(N+1)

for i in range(1,N+1):
    v[i],w[i]=list(map(int,input().split()))

cell=[[0]*(V+1) for i in range(N+1)]

#def findMax(i,j):
#    max=cell[i-1][j]
#    for k in range(int(j/v[i]+1)):   
#        if(cell[i-1][j-k*v[i]]+k*w[i]>max):
#            max=cell[i-1][j-k*v[i]]+k*w[i]
#    return max

#for i in range(1,N+1):
#    for j in range(1,V+1):
#       cell[i][j]=findMax(i,j)

#print(cell[N][V])

for i in range(1,N+1):
    for j in range(1,V+1):
        if(j>=v[i]):
            if(cell[i][j-v[i]]+w[i]>cell[i-1][j]):
                cell[i][j]=cell[i][j-v[i]]+w[i]
            else:
                cell[i][j]=cell[i-1][j]
        else:
            cell[i][j]=cell[i-1][j]

print(cell[N][V])