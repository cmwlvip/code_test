N,V=list(map(int,input().split()))

v=[0 for i in range(11*N)]
w=[0 for i in range(11*N)]
s=[0 for i in range(N+1)]

for i in range(1,N+1):
    v[i],w[i],s[i]=list(map(int,input().split()))
count=N
for i in range(1,count+1):
    if(s[i]*v[i]>V):
        s[i]=int(V/v[i])
    k=1
    while(k<=s[i]):
        s[i]-=k
        N+=1
        v[N]=k*v[i]
        w[N]=k*w[i]
        k*=2
    v[i]=s[i]*v[i]
    w[i]=s[i]*w[i]
cell=[[0]*(V+1) for i in range(N+1)]
for i in range(1,N+1):
    for j in range(1,V+1):
        if(j>=v[i] and cell[i-1][j-v[i]]+w[i]>cell[i-1][j]):
            cell[i][j]=cell[i-1][j-v[i]]+w[i]
        else:
            cell[i][j]=cell[i-1][j]

print(cell[N][V])
