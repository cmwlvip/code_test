#include"Graph.h"
#include"../DataStructure/myFunction.h"
#include"SqQueue.h"

bool visited[MVNum];

int LocateVex(AMGraph G, VerTexType u)
{
	for (int i = 0; i < G.vexnum; i++)
	{
		if (G.vexs[i] == u)
		{
			return i;
		}
	}
	return -1;
}

int LocateVex(ALGraph G, VerTexType u) {
	for (int i = 0; i < G.vexnum; i++)
	{
		if (G.vertices[i].data==u)
		{
			return i;
		}
	}
	return -1;
}

bool CreateUDN(AMGraph& G) {
	//采用邻接矩阵表示法，创建无向网G
	printf("输入总顶点数：\n");					//输入总顶点数、总边数
	G.vexnum = intCin();
	printf("输入总边数：\n");
	G.arcnum = intCin();
	printf("依次输入顶点信息\n");
	for (int i = 0; i < G.vexnum; i++)
	{
		printf("输入...\n");
		Clean();
		G.vexs[i] = getchar();					//依次输入顶点信息
	}
	for (int i = 0; i < G.vexnum; i++)
	{
		for (int j = 0; j < G.vexnum; j++)
		{
			G.arcs[i][j] = MaxInt;				//初始化邻接矩阵，边的权值均置为最大值
		}
	}
	for (int k = 0; k < G.arcnum; k++)
	{
		printf("输入一条边依附的顶点和权值\n");	//输入一条边依附的顶点和权值
		printf("请输入边的权值\n");
		int w = intCin();
		printf("请输入其中一个顶点信息\n");
		Clean();
		char v1 = getchar();
		printf("请输入另外一个顶点信息\n");
		Clean();
		char v2 = getchar();
		int i = LocateVex(G, v1);
		int j = LocateVex(G, v2);				//确定v1和v2在G中的位置，即顶点数组下标
		G.arcs[i][j] = w;						//边<v1,v2>权值为w
		G.arcs[j][i] = G.arcs[i][j];			//无向图，邻接矩阵对称
	}
	return true;
}

bool CreateDG(AMGraph& G) {
	//采用邻接矩阵表示法，创建有向图G
	printf("输入总顶点数：\n");					//输入总顶点数、总边数
	G.vexnum = intCin();
	printf("输入总边数：\n");
	G.arcnum = intCin();
	printf("依次输入顶点信息\n");
	for (int i = 0; i < G.vexnum; i++)
	{
		printf("输入...\n");
		Clean();
		G.vexs[i] = getchar();					//依次输入顶点信息
	}
	for (int i = 0; i < G.vexnum; i++)
	{
		for (int j = 0; j < G.vexnum; j++)
		{
			G.arcs[i][j] = MaxInt;				//初始化邻接矩阵，边的权值均置为最大值
			if (i == j)
			{
				G.arcs[i][j] = 0;
			}
		}
	}
	for (int k = 0; k < G.arcnum; k++)
	{
		printf("输入一条边依附的顶点和权值\n");	//输入一条边依附的顶点和权值
		printf("请输入边的权值\n");
		int w = intCin();
		printf("请输入始点（弧尾）信息\n");
		Clean();
		char v1 = getchar();
		printf("请输入终点（弧头）信息\n");
		Clean();
		char v2 = getchar();
		int i = LocateVex(G, v1);
		int j = LocateVex(G, v2);				//确定v1和v2在G中的位置，即顶点数组下标
		G.arcs[i][j] = w;						//边<v1,v2>权值为w
	}
	return true;
}

void GraphTraverse(AMGraph G) {
	printf("图的顶点信息如下：\n");
	for (int i = 0; i < G.vexnum; i++)
	{
		printf("%c\t", G.vexs[i]);
	}
	printf("\n");
	printf("图的邻接矩阵如下：\n");
	for (int i = 0; i < G.vexnum; i++)
	{
		for (int j = 0; j < G.vexnum; j++)
		{
			printf("%d\t", G.arcs[i][j]);
		}
		printf("\n");
	}
}

bool GreatUDG(ALGraph& G) {
	printf("输入总顶点数：\n");					//输入总顶点数、总边数
	G.vexnum = intCin();
	printf("输入总边数：\n");
	G.arcnum = intCin();
	printf("依次输入顶点信息\n");
	for (int i = 0; i < G.vexnum; i++)
	{
		printf("输入...\n");
		Clean();
		G.vertices[i].data = getchar();			//输入顶点值
		G.vertices[i].firstArc = NULL;			//初始化表头结点的指针域为NULL
	}
	for (int k = 0; k < G.arcnum; k++)			//输入各边，构建邻接表
	{
		printf("输入一条边依附的顶点\n");		//输入一条边依附的顶点
		printf("请输入其中一个顶点信息\n");
		Clean();
		char v1 = getchar();
		printf("请输入另外一个顶点信息\n");
		Clean();
		char v2 = getchar();
		int i = LocateVex(G, v1);
		int j = LocateVex(G, v2);				//确定v1和v2在G中的位置，即顶点在G.vertices中下标
		//将新结点*p1插入顶点Vi的边表头部
		ArcNode* p1 = (ArcNode*)malloc(sizeof(ArcNode));	//生成新的边结点*p1
		if (p1==NULL)
		{
			return false;
		}
		p1->adjvex = j;										//邻接点序号为j
		p1->nextarc = G.vertices[i].firstArc;
		G.vertices[i].firstArc = p1;
		//将新结点*p2插入顶点Vj的边表头部
		ArcNode* p2 = (ArcNode*)malloc(sizeof(ArcNode));	//生成新的边结点*p2
		if (p2==NULL)
		{
			return false;
		}
		p2->adjvex = i;									//邻接点序号为i
		p2->nextarc = G.vertices[j].firstArc;
		G.vertices[j].firstArc = p2;
	}
	return true;
}

void GraphTraverse(ALGraph G) {
	printf("图的顶点信息如下：\n");
	for (int i = 0; i < G.vexnum; i++)
	{
		printf("%c\t", G.vertices[i].data);
	}
	printf("\n");
}

void DFS_AM(AMGraph G, int v) {

	//图G为邻接矩阵类型，从第v个顶点出发深度优先搜索遍历图G
	printf("%c\t",G.vexs[v]);			//访问第v个顶点，并置访问标志数组相应分量值为true
	visited[v] = true;
	for (int w = 0; w < G.vexnum; w++)	//依次检查邻接矩阵v所在的行
	{
		//G.arcs[v][w]!=MaxInt表示w是v的邻接点，如果w未访问，则递归条用DFS_AM
		if (G.arcs[v][w] != MaxInt && !visited[w])
		{
			DFS_AM(G, w);
		}
	}
}

void DFS_AL(ALGraph G, int v) {
	//图G为邻接表类型，从第v个顶点出发深度优先遍历搜索图G
	printf("%c\t", G.vertices[v].data);			//访问第v个顶点，并置访问标志数组相应分量值为true
	visited[v] = true;
	ArcNode* p = G.vertices[v].firstArc;		//p指向v的边链表的第一个边结点
	while (p!=NULL)
	{
		int w = p->adjvex;				//w是v的邻接点
		if (!visited[w])				//如果w未访问，则递归调用DFS_AL
		{
			DFS_AL(G, w);
		}
		p = p->nextarc;					//p指向下一个结点
	}
}

void DFS(AMGraph G) {
	for (int i = 0; i < MVNum; i++)
	{
		visited[i] = false;
	}
	DFS_AM(G, 0);
}

void DFS(ALGraph G) {
	for (int i = 0; i < MVNum; i++)
	{
		visited[i] = false;
	}
	DFS_AL(G, 0);
}

void BFS_AM(AMGraph G, int v) {
	//图G为邻接矩阵类型，从第v个顶点出发【广度优先非递归】搜索遍历图G
	printf("%c\t", G.vexs[v]);			//访问第v个顶点，并置访问标志数组相应分量值为true
	visited[v] = true;
	SqQueue Q;
	InitQueue(Q);						//辅助队列Q置空
	EnQueue(Q, G.vexs[v]);				//v进队
	while (!QueueEmpty(Q))				//队列非空
	{
		VerTexType u;
		DeQueue(Q, u);					//队头元素出队并赋给u
		int index = LocateVex(G, u);
		for (int w = 0; w < G.vexnum; w++)
		{
			if ((G.arcs[index][w] != MaxInt) && (!visited[w]))
			{
				printf("%c\t", G.vexs[w]);		//访问w,并置访问数组相应分量为true
				visited[w] = true;
				EnQueue(Q, w);					//w进队
			}
		}
	}
}

void BFS_AL(ALGraph G, int v) {
	//图为邻接表存储类型，从第v个结点出发【广度优先遍历】图G
	printf("%c\t", G.vertices[v].data);			//访问第v个顶点，并置访问标志数组相应分量值为true
	visited[v] = true;
	SqQueue Q;
	InitQueue(Q);								//辅助队列Q置空
	EnQueue(Q, G.vertices[v].data);				//v进队
	while (!QueueEmpty(Q))						//队列非空
	{
		VerTexType u;
		DeQueue(Q, u);							//队头元素出队并赋给u
		int index = LocateVex(G, u);
		ArcNode* p = G.vertices[index].firstArc;//p指向出队结点u(先确定下标)的边链表的第一个边结点,
		while (p != NULL)
		{
			int w = p->adjvex;					//w是v的邻接点
			if (!visited[w])					//如果w未访问，访问并标记
			{
				printf("%c\t", G.vertices[w].data);
				visited[w] = true;
				EnQueue(Q, G.vertices[w].data);
			}
			p = p->nextarc;						//p指向下一个结点
		}
	}
}

void BFS(AMGraph G) {
	for (int i = 0; i < MVNum; i++)
	{
		visited[i] = false;
	}
	BFS_AM(G, 0);
}

void BFS(ALGraph G) {
	for (int i = 0; i < MVNum; i++)
	{
		visited[i] = false;
	}
	BFS_AL(G, 0);
}

int Min(AMGraph G) {
	int i = 0;
	while (closedge[i].lowcost == 0)
	{
		i++;
	}
	int min = i;
	for (i=i+1; i < G.vexnum; i++)
	{
		if (closedge[i].lowcost < closedge[min].lowcost && (closedge[i].lowcost != 0))
		{
			min = i;
		}
	}
	return min;
}
//U={u},找到最小边的另一个点并入U,直到U=V
void MiniSpanTree_Prim(AMGraph G, VerTexType u) {
	//无向网G以邻接矩阵形式存储，从顶点u出发构造G的最小生成树，输出T的各边
	int k = LocateVex(G, u);			//k为u的下标
	for (int i = 0; i < G.vexnum; i++)
	{
		if (i!=k)
		{
			closedge[i] = { u,G.arcs[k][i] };
		}
	}
	closedge[k].lowcost = 0;			//初始，U={u}
	//选择其余n-1个顶点，生成n-1条边（n=G.vexnum）
	for (int i = 1; i < G.vexnum; i++)
	{
		//求出T的下一个结点：第k个顶点，closedge[k]中存有当前最小边
		k = Min(G);
		VerTexType u0 = closedge[k].adjvex;		//u0为最小边一个顶点u0∈U
		VerTexType v0 = G.vexs[k];				//v0为最小边的令一个顶点，v0∈V-U
		printf("(%c,%c)\t", u0, v0);				//输出最小边
		closedge[k].lowcost = 0;				//将第k个顶点并入U集
		for (int j = 0; j < G.vexnum; j++)		//新点并入U后从新选择最小边
		{
			if (G.arcs[k][j]<closedge[j].lowcost)//不会比0小，不会再有用过的边
			{
				closedge[j] = { G.vexs[k],G.arcs[k][j] };
			}
		}
	}
}

int Edge_Compare(Edge a, Edge b) {
	if (a.lowcost==b.lowcost)
	{
		return 0;
	}
	else if(a.lowcost>b.lowcost)
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

void Edge_Change(Edge& a, Edge& b) {
	Edge temp = a;
	a = b;
	b = temp;
}
void Sort(Edge edge[], AMGraph& G) {
	for (int i = 0; i < G.arcnum; i++)					//一共进行n-1趟
	{
		int min = i;									//记录最小元素位置
		for (int j = i + 1; j < G.arcnum; j++)
		{
			if (Edge_Compare(edge[j] ,edge[min])==-1)	//找到最小元素
			{
				min = j;
			}
		}
		if (min != i) {									//交换
			Edge_Change(edge[i], edge[min]);
		}
	}
}
int Vexset[MVNum] = { 0 };
void MiniSpanTree_Kruskal(AMGraph G) {
	//无向图G以邻接矩阵形式存储，构造G的最小生成树T,输出T的各边
	if (G.arcnum<1)
	{
		return;
	}
	Edge* edge = (Edge*)malloc(sizeof(Edge) * (G.arcnum));
	if (edge==NULL)
	{
		return;
	}
	int k = 0;
	for (int i = 0; i < G.vexnum; i++)			//下三角
	{
		for (int j = 0; j < i; j++)
		{
			if (G.arcs[i][j] != MaxInt)
			{
				edge[k].Head = G.vexs[i];
				edge[k].Tail = G.vexs[j];
				edge[k].lowcost = G.arcs[i][j];
				k++;
			}
		}
	}
	Sort(edge,G);							//将数组Edge中的元素按从小到大排序
	for (int i = 0; i < G.vexnum; i++)		//辅助数组，表示各个顶点自成一个连通分量
	{
		Vexset[i] = i;
	}
	for (int i = 0; i < G.arcnum; i++)		//依次查看数组Edge中的边
	{
		int v1 = LocateVex(G, edge[i].Head);//v1为边始点Head下标
		int v2 = LocateVex(G, edge[i].Tail);//v2为边终点下标
		int vs1 = Vexset[v1];				//获取edge[i]的始点所在连通分量vs1
		int vs2 = Vexset[v2];				//获取edge[i]的终点所在连通分量vs2
		if (vs1 != vs2)						//属于不同的连通分量
		{
			printf("(%c,%c)\t", edge[i].Head, edge[i].Tail);	//输出最小边
			for (int j = 0; j < G.vexnum; j++)		//合并vs1和vs2两个连通分量，即两个集合同一编号
			{
				if (Vexset[j] == vs2)
				{
					Vexset[j] = vs1;			//同一编号
				}
			}
		}
	}
}

bool S[];
int Path[];
ArcType D[];

//v0源点
void PathPrint(AMGraph G, int v, int v0)
{
	printf("%c ← \t", G.vexs[v]);
	if (Path[v] == v0)					//Path记录的是前驱
	{
		printf("%c\t", G.vexs[v0]);
		return;
	}
	PathPrint(G, Path[v], v0);
}

void ShortestPath_DIJ(AMGraph G, int v0) {
	//用Dijkstra算法求有向网G的v0顶点到其余顶点的最短路径
	int n = G.vexnum;					//n为G中的顶点个数
	for (int i = 0; i < n; i++)			//n个顶点依次初始化
	{
		S[i] = false;					//S初始为空集
		D[i] = G.arcs[v0][i];			//将v0到各个终点的最短路径长度初始化为弧上的权值
		if (D[i] < MaxInt)
		{
			Path[i] = v0;				//如果v0和i直间有弧，将v的前驱置为v0
		}
		else
		{
			Path[i] = -1;				//如果v0和i直间无弧，将v的前驱置为-1
		}
	}
	S[v0] = true;						//将v0加入S
	D[v0] = 0;							//源点到源点的距离为0
	/*-----初始化结束，开始主循环，每次求得v0到每个点i的最短路径，将i加入S集*/
	for (int i = 1; i < n; i++)			//对其余n-1个顶点，依次进行计算
	{
		int min = MaxInt;
		int v=v0;
		for (int j = 0; j < n; j++)
		{
			if (!S[j] && D[j] < min)
			{
				v = j;
				min = D[j];				//选择一条当前最短的路径，终点为v
			}
		}
		S[v] = true;					//将v加入S
		for (int j = 0; j < n; j++)		//更新从v0出发到集合V-S上所有顶点的最短路径长度
		{
			if (!S[j] && (D[v] + G.arcs[v][j] < D[j]))
			{
				D[j] = D[v] + G.arcs[v][j];	//更新D[v]
				Path[j] = v;				//更改j的前驱为v
			}
		}
	}
	for (int i = 0; i < n; i++)
	{
		if (D[i]==MaxInt)
		{
			printf("从%c到%c无短路径！！\n", G.vexs[v0], G.vexs[i]);
			continue;
		}
		printf("从%c到%c最短路径为：\n", G.vexs[v0], G.vexs[i]);
		PathPrint(G, i, v0);
		printf("\n");
		printf("最短路径为：%d\n",D[i]);
		printf("\n");
	}
}