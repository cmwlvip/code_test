#include<stdio.h>
#include <stdlib.h>
#include"../DataStructure/myFunction.h"
#include"Graph.h"

void ListMenu() {
	printf("********************************************\n");
	printf("**************菜单**************************\n");
	printf("**************0.退出************************\n");
	printf("**************1.采用【邻接矩阵】建立无向图**\n");
	printf("**************2.采用【邻接表】建立无向图****\n");
	printf("**************3.深度优先遍历（邻接矩阵）*****\n");
	printf("**************4.深度优先遍历（邻接表）*******\n");
	printf("**************5.广度优先遍历（邻接矩阵）*****\n");
	printf("**************6.广度优先遍历（邻接表）*******\n");
	printf("**************7.普里姆(Prim)算法**************\n");
	printf("**************8.克鲁斯卡尔(Kruskal)算法*******\n");
	printf("**************9.采用【邻接矩阵】建立有向图****\n");
	printf("**************10.迪杰斯特拉算法***************\n");
}

int main() {
	AMGraph MG{};
	ALGraph LG{};
	while (true)
	{
		ListMenu();
		printf("请输入您的选择：\n");
		int choice = intCin();
		switch (choice)
		{
		case 0:
			ExitSystem();//退出程序
			break;
		case 1: {
			CreateUDN(MG);
			printf("创建完成，遍历如下：\n");
			GraphTraverse(MG);
			system("pause");
			system("cls");
		}
			  break;
		case 2: {
			GreatUDG(LG);
			GraphTraverse(LG);
			system("pause");
			system("cls");
		}
			  break;
		case 3: {
			printf("深度优先遍历如下：\n");
			DFS(MG);
			system("pause");
			system("cls");
		}
			  break;
		case 4: {
			printf("深度优先遍历如下：\n");
			DFS(LG);
			system("pause");
			system("cls");
		}
			  break;
		case 5: {
			printf("广度度优先遍历如下：\n");
			BFS(MG);
			system("pause");
			system("cls");
			break;
		}
		case 6: {
			printf("广度度优先遍历如下：\n");
			BFS(LG);
			system("pause");
			system("cls");
			break;
		}
		case 7: {
			printf("【普里姆算法】最小生成树如下：\n");
			MiniSpanTree_Prim(MG, MG.vexs[0]);
			system("pause");
			system("cls");
			break;
		}
		case 8: {
			printf("【克鲁斯卡尔算法】最小生成树如下：\n");
			MiniSpanTree_Kruskal(MG);
			system("pause");
			system("cls");
			break; 
		}
		case 9: {
			CreateDG(MG);
			system("pause");
			system("cls");
			break; 
		}
		case 10: {
			ShortestPath_DIJ(MG, 0);
			system("pause");
			system("cls");
			break;
		}
		default:
			printf("非法输入，请重新输入！\n");
			system("pause");
			system("cls");
			break;
		}
	}
	return 0;
}