#pragma once
#include<stdio.h>
#include<stdlib.h>

#define MaxInt 32767				//表示极大值∞
#define MVNum 16					//最大顶点数
typedef char VerTexType;			//假设顶点数据类型为字符型
typedef int ArcType;				//假设边数据类型为整形

//采用邻接矩阵建立无向网
//1、输入总顶点数和总边数
//2、依次输入点的信息存入定点表
//3、初始化邻接矩阵，使每个权值初始化为无穷大
//4、构造邻接矩阵

//--------图的邻接矩阵存储表示-------
typedef struct
{
	VerTexType vexs[MVNum];			//顶点表【vertex】
	ArcType arcs[MVNum][MVNum];		//邻接矩阵【弧arc】
	int vexnum, arcnum;				//图当前点数和边数
}AMGraph;	//Adjacency Matrix Graph

//图中查找顶点u，存在返回下标，否则返回-1
int LocateVex(AMGraph G, VerTexType u);

//采用邻接矩阵表示法创建无向网【Undirected network】
bool CreateUDN(AMGraph& G);
//采用邻接矩阵表示法创建有向图【Undirected network】
bool CreateDG(AMGraph& G);

//遍历邻接矩阵存储的图
void GraphTraverse(AMGraph G);

/***********************************************************************/
typedef int OtherInfo;
//-----图的邻接表存储表示-------
typedef struct ArcNode
{
	int adjvex;					//该边所指向的顶点位置
	struct ArcNode* nextarc;	//指向下一条边指针
	OtherInfo info;				//和边相关信息
}ArcNode;

typedef struct VNode
{
	VerTexType data;			//顶点信息
	ArcNode* firstArc;			//指向第一条依附该顶点边指针
}AdjList[MVNum];

typedef struct
{
	AdjList vertices;			//vertices-vertex复数
	int vexnum, arcnum;			//当前顶点数与弧数
}ALGraph;	//Adjacency List Graph

//采用邻接表法创建无向图
bool GreatUDG(ALGraph& G);

//图中查找顶点u，存在返回下标，否则返回-1(重载)
int LocateVex(ALGraph G, VerTexType u);

//遍历邻接表存储的图
void GraphTraverse(ALGraph G);

/***********************************************************************/
//头文件只用于声明和描述全局变量，不应该进行定义和初始化操作
//全局变量的定义和初始化只能在一个源文件中进行
extern bool visited[MVNum];

//采用邻接矩阵表示图的深度优先遍历搜索
void DFS_AM(AMGraph G, int v);

//采用邻接表表示的图的深度优先搜索遍历
void DFS_AL(ALGraph G, int v);

//图的深度优先遍历(Depth First Search)
void DFS(AMGraph G);

void DFS(ALGraph G);

//采用邻接矩阵表示图的广度优先遍历搜索
void BFS_AM(AMGraph G, int v);

//采用邻接表表示的图的广度优先搜索遍历
void BFS_AL(ALGraph G, int v);

//图的广度优先遍历(Breadth First Search)
void BFS(AMGraph G);

void BFS(ALGraph G);

/***********************************************************************/
struct
{
	VerTexType adjvex;			//最小边在U中的那个顶点
	ArcType lowcost;			//最小边上的权值
}closedge[MVNum];

//普里姆算法
void MiniSpanTree_Prim(AMGraph G, VerTexType u);

/***********************************************************************/
struct Edge
{
	VerTexType Head;			//边的始点
	VerTexType Tail;			//边的终点
	ArcType lowcost;			//边上的权值
};
//相等为0,a>b为1，a<b为-1
int Edge_Compare(Edge a, Edge b);
//结构体交换
void Edge_Change(Edge& a, Edge& b);
//辅助数组Vexset的定义
extern int Vexset[MVNum];

//克鲁斯卡尔算法
void MiniSpanTree_Kruskal(AMGraph G);

/***********************************************************************/
//-----迪杰斯特拉算法辅助数据结构-----
//记录源点v0到终点vi是否确定最短路径长度.true表示确定，false表示不确定
extern bool S[MVNum];		

//记录从源点v0到终点vi的当前最短路径上vi的直接前驱顶点序号
//【初值】如果从v0到vi有弧，则Path[i]为v0，否则为-1
extern int Path[MVNum];

//记录从源点v0到终点vi的当前最短路径长度
//【初值】如果从v0到vi有弧，则D[i]为弧上的权值，否则为∞
extern ArcType D[MVNum];

//迪杰斯特拉算法[Dijkstra algorithm]
void ShortestPath_DIJ(AMGraph G, int v0);

/***********************************************************************/
//弗洛伊德(Floyd)算法

//拓扑排序

//关键路径算法
