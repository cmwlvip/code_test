#include"SqQueue.h"

bool InitQueue(SqQueue& Q) {
	//构造一个空队列Q
	Q.base = (QElemType*)malloc(sizeof(QElemType) * MAXSIZE);		//为队列分配一个最大容量为MAXSIZE的数组空间
	if (!Q.base)
	{
		return false;				//存储分配失败
	}
	Q.front = Q.rear = 0;			//头指针和尾指针置为0，队列为空
	return true;
}

int QueueLength(SqQueue Q) {
	//返回Q的元素个数，即队列的长度
	return (Q.rear - Q.front + MAXSIZE) % MAXSIZE;		//对于循环队列差值可能为负数，加上MAXSIZE
}

bool EnQueue(SqQueue& Q, QElemType e) {
	//插入元素e为Q的新队尾元素
	if ((Q.rear + 1) % MAXSIZE == Q.front)
	{
		return false;			//尾指针在循环意义上+1等于头指针，队满（牺牲一个单元）
	}
	Q.base[Q.rear] = e;			//新元素插入队尾
	Q.rear = (Q.rear + 1) % MAXSIZE;
	return true;
}

bool DeQueue(SqQueue& Q, QElemType& e) {
	//删除Q的队头元素，用e返回其值
	if (Q.front==Q.rear)
	{
		return false;							//队空
	}
	e = Q.base[Q.front];						//保存队头元素
	Q.front = (Q.front + 1) % MAXSIZE;			//队尾指针+1
	return true;
}

bool GetHead(SqQueue Q, QElemType& e) {
	//返回Q的队头元素，不修改队头指针
	if (Q.front!=Q.rear)						//队列非空
	{
		e = Q.base[Q.front];					//返回队头元素的值，队头指针不变
		return true;
	}
	return false;
}

bool QueueEmpty(SqQueue Q) {
	if (Q.front==Q.rear)
	{
		return true;
	}
	return false;
}