#pragma once
#include<stdio.h>
#include <stdlib.h>
#include"../DataStructure/Person.h"

typedef Person SElemType;
//------˳��ջ�Ĵ洢�ṹ-------------
#define MAXSIZE 100				//˳��ջ�洢�ռ�ĳ�ʼ������

typedef struct {
	SElemType* base;			//ջ��ָ��
	SElemType* top;				//ջ��ָ��
	int stacksize;
}SqStack;

//˳��ջ�ĳ�ʼ��
bool InitStack(SqStack& S);

//˳��ջ����ջ
bool Push(SqStack& S, SElemType e);

//˳��ջ�ĳ�ջ
bool Pop(SqStack& S, SElemType& e);

//ȡ˳��ջ��ջ��Ԫ��
SElemType GetTop(SqStack S);

//˳��ջ�п�
bool StackEmpty(SqStack S);