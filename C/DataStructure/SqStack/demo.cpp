#include"SqStack.h"
#include<stdio.h>
#include <stdlib.h>

#include"../DataStructure/myFunction.h"

void ListMenu() {
	printf("*******************************************\n");
	printf("**************菜单*************************\n");
	printf("**************0.退出***********************\n");
	printf("**************1.顺序栈的入栈****************\n");
	printf("**************2.顺序栈的出栈***************\n");
	printf("**************3.取顺序栈的栈顶元素*********\n");
	printf("**************10.*************************\n");
}

int main() {
	SqStack S;
	InitStack(S);
	while (true)
	{
		ListMenu();
		printf("请输入您的选择：\n");
		int choice = intCin();
		switch (choice)
		{
		case 0:
			ExitSystem();//退出程序
			break;
		case 1: {
			printf("请输入对象的的姓名！\n");
			char name[16];
			scanf_s("%15s", name, sizeof(name)); // 获取名字
			printf("请输入对象的的年龄！\n");
			int age = intCin();
			Person e = create_struct(name, age);
			if (Push(S, e))
			{
				printf("入栈成功！\n");
			}
			else {
				printf("入栈失败！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 2: {
			SElemType e = create_struct();
			if (Pop(S,e))
			{
				printf("出栈成功！出栈信息如下：\n");
				PersonPrint(e);
			}
			else {
				printf("栈空！出栈失败！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 3: {
			SElemType e = GetTop(S);
			printf("栈顶信息如下:\n");
			PersonPrint(e);
			system("pause");
			system("cls");
		}
			  break;
		default:
			printf("非法输入，请重新输入！\n");
			system("pause");
			system("cls");
			break;
		}
	}

	system("pause");
	return 0;
}