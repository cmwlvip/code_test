#pragma once
#include<stdio.h>
#include<stdlib.h>
#include"../DataStructure/Person.h"

typedef Person QElemType;
//为了操作方便，给链队添加一个头结点
//并令头指针始终指向头结点
//-----队列的链式存储------
typedef struct QNode			
{
	QElemType data;
	struct QNode* next;
}QNode,*QueuePtr;
typedef struct
{
	QueuePtr front;				//队头指针
	QueuePtr rear;				//队尾指针
}LinkQueue;

//链队的初始化
bool InitQueue(LinkQueue& Q);

//链队的入队
bool EnQueue(LinkQueue& Q, QElemType e);

//链队的出队
bool DeQueue(LinkQueue& Q, QElemType& e);

//取链队的队头元素
bool GetHead(LinkQueue Q,QElemType& e);