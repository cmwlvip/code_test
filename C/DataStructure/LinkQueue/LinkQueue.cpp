#include"LinkQueue.h"

bool InitQueue(LinkQueue& Q) {
	//构造一个空队列
	Q.front = Q.rear = (QueuePtr)malloc(sizeof(QNode));	//生成新的结点作为头结点，队头队尾指向此结点
	if (!Q.front || !Q.rear)
	{
		return false;						//申请分配空间失败
	}
	Q.front->next = NULL;					//头结点指针域置空
	return true;
}

bool EnQueue(LinkQueue& Q, QElemType e) {
	//插入元素e为Q的新队尾元素
	QNode* p = (QueuePtr)malloc(sizeof(QNode));		//为入队元素分配结点空间，用p指向新结点
	if (!p)
	{
		return false;
	}
	p->data = e;									//将新结点的指针域置为e
	p->next = NULL;									
	Q.rear->next = p;								//将新结点插入到队尾
	Q.rear = p;										//修改队尾指针
	return true;
}

bool DeQueue(LinkQueue& Q, QElemType& e) {
	//删除Q的队头元素，用e返回其值
	if (Q.front==Q.rear)
	{
		return false;							//队空
	}
	QNode* p = Q.front->next;					//p指向队头元素
	e = p->data;								//e保存队头元素
	Q.front->next = p->next;					//修改头头结点的指针域
	if (Q.rear==p)								//这里需要特变注意，最后一个元素被删，队尾指针丢失
	{	
		Q.rear = Q.front;						//最后一个元素被删，队尾指针指向头结点
	}
	free(p);		
}

bool GetHead(LinkQueue Q, QElemType& e) {
	//返回Q的队头元素，不修改队头指针
	if (Q.front!=Q.rear)						//队列非空
	{
		e = Q.front->next->data;				//返回队头元素，队头指针不变
		return true;
	}
	return false;
}
