#include<stdio.h>
#include <stdlib.h>
#include"DynamicSequentialList.h"
#include"../DataStructure/myFunction.h"

void ListMenu() {
	printf("*******************************************\n");
	printf("**************动态分配顺序表***************\n");
	printf("**************0.退出***********************\n");
	printf("**************1.初始化顺序表***************\n");
	printf("**************2.清空顺序表*****************\n");
	printf("**************3.销毁顺序表*****************\n");
	printf("**************4.按位插入元素***************\n");
	printf("**************5.插入一组测试元素***********\n");
	printf("**************6.按位查找元素***************\n");
	printf("**************7.按值查找元素***************\n");
	printf("**************8.按位删除元素***************\n");
	printf("**************10.遍历顺序表***************\n");
}

int main() {
	SeqList list;
	list.data = NULL;	//先把指针指向空
	while (true)
	{
		ListMenu();
		printf("请输入您的选择：\n");
		int choice = intCin();
		switch (choice)
		{
		case 0://退出程序
			ExitSystem();
			break;
		case 1: {
			//用于初始化顺序表
			list = InitList(list);
			printf("已为你初始化动态顺序表！\n");
			system("pause");
			system("cls");
		}
			  break;
		case 2: {
			ClearList(list);//清空
			system("pause");
			system("cls");
		}
			  break;
		case 3: {
			DestroyList(list);
			system("pause");
			system("cls");
		}
			  break;
		case 4: {
			printf("请输入插入的位序！\n");
			int i = intCin();
			printf("请输入对象的的姓名！\n");
			char name[16];
			scanf_s("%15s", name, sizeof(name)); // 获取名字
			printf("请输入对象的的年龄！\n");
			int age = intCin();
			ElemType e = create_struct(name, age);
			ListInsert(list, i, e);
			system("pause");
			system("cls");
		}
			break;
		case 5: {
			//准备好5个数据
			char personName_array[5][16] = { "亚瑟","王昭君","貂蝉","孙悟空","张飞" };
			int age_array[5] = { 38,23,16,22,43 };
			for (int i = 0; i < 5; i++)
			{
				ElemType e = create_struct(personName_array[i], age_array[i]);
				ListInsert(list,1,e);
			}
			printf("成功插入一组测试数据！\n");
			system("pause");
			system("cls");
		}
			  break;
		case 6: {
			printf("请输入插入的位序！\n");
			int i = intCin();
			ElemType e;
			if (GetElem(list, i, e))
			{
				printf("查找结果如下:\n");
				PersonPrint(e);
			}
			else
			{
				printf("查找失败！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 7: {
			printf("请输入待查找对象的的姓名！\n");
			char name[16];
			scanf_s("%15s", name, sizeof(name)); // 获取名字
			printf("请输入待查找对象的的年龄！\n");
			int age = intCin();
			ElemType e = create_struct(name, age);
			int flag = LocateElem(list, e, PersonCompare);
			if (flag) {
				printf("查找对象位序为：%d\n", flag);
			}
			else
			{
				printf("查无此人！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 8: {
			printf("请输入想删除的元素位序\n");
			int i = intCin();
			ElemType e;
			if (ListDelete(list,i,e))
			{
				printf("删除成功");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 10: {
			TraverseList(list,PersonPrint);
			system("pause");
			system("cls");
		}
			break;
		default:
			printf("非法输入，请重新输入！\n");
			system("pause");
			system("cls");
			break;
		}
	}
	system("pause");
	return 0;
}