#pragma once //防止头文件重复包含
#include<stdio.h>
#include <stdlib.h>
#include<string.h>
#include"../DataStructure/Person.h"

//表长度的初始定义
#define InitSize 10

typedef struct
{
	ElemType *data;			//指示动态分配数组的指针
	int capacity;			//动态顺序表当前申请的容量
	int length;				//顺序表当前长度
}SeqList;					//动态分配顺序表的类型定义

//初始化表，构造一个空的线性表
SeqList InitList(SeqList& L);

//将线性表置空
void ClearList(SeqList& L);

//销毁线性表
void DestroyList(SeqList& L);

//判空操作，若为空，返回true，否则返回false
bool ListEmpty(SeqList L);

//返回线性表元素个数
int ListLength(SeqList L);

//按位查找，获取表中的第i个位置数据元素的值，用e返回
bool GetElem(SeqList L, int i, ElemType& e);

//按值查找,返回L中第1个值与e相同的元素在L中的位置，不存在，返回0
int LocateElem(SeqList L, ElemType e, int(*myCompare)(ElemType, ElemType));

//按位插入操作，在表中指定元素e，(i <= i<= L.length+1),成功为true
bool ListInsert(SeqList& L, int i,ElemType e);

//按位删除，删除表中第i个位置上的元素，并用e返回删除元素的值
bool ListDelete(SeqList& L, int i, ElemType& e);

//遍历
void TraverseList(SeqList L,void(*myPrint)(ElemType));