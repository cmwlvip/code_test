#include "DynamicSequentialList.h"

//L不存在信息提示函数
void errorInfo() {
	printf("顺序表不存在！请先初始化顺序表！\n");
}

SeqList InitList(SeqList& L) {
	if (L.data!=NULL) {
		printf("顺序表表已初始化！无需二次初始！\n");
		return L;
	}
	L.data = (ElemType*)malloc(sizeof(ElemType) * InitSize);
	//判断内存是否申请成功
	if (L.data == NULL)
	{
		printf("内存申请失败!\n");
		system("pause");							//暂停，随后继续生请
		L = InitList(L);								//继续尝试申请
	}
	L.length = 0;
	L.capacity = InitSize;							//申请完成，线性表容量位初始值
	return L;
}

void ClearList(SeqList& L) {
	if (L.data==NULL)
	{
		errorInfo();
		return;
	}
	L.length = 0;									//逻辑清空
}

void DestroyList(SeqList& L) {
	if (L.data==NULL)
	{
		errorInfo();
		return;
	}
	L.capacity = 0;
	L.length = 0;
	free(L.data);									//释放空间
	L.data = NULL;
}

bool ListEmpty(SeqList L) {
	return L.length;
}

int ListLength(SeqList L) {
	return L.length;
}

bool GetElem(SeqList L, int i, ElemType& e) {
	if (L.data == NULL)
	{
		errorInfo();
		return false;
	}
	if (i<1 || i>L.length)
	{
		printf("查找位序应该在[1,%d+1]，位置无效！\n", L.length);
		return false;
	}
	e = L.data[i - 1];			//elem[i-1]存储第i个数据元素
	return true;
}

void my_malloc(SeqList& L) {
	ElemType *newSpace=(ElemType*)malloc(sizeof(ElemType) * L.capacity*2);		//创建新空间
	if (newSpace==NULL)
	{
		printf("顺序表已满，动态分配内存失败！插入失败！\n");
		return;
	}
	// 使用 memcpy 进行内存拷贝
	memcpy(newSpace, L.data, sizeof(ElemType) * L.capacity);					//将原来的数据拷贝到新空间
	free(L.data);																//释放原有空间
	L.data = NULL;
	L.data = newSpace;															//更新指针指向
	L.capacity *= 2;															//更新容量大小
	printf("线性表已满，但是动态重新分配内存！\n");
}
void re_alloc(SeqList& L) {
	ElemType* newSpace = (ElemType*)realloc(L.data, sizeof(ElemType) * L.capacity * 2);
	if (newSpace == NULL)
	{
		printf("线性表已满，但是动态内存分配失败！插入失败！\n");
		return;
	}
	L.data = newSpace;
	L.capacity *= 2;
}

int LocateElem(SeqList L, ElemType e, int(*myCompare)(ElemType, ElemType)) {
	if (L.data == NULL)
	{
		errorInfo();
		return 0;				//查找失败，返回0，线性表的位序从1开始
	}
	for (int i = 0; i < L.length; i++)
	{
		if (myCompare(L.data[i],e))
		{
			return i + 1;
		}
	}
	return 0;
}

bool ListInsert(SeqList& L,int i, ElemType e) {
	if (L.data == NULL)
	{
		errorInfo();
		return false;
	}
	if (i<1 || i>L.length + 1) {	//判断i的范围是否有效
		printf("插入的位置应该在[1,%d+1]，位置无效！\n",L.length);
		return false;
	}
	if (L.length==L.capacity) {		//空间不足需要动态开辟
		//两种方式
		//re_alloc(L);
		my_malloc(L);
	}
	for (int j = L.length;j >= i; j--)
	{
		L.data[j] = L.data[j - 1];
	}
	L.data[i - 1] = e;				//在位置i处放入e
	L.length++;
	return true;
}

bool ListDelete(SeqList& L, int i, ElemType& e) {
	if (L.data == NULL)
	{
		errorInfo();
		return false;
	}
	if (i<1 || i>L.length)
	{
		printf("删除的位置应该在[1,%d]，位置无效！\n", L.length);
		return false;
	}
	e = L.data[i - 1];
	for (int j = i; j < L.length; j++)
	{
		L.data[j - 1] = L.data[j];
	}
	L.length--;
	return true;
}

void TraverseList(SeqList L, void(*myPrint)(ElemType)){
	if (L.data == NULL) {
		errorInfo();
		return;
	}
	printf("\n");
	printf("当前顺序表长度为：%d，遍历如下：\n", L.length);
	for (int i = 0; i < L.length; i++)
	{
		myPrint(L.data[i]);
	}
}