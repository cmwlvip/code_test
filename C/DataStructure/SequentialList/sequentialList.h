#pragma once //防止头文件重复包含
#include<stdio.h>
#include<string.h>
#include"../DataStructure/Person.h"	//导入元素头文件，这里导入的是Person结构体

//高可用 静态顺序表

#define MaxSize 50

typedef struct
{
	ElemType data[MaxSize];	//顺序表的元素
	int length;				//顺序表当前长度
}SqList;					//顺序表的类型定义

//初始化表，构造一个空的线性表
SqList InitList();

//将线性表置空
void ClearList(SqList& L);

//销毁线性表
void DestroyList(SqList& L);

//判空操作，若为空，返回true，否则返回false
bool ListEmpty(SqList L);

//返回线性表元素个数
int ListLength(SqList L);

//按位查找，获取表中的第i个位置数据元素的值，用e返回
void GetElem(SqList L,int i,ElemType& e);

//按值查找,返回L中第1个值与e相同的元素在L中的位置，不存在，返回0
int LocateElem(SqList L, ElemType e, int(*myCompare)(ElemType, ElemType));

//按位插入操作，在表中指定元素e
void ListInsert(SqList& L,int i, ElemType e);

//按位删除，删除表中第i个位置上的元素，并用e返回删除元素的值
void ListDelete(SqList& L, int i, ElemType& e);

//遍历
void TraverseList(SqList L, void(*myPrint)(ElemType));