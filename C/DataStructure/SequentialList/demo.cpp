#include<stdio.h>
#include<stdlib.h>
#include"sequentialList.h"
#include"../DataStructure/myFunction.h"

void ListMenu() {
	printf("*******************************************\n");
	printf("**************静态分配顺序表***************\n");
	printf("**************0.退出***********************\n");
	printf("**************1.插入一组元素(测试元素)*****\n");
	printf("**************2.按值查找元素(返回索引)*****\n");
	printf("**************3.按位删除元素***************\n");
	printf("**************4.清空顺序表*****************\n");
	printf("**************10.遍历顺序表***************\n");
}

int main() {
	SqList personList = InitList();
	while (true)
	{
		ListMenu();
		printf("请输入您的选择：\n");
		int choice = intCin();
		switch (choice)
		{
		case 0://退出程序
			ExitSystem();
			break;
		case 1:{
			//顺序插入元素
			//准备好5个数据
			char personName_array[5][16] = { "亚瑟","王昭君","貂蝉","孙悟空","张飞" };
			int age_array[5] = { 38,23,16,22,43 };
			for (int i = 0; i < 5; i++)
			{
				ElemType e=create_struct(personName_array[i], age_array[i]);
				ListInsert(personList,1, e);
			}
			printf("成功插入一组测试数据！\n");
			system("pause");
			system("cls");
		}
			break;
		case 2: {
			//按值查找元素
			printf("请输入查找对象名字\n");
			char name[16];
			scanf_s("%15s", name, sizeof(name)); // 获取名字
			printf("请输入查找对象年龄\n");
			int age=intCin();
			//name[sizeof(name) - 1] = '\0';
			ElemType e=create_struct(name,age);
			printf("查找元素位置为：%d\n", LocateElem(personList, e, PersonCompare));
			system("pause");
			system("cls");
		}
			  break;
		case 3: {
			printf("请输入需要删除的元素位序！\n");
			int index=intCin();
			/*scanf_s("%d", &index);*/
			ElemType e=create_struct();
			ListDelete(personList, index, e);
			system("pause");
			system("cls");
		}
			break;
		case 4:
			ClearList(personList);
			printf("列表已清空！\n");
			system("pause");
			system("cls");
			break;
		case 10:
			TraverseList(personList,PersonPrint);
			system("pause");
			system("cls");
			break;
		default:
			printf("非法输入，请重新输入！\n");
			system("pause");
			system("cls");
			break;
		}
	}
	system("pause");
	return 0;
}