#include "sequentialList.h"

SqList InitList() {
	SqList L;				//构造一个空的顺序表L
	L.length = MaxSize;		//先让顺序表长度为最大
	ClearList(L);
	return L;
}

void ClearList(SqList& L) {
	for (int i = 0; i < L.length; i++)
	{
		ElemType e = create_struct();
		L.data[i] = e;
	}
	L.length = 0;	//空表长度为0
}

//在 C 语言中，静态数组的内存空间是在编译时分配的，静态数组的生命周期是整个程序执行期间
//因此，在程序执行过程中，静态数组一直处于存在的状态，不会自动销毁,也不需要自动销魂
void DestroyList(SqList& L) {
	ClearList(L);	//相当于清空顺序表
}

bool ListEmpty(SqList L) {
	if (L.length)
	{
		return true;
	}
	return false;
}

int ListLength(SqList L) {
	return L.length;
}

void GetElem(SqList L, int i, ElemType& e) {
	if (i<1 || i>L.length)
	{
		printf("位序不存在！，当前顺序表长度%d\n", L.length);
		return;
	}
	e = L.data[i-1];
}


int LocateElem(SqList L, ElemType e, int(*myCompare)(ElemType, ElemType)) {
	for (int i = 0; i < L.length; i++)
	{
		if (myCompare(L.data[i],e)) {
			return i+1;				//返回元素所在位置,位序从1开始
		}
	}
	return 0;
}

void ListInsert(SqList& L, int i, ElemType e) {
	if (L.length == MaxSize) {
		printf("顺序表已满！无法完成插入操作！\n");
		return;
	}
	if (i<1 || i>L.length + 1) {
		printf("位序不合法！，当前顺序表长度%d\n", L.length);
		return;
	}
	for (int j = L.length; j >= i; j--)
	{
		L.data[j] = L.data[j-1];
	}
	L.data[i-1] = e;		//减1为数组索引
	L.length += 1;
}

void ListDelete(SqList& L, int i, ElemType& e) {
	if (i>L.length || i < 1)
	{
		printf("这不是一个有效位置，顺序表当前长度:%d\n", L.length);
		return;
	}
	e = L.data[i-1];			//用e返回,位序从1开始
	while (i < L.length)
	{
		L.data[i-1] = L.data[i];//用后面的覆盖前面
		i++;
	}
	L.length --;
	printf("删除成功！删除信息如下：\n");
	PersonPrint(e);
}

void TraverseList(SqList L, void(*myPrint)(ElemType)) {
	printf("\n");
	printf("当前顺序表长度为：%d，遍历如下：\n", L.length);
	for (int i = 0; i < L.length; i++)
	{
		myPrint(L.data[i]);
	}
}