#pragma once //防止头文件重复包含
#include<stdio.h>
#include<string.h>

struct Node
{
	char info;			//结点信息
	int weight;			//权重
};

//定义一个比较函数,不等返回0,相等为真
bool NodeCompare(Node e1, Node e2);

//定义一个输出函数，用于输出Node信息
void NodePrint(Node e);

// 定义结构体时提供默认值
Node create_struct();

Node create_struct(char info, int weight);