#include"Person.h"
#include"myFunction.h"

int PersonCompare(Person e1, Person e2) {
	//strcmp(e1.name, e2.name)两个字符串相等，则返回值为 0
	return e1.age == e2.age && !strcmp(e1.name, e2.name);
}

void PersonPrint(Person e) {
	printf("姓名：%s\t年龄：%d\n", e.name, e.age);
}

Person create_struct() {
	Person e = { NULL, 0 };
	return e;
}

Person create_struct(const char* name, int age) {
	Person e = { "", 0 };
	// 将传入的参数赋值给结构体成员
	if (name != NULL) {
		//后面赋值给前面
		strncpy_s(e.name, name, sizeof(e.name) - 1);
	}
	e.age = age;
	return e;
}

Person create_Input() {
	printf("请输入对象的的姓名！\n");
	char name[16];
	scanf_s("%15s", name, sizeof(name));		// 获取名字
	printf("请输入对象的的年龄！\n");
	int age = intCin();							//获取年龄
	Person e = create_struct(name, age);
	return e;
}