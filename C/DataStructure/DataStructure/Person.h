#pragma once //防止头文件重复包含
#include<stdio.h>
#include<string.h>

/*
* 定义一个结构体，构造好如下函数
* 1.定义一个比较函数,用于比较结构体,不等返回0,相等为真		__Compare
* 2.定义一个输出函数，用于输出结构体，有点像重写toString	__Print
* 3.定义类似构造函数的函数									create_struct
//在 C 语言中，结构体本身并不支持构造函数的概念，必须通过手动初始化结构体变量来赋值
//不过，可以通过一些技巧来实现类似构造函数的功能
*/

struct Person
{
	char name[16];
	int age;
};

typedef Person ElemType;
//typedef int ElemType;

//定义一个比较函数,不等返回0,相等为真
int PersonCompare(Person e1, Person e2);

//定义一个输出函数，用于输出Person信息
void PersonPrint(Person e);

// 定义结构体时提供默认值
Person create_struct();

Person create_struct(const char* name, int age);

//输入方式创造结构体
Person create_Input();