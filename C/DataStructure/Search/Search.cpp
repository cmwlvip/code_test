#include"Search.h"

int Search_Seq(SSTable ST, KeyType key) {
	//在顺序表ST中顺序查找其关键字等于key的数据元素。若找到，则返回该元素在表中的位置，否则返回0
	for (int i = ST.length; i >=1; i--)
	{
		if (ST.R[i].key==key)			//从后往前找
		{
			return i;
		}
	}
	return 0;
}

int Search_SeqList(SSTable ST, KeyType key) {
	//在顺序表ST中顺序查找其关键字等于key的数据元素。若找到，则返回该元素在表中的位置，否则返回0
	ST.R[0].key = key;
	int i = ST.length;
	for (i; ST.R[i].key != key; i--){}		//从后往前找
	return i;
}

int Search_Bin(SSTable ST, KeyType key) {
	//在顺序表ST中顺序查找其关键字等于key的数据元素。若找到，则返回该元素在表中的位置，否则返回0
	int low = 1;
	int high = ST.length;					//设置查找区间初值
	int mid;
	while (low<=high)
	{
		mid = (low + high) / 2;
		if (key==ST.R[mid].key)
		{
			return mid;						//找到待查元素
		}
		else if (key<ST.R[mid].key)
		{
			high = mid - 1;					//继续在前一子表进行查找
		}
		else
		{
			low = mid + 1;					//继续在后一子表进行查找
		}
	}
	return 0;								//表中不存在待查元素
}

BSTree SearchBST(BSTree T, KeyType key) {
	//在根指针T所指二叉排序树中递归地查找某关键字等于key的数据元素
	//若查找成功，则返回指向该数据元素结点的指针，否则返回空指针
	if ((!T) || key == T->data.key)
	{
		return T;							//查找结束
	}
	else if (key<T->data.key)
	{
		return SearchBST(T->lchild, key);	//在左子树中继续查找
	}
	else
	{
		return SearchBST(T->rchild, key);	//在右子树中继续查找
	}
}

void InsertBST(BSTree& T, ElemType e) {
	//当二叉排序树T中不存在关键字等于e.key的数据元素时，则插入该元素
	if (!T)
	{
		BSTNode* S = (BSTNode*)malloc(sizeof(BSTNode));		//生成新结点*S
		if (S==NULL)
		{
			return;
		}
		S->data = e;										//新结点*S的数据域置为e
		S->lchild = S->rchild = NULL;						//新结点*S作为叶子结点
		T = S;												//把新结点*S链接到已找到的插入位置
	}
	else if (e.key < T->data.key)
	{
		InsertBST(T->lchild, e);							//将*S插入左子树
	}
	else if (e.key > T->data.key)
	{
		InsertBST(T->rchild, e);							//将*S插入右子树
	}
}

void CreatBST(BSTree& T) {
	//依次读入一个关键字为key的结点，将此结点插入二叉排序树T中
}

void CreatBST(BSTree& T, KeyType str[], int n) {
	T = NULL;					//将二叉排序树初始化为空树
	int i = 0;
	while (i<n)
	{
		ElemType e;
		e.key = str[i];
		InsertBST(T, e);
	}
}

void DeleteBST(BSTree& T, KeyType key)  {
	//从二叉排序树中删除关键字等于key的结点
	BSTNode* p = T;								//初始化
	BSTNode* f = NULL;
	/*---下面的while循环从根开始查找关键字等于key的结点*p---------*/
	while (p)
	{
		if (p->data.key==key)
		{
			break;								//找到关键字等于key的结点*p，结束循环
		}
		f = p;									//*f为*p的双亲结点
		if (p->data.key>key)
		{
			p = p->lchild;						//在*p的左子树中继续查找
		}
		else
		{
			p = p->rchild;						//在*p的右子树继续查找
		}
	}
	if (!p)
	{
		return;									//找不到被删除结点则返回
	}
	BSTNode* q = p;
	/*----考虑3种情况实现p所指子树内部的处理：*p左右子树均不空，无右子树，无左子树------*/
	if ((p->lchild) && (p->rchild))
	{
		BSTNode* s = p->lchild;
		while (s->rchild)						//在*p的左子树中继续查找其【前驱】结点，即最右下结点
		{
			q = s;
			s = s->rchild;						//向右到尽头
		}
		p->data = s->data;						//s即指向被删结点的“前驱”
		if (q!=p)
		{
			q->rchild = s->lchild;				//重接*q的右子树
		}
		else {
			q->lchild = s->lchild;				//重接*q的左子树
		}
		free(s);
		return;
	}
	else if (!p->rchild)						//被删结点*p无右子树，只需重接其左子树
	{
		p = p->lchild;
	}
	else if (!p->lchild)						//被删结点*p无左子树，只需重接其右子树
	{
		p = p->rchild;
	}
	/*------将p所指的子树挂接到其双亲结点*f相应的位置----------*/
	if (!f)
	{
		T = p;									//被删结点为根结点
	}
	else if (q==f->lchild)
	{
		f->lchild = p;							//挂接到*f的左子树位置
	}
	else
	{
		f->rchild = p;							//挂接到*f的右子树位置
	}
	free(q);
}