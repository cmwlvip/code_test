#pragma once
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef int KeyType;
typedef char InfoType[16];

//数据元素类型
typedef struct {
	KeyType key;						//关键字域
	InfoType otherinfor;				//其他域
}ElemType;
//顺序表的定义
typedef struct {
	ElemType* R;						//存储空间地址
	int length;							//当前长度
}SSTable;

//------二叉排序树(Binary Sort Tree)的二叉链表存储表示--------
typedef struct BSTNode {
	ElemType data;						//每个结点的数据域包括关键字项和其他数据项
	struct BSTNode* lchild, * rchild;	//左右孩子指针
}BSTNode,*BSTree;

//顺序查找
int Search_Seq(SSTable ST, KeyType key);

//设置监视哨的顺序查找
int Search_SeqList(SSTable ST, KeyType key);

//折半查找
int Search_Bin(SSTable ST, KeyType key);

//二叉排序树的递归查找
BSTree SearchBST(BSTree T, KeyType key);

//二叉排序树的插入
void InsertBST(BSTree& T, ElemType e);

//二叉排序树的创建
void CreatBST(BSTree& T);
void CreatBST(BSTree& T,KeyType str[],int n);

//二叉排序树的删除
void DeleteBST(BSTree& T, KeyType key);




//B-树的查找


//B-树的插入


//散列表的查找