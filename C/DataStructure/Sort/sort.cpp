#include"sort.h"

void SLListInit(SLList& L, KeyType keys[], int len, int keynum) {
	L.keynum = keynum;
	L.length = len;
	for (int i = 1; i <= len; i++)
	{
		L.r[i].keys[2] = keys[i] / 100;
		L.r[i].keys[1] = keys[i] % 100 / 10;
		L.r[i].keys[0] = keys[i] % 10;
	}
}

void TraverseArray(ElemType A[], int n) {
	for (int i = 1; i < n + 1; i++)
	{
		printf("%d\t", A[i]);
	}
	printf("\n");
}

void InsertSort(ElemType A[], int n) {
	int i, j;
	for (i = 2; i <= n; i++)					//依次将A[2]~A[n]插入前面已排序序列
	{
		if (A[i] < A[i - 1])					//若A[i]的关键码小于其前驱，将A[i]插入有序表
		{
			A[0] = A[i];						//复制为哨兵，A[0]不存放元素
			for (j = i - 1; A[0] < A[j]; j--)	//从后往前查找待插入的位置
			{
				A[j + 1] = A[j];				//向后挪位
			}
			A[j + 1] = A[0];					//复制到插入位置
		}
	}
}

void BinaryInsertSort(ElemType A[], int n) {
	int i, j, low, high, mid;
	for (i = 2; i <= n; i++)					//依次将A[2]~A[n]插入前面已排序序列
	{
		A[0] = A[i];							//将待插入的记录暂存到监视哨中
		low = 1;
		high = i - 1;							//设置折半查找的范围
		while (low <= high)						//在[low...high]中折半查找插入的位置
		{
			mid = (low + high) / 2;				//取中间点
			if (A[0] < A[mid])
			{
				high = mid - 1;					//插入点在前一子表
			}
			else
			{
				low = mid + 1;					//插入点在后一子表
			}
		}
		for (j = i - 1; j >= high + 1; j--)
		{
			A[j + 1] = A[j];
		}
		A[high + 1] = A[0];
	}
}

void ShellInsert(ElemType A[], int n,int dk) {		//对顺序表做一趟增量是dk的希尔插入排序
	int i, j;
	for (i = dk+1; i <=n; i++)
	{
		if (A[i]<A[i-dk])							//需要将A[i]插入有序增量子表（后面小于前面）
		{
			A[0] = A[i];							//暂存在A[0]
			for (j = i-dk; j >0 && A[0]<A[j] ; j-=dk)
			{
				A[j + dk] = A[j];					//向后挪位，留出插入位置
			}
			A[j + dk] = A[0];						//将A[0]即原A[i]插入到正确位置
		}
	}
}

void ShellSort(ElemType A[], int n) {
	for (int dk = n/2; dk >=1 ; dk=dk/2)			//增量变化无规定，如可以dk--
	{
		ShellInsert(A, n, dk);
	}
}

void BubbleSort(ElemType A[], int n) {				//实现从小到大，冒泡小的
	for (int i = 1; i <n; i++)
	{
		bool flag = false;							//用来标记某一趟排序是否发生交换
		for (int j = n; j >i; j--)
		{
			if (A[j-1]>A[j])
			{
				A[0] = A[j];						//位置不对交换
				A[j] = A[j - 1];
				A[j - 1] = A[0];
				flag = true;
			}
		}
		if (!flag)
		{
			return;									//本趟遍历没有发生交换，说明表已经有序
		}
	}
}

//对顺序表中的子表[low...high]进行一趟排序，返回枢轴位置
int Partition(ElemType A[],int low,int high) {
	ElemType pivot = A[low];			//将当前表中的第一个元素设为枢轴，对表进行划分
	while (low<high)
	{
		while (low<high && A[high]>=pivot)
		{
			high--;
		}
		A[low] = A[high];				//将比枢轴小的元素移动到左端(用小的元素覆盖枢轴)
		while (low<high && A[low]<=pivot)
		{
			low++;
		}
		A[high] = A[low];				//将比枢轴大的元素移动到右端
	}
	A[low] = pivot;						//枢轴元素存放到最终位置
	return low;							//返回存放枢轴的最终位置
}	

//调用前置初值如1...n
void QSort(ElemType A[], int low, int high) {
	if (low<high)									//确保长度大于1
	{
		int pivotpos = Partition(A, low, high);		//将[low...high]一分为二，确定枢轴位置
		QSort(A, low, pivotpos - 1);				//对左子表递归排序
		QSort(A, pivotpos + 1, high);				//对右子表递归排序
	}
}

void QuickSort(ElemType A[],int n) {
	QSort(A, 1, n);
}

void SelectSort(ElemType A[], int n) {
	for (int i = 1; i < n; i++)					//一共进行n-1趟
	{
		int min = i;							//记录最小元素位置
		for (int j = i+1; j <= n; j++)
		{
			if (A[j]<A[min])					//找到最小元素
			{
				min = j;
			}
		}
		if (min!=i) {							//交换
			A[0] = A[i];
			A[i] = A[min];
			A[min] = A[0];
		}
	}
}

//调整为大根堆（假设[s+1...m]已经是堆，将[s...m]重新调整为大根堆）【即s“下坠”】
void HeapAdjust(ElemType A[], int s,int m) {
	A[0] = A[s];							//A[0]暂存子树根结点
	for (int i = 2*s; i <=m; i*=2)			//沿着key较大的孩子结点向下筛选
	{
		if (i<m && A[i]<A[i+1])				//i为较大孩子结点下标(找到最大孩子)
		{
			i++;
		}
		if (A[0]>=A[i])							//满足大根堆
		{
			break;								//就是应该插在s上
		}
		A[s] = A[i];							//将大的叶子结点调整至双亲位置
		s = i;									//继续向下筛选（叶子变双亲）
	}
	A[s] = A[0];								//插入，元素“下坠完成”
}

void CreatMaxHeap(ElemType A[],int n) {			//把无序序列建成大根堆
	for (int i = n/2; i > 0; i--)
	{
		HeapAdjust(A, i, n);					//反复调用HeapAdjust,从i=[n/2]~1
	}
}

void HeapSort(ElemType A[], int n) {
	CreatMaxHeap(A, n);							//把无序序列建成大根堆
	for (int i = n;  i>1; i--)					//n-1趟交换和建堆过程（堆长逐渐减小）
	{
		A[0] = A[i];							//堆顶于最后一个元素交换
		A[i] = A[1];	//换成max
		A[1] = A[0];
		HeapAdjust(A, 1, i - 1);				//[1...i-1]重新调成堆
	}
}

//王道数据结构
ElemType B[9] = {0};							//辅助数组B
//相邻两个有序子序列的归并
void Merge(ElemType A[],int low,int mid,int high) {
	//将有序表R[low...mid]和R[mid+1...high]归并为有序表T[row...high]
	int i=low, j=mid+1, k=low;
	for (int x = low; x <= high; x++)
	{
		B[x] = A[x];							//将R中所有元素复制到B
	}
	while (i <= mid && j <= high)
	{
		if (B[i] <= B[j])
		{
			A[k++] = B[i++];
		}
		else
		{
			A[k++] = B[j++];
		}
	}
	while (i<=mid)
	{
		A[k++] = B[i++];						//将剩余的T[i...mid]复制到R
	}
	while (j<=high)
	{
		A[k++] = B[j++]; 						//将剩余的T[j...high]复制到R
	}
}
void MergeSort(ElemType A[], int low, int high) {
	if (low < high) {
		int mid = (low + high) / 2;				//从中间划分两个子序列
		MergeSort(A, low, mid);					//对左侧子序列进行递归排序
		MergeSort(A, mid + 1, high);			//对右侧子序列进行递归排序
		Merge(A, low, mid, high);				//归并
	}
}

//严蔚敏版
void Merge(ElemType R[], ElemType T[], int low, int mid, int high) {
	//将有序表R[low...mid]和R[mid+1...high]归并为有序表T[row...high]
	int i = low, j = mid + 1, k = low;
	while (i <= mid && j <= high)
	{
		if (R[i] <= R[j])
		{
			T[k++] = R[i++];
		}
		else {
			T[k++] = R[j++];
		}
	}
	while (i <= mid)
	{
		T[k++] = R[i++];
	}
	while (j <= high) {
		T[k++] = R[j++];
	}
}
void MSort(ElemType R[], ElemType T[], int low, int high) {
	if (low == high)
	{
		T[low] = R[low];				//直接放入T
	}
	else {
		ElemType* S = (ElemType*)malloc((high + 1) * sizeof(ElemType));
		int mid = (low + high) / 2;			//将序列一分为二
		MSort(R, S, low, mid);				//对子序列R[low..mid]递归归并排序，结果放入S[low...mid]
		MSort(R, S, mid + 1, high);			//对子序列R[mid+1..high]递归归并排序，结果放入S[mid+1..high]
		Merge(S, T, low, mid, high);		//将S[low..mid]和S[mid+1..high]归并到T[low..high]
		free(S);
	}
}
void MergeSort(ElemType R[],int n) {
	//对数组做归并排序
	MSort(R, R, 1, n);
}


void Distribute(SLList& L,int i, ArrType& f, ArrType& e) {
	//按第i个关键字排序
	// 本算法按第i个关键字keys[i]建立RADIX个子表，使同一子表中记录的keys[i]相同
	//【first】f[0...radix-1]指向各子表对应的第一个元素
	//【end】e[0...radix-1]指向各子表对应的最后一个元素
	for (int j = 0; j < RADIX; j++)
	{
		f[j] = 0;								//各子表初始化为空表
	}
	for (int p = L.r[0].next; p ; p=L.r[p].next)
	{
		int key = L.r[p].keys[i];				//将记录中的第i个关键字记录
		if (!f[key])							//f为空
		{
			f[key] = p;							//说明此记录必为【对应关键字key值子表】第一个元素
		}
		else									//非空
		{
			L.r[e[key]].next = p;				//让子表中最后一个记录的next指向新来记录
		}
		e[key] = p;								//把p所指结点插入第key个子表（即最后一个记录更新为新来记录）
	}
}
void Collect(SLList& L, int i, ArrType f, ArrType e) {
	//本算法按keys[i]从小到大地将f[0...radix-1]所指各子表依次连接成一个链表
	//【end】e[0...radix-1]为各子表地尾指针
	int j;
	for (j = 0; !f[j]; j++) {}					//找第一个非空子表
	L.r[0].next = f[j];							//r[0].next指向第一个非空子表中的第一个结点
	int t = e[j];
	while(j < RADIX) {
		for (j = j + 1; j < RADIX - 1 && !f[j]; j++) {}	//找下一个非空子表
		if (j != RADIX && f[j])					//f[RADIX]会越界
		{
			L.r[t].next = f[j];
			t = e[j];							//链接两个非空子表
		}
	}
	L.r[t].next = 0;							//t指向最后一个非空子表中的最后一个结点
}
void RadixSort(SLList& L) {
	ArrType f, e;
	//L是采用静态链表表示的顺序表
	//对L做基数排序，使得L成为按关键字从小到大的有序静态链表，L.r[0]为头结点
	for (int i = 0; i < L.length; i++)
	{
		L.r[i].next = i + 1;
	}
	L.r[L.length].next = 0;						//将L改造为静态链表【初始化】
	for (int i = 0; i < L.keynum; i++)
	{
		Distribute(L, i, f, e);					//第i趟分配
		Collect(L, i, f, e);					//第i趟收集
	}
}