#pragma once
#include<stdio.h>
#include <stdlib.h>
typedef int ElemType;

//遍历数组前n个元素
void TraverseArray(ElemType A[], int n);

//直接插入排序
void InsertSort(ElemType A[], int n);

//折半插入排序
void BinaryInsertSort(ElemType A[], int n);

//对顺序表做一趟增量是dk的希尔插入排序
void ShellInsert(ElemType A[],int n, int dk);

//希尔排序
void ShellSort(ElemType A[], int n);

//冒泡排序
void BubbleSort(ElemType A[], int n);

//快速排序
void QuickSort(ElemType A[], int n);

//选择排序
void SelectSort(ElemType A[], int n);

//堆排序
void HeapSort(ElemType A[], int n);

//归并排序
void MergeSort(ElemType R[], int n);

void MergeSort(ElemType A[], int low, int high);


//以下用于【基数排序】
#define MAXNUM_KEY 8					//关键字项数(比如【个位、十位】或【花色、数字】)最大值
#define RADIX 10						//关键字基数(radix)，此时是十进制整数的基数
#define MAXSIZE 100						//待比较元素个数

typedef int KeyType;

typedef struct {
	KeyType keys[MAXNUM_KEY];			//待比较关键字
	char otherItems[16];				//其他数据项
	int next;
}SLCell;								//静态链表结点类型

typedef struct {
	SLCell r[MAXSIZE];					//静态链表可利用的空间，r[0]为头结点
	int keynum;							//记录待比较关键字个数(三位数有【个、十、百位】三项)
	int length;							//静态链表当前长度
}SLList;								//静态链表类型

typedef int ArrType[RADIX];				//数组类型

//静态链表初始化，用于基数排序
void SLListInit(SLList& L, KeyType keys[], int len, int keynum);

//基数排序
void RadixSort(SLList& L);