#pragma once
#include<stdio.h>
#include <stdlib.h>

typedef char InfoType[16];
//严蔚敏版
#define MAXSIZE 20			//顺序表的最大长度
typedef int KeyType;		//定义关键字类型为整形
typedef struct {
	KeyType key;			//关键字项
	InfoType otherinfo;		//其他数据项
}ElemType;					//记录类型record(RedType)
typedef struct {
	ElemType r[MAXSIZE + 1];	//r[0]闲置或用作哨兵单元
	int length;					//顺序表长度
}Sqlist;						//顺序表类型(静态分配)