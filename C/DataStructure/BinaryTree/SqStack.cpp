#include"SqStack.h"//���ڡ���������

bool InitStack(SqStack& S) {					//Ϊ˳���
	S.base = (SElemType*)malloc(sizeof(SElemType) * MAXSIZE);
	if (!S.base)
	{
		return false;							//�洢����ʧ��
	}
	S.top = S.base;								//top��ʼΪbase����ջ
	S.stacksize = MAXSIZE;						//����ջ�������
	return true;
}

bool Push(SqStack& S, SElemType e) {
	if (S.top-S.base==S.stacksize)
	{
		return false;							//ջ��
	}
	*S.top++ = e;								//Ԫ��ѹ��ջ����ջ��ָ���1
	return true;
}

bool Pop(SqStack& S, SElemType& e){
	//ɾ��S��ջ��Ԫ�أ���e������ֵ
	if (S.top==S.base)
	{
		return false;							//ջ��
	}											//ջ��ָ���1����ջ��Ԫ�ظ���e
	e = *--S.top;
	return true;
}

bool GetTop(SqStack S, SElemType& e) {
	//����S��ջ��ָ�룬���޸�ջ��ָ��
	if (S.top != S.base)							//ջ�ǿ�
	{
		e = *(S.top - 1);
		return true;
	}
	return false;
}

bool StackEmpty(SqStack S) {
	if (S.top == S.base)
	{
		return true;				//ջ��
	}
	return false;
}