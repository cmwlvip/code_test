#include<stdio.h>
#include <stdlib.h>
#include"BiTree.h"
#include"../DataStructure/myFunction.h"

void ListMenu() {
	printf("*******************************************\n");
	printf("**************菜单*************************\n");
	printf("**************0.退出***********************\n");
	printf("**************1.先序遍历的顺序建立二叉链表**\n");
	printf("**************2.哈夫曼树的使用*************\n");
	printf("**************3.***************************\n");
	printf("**************10.*************************\n");
}

int main() {
	BiTree T;
	while (true)
	{
		ListMenu();
		printf("请输入您的选择：\n");
		int choice = intCin();
		switch (choice)
		{
		case 0:
			ExitSystem();//退出程序
			break;
		case 1: {
			CreateBiTree(T);
			InOrderTraverse2(T, NodePrint);
			system("pause");
			system("cls");
		}
			  break;
		case 2: {
			HuffmanTree HT;
			printf("请输入权值个数：\n");
			int n = intCin();
			GreateHuffmanTree(HT,n);
			printf("%d", GetPower(HT, n));
			system("pause");
			system("cls");
		}
			  break;
		case 3: {
			system("pause");
			system("cls");
		}
			  break;
		default:
			printf("非法输入，请重新输入！\n");
			system("pause");
			system("cls");
			break;
		}
	}
	return 0;
}