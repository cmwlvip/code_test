#pragma once
#include<stdio.h>
#include<stdlib.h>
#include"../DataStructure/Node.h"

typedef Node TElemType;

//----- 二叉树的【二叉链表】存储表示------
typedef struct BiTNode {
	TElemType data;							//结点的数据域
	struct BiTNode* lchild, * rchild;		//左右孩子指针
}BiTNode,*BiTree;


//中序遍历的递归算法
void InOrderTraverse(BiTree T, void(*myPrint)(TElemType));

//中序遍历的非递归算法
void InOrderTraverse2(BiTree T, void(*myPrint)(TElemType));

//先序遍历的顺序建立二叉链表
void CreateBiTree(BiTree& T);

//复制二叉树
void Copy(BiTree T, BiTree& NewT);

//计算二叉树的深度
int Depth(BiTree T);

//计算二叉树中结点的个数
int NodeCount(BiTree T);

//【线索二叉树】(Threaded Binary Tree)
// 【LTag】0 lchild指示结点的左孩子；1 指示结点前驱
// 【RTag】0 rchild指示结点的右孩子；2 指示结点后继
//-----二叉树的【二叉线索】存储表示--------
typedef struct BiThrNode 
{
	TElemType data;
	struct BiThrNode* lchild, * rchild;			//左右孩子指针
	int LTag, RTag;								//左右标记
}BiThrNode,*BiThrTree;

//以结点p为根的子树中序线索化
void InThreading(BiThrTree& p,BiThrTree& pre);

//带头结点的二叉树中序线索化
void InOrderThreading(BiThrTree& Thrt,BiThrTree& T);

//遍历中序线索二叉树
void InOrderTraverse_Thr(BiThrTree T, void(*myPrint)(TElemType));

//------哈夫曼树的存储表示---------
typedef struct
{
	int weight;						//结点权值
	int parent, lchild, rchlid;		//结点双亲 左孩子 右孩子下标
}HTNode, * HuffmanTree;				//动态分配数组存储哈夫曼树


void GreateHuffmanTree(HuffmanTree& HT,int n);

int GetPower(HuffmanTree HT, int n);