#include"BiTree.h"
#include"SqStack.h"
#include"../DataStructure/myFunction.h"

void InOrderTraverse(BiTree T, void(*myPrint)(TElemType)) {
	//中序遍历二叉树T的递归算法
	if (T)										//若二叉树非空
	{
		InOrderTraverse(T->lchild, myPrint);	//中序遍历左子树
		myPrint(T->data);						//访问根节点
		InOrderTraverse(T->rchild, myPrint);	//中序遍历右子树
	}
}

void InOrderTraverse2(BiTree T, void(*myPrint)(TElemType)) {
	//中序遍历的非递归算法需要用到栈
	SqStack S;
	InitStack(S);
	BiTNode* p = T;
	BiTNode* q = (BiTNode*)malloc(sizeof(BiTNode));			//申请结点空间用于存放栈顶弹出的元素
	while (p || !StackEmpty(S))
	{
		if (p)
		{
			Push(S, p);									//根指针进栈
			p = p->lchild;								//遍历左子树
		}
		else{											//p为空
			Pop(S, q);									//退栈
			myPrint(q->data);							//访问根节点
			p = q->rchild;								//遍历右子树
		}
	}
}

void CreateBiTree(BiTree& T) {
	//按照先序次序输入二叉树中结点的值（一个字符），创建二叉链表示的二叉树
	printf("请输入结点信息！\n");
	char ch=getchar();			//获取结点信息
	while (getchar() != '\n') continue;
	if (ch=='#')
	{
		T = NULL;								//递归结束，建空树
	}
	else {										//递归创建二叉树
		T = (BiTNode*)malloc(sizeof(BiTNode));	//生成根结点
		if (T==NULL)
		{
			return;
		}
		T->data.info = ch;						//根结点数据域置为ch
		CreateBiTree(T->lchild);				//递归创建左子树
		CreateBiTree(T->rchild);				//递归传教又子树
	}
}

void Copy(BiTree T, BiTree& NewT) {
	//复制一颗和T完全相同的二叉树
	if (T==NULL)								//如果是空树，递归结束
	{
		NewT = NULL;
		return;
	}
	else {
		NewT = (BiTNode*)malloc(sizeof(BiTNode));
		if (NewT==NULL)
		{
			return;
		}
		NewT->data = T->data;					//复制根结点
		Copy(T->lchild, NewT->lchild);			//递归复制左子树
		Copy(T->rchild, NewT->rchild);			//递归复制右子树
	}
}

int Depth(BiTree T) {
	//计算二叉树的深度
	if (T==NULL)
	{
		return 0;						//如果是空树，深度是0，递归结束
	}
	else {
		int m = Depth(T->lchild);		//递归计算左子树的深度记为m
		int n = Depth(T->rchild);		//递归计算右子树的深度记为n
		if (m>n)
		{
			return m + 1;				//二叉树的深度为m与n的较大者加1
		}
		else
		{
			return n + 1;				//显然,计算二叉树的深度是在后续遍历二叉树的基础上进行计算的
		}
	}
}

int NodeCount(BiTree T) {
	//统计二叉树T中结点个数
	if (T==NULL)
	{
		return 0;				//空树，结点个数为0，递归结束
	}
	else
	{
		return NodeCount(T->rchild) + NodeCount(T->rchild) + 1;
		//否则结点个数为左子树的结点个数+右子树的结点个数+1
	}
}

void InThreading(BiThrTree& p, BiThrTree& pre) {
	if (p)
	{
		InThreading(p->lchild,pre);					//左子树递归线索化
		if (!p->lchild)							//p的左孩子为空
		{
			p->LTag = 1;						//给p加上左线索
			p->lchild = pre;					//p的左孩子指针指向pre（前驱）
		}
		else
		{
			p->LTag = 0;
		}
		if (!pre->rchild)
		{
			pre->RTag = 1;						//给pre加上右线索
			pre->rchild = p;					//pre的右孩子指向p（后继）
		}
		else {
			pre->RTag = 0;						
		}
		pre = p;								//保持pre指向p的前驱
		InThreading(p->rchild,pre);					//右子树递归线索化
	}
}

void InOrderThreading(BiThrTree& Thrt, BiThrTree& T) {
	//中序遍历二叉树T,并将其中序线索化，Thrt指向头结点
	Thrt = (BiThrNode*)malloc(sizeof(BiThrNode));			//建头结点
	if (Thrt==NULL)
	{
		return;
	}
	Thrt->LTag = 0;			//头结点有左孩子，若树非空，则其左孩子为树根
	Thrt->RTag = 1;			//头结点的右孩子指针为右线索
	Thrt->rchild = Thrt;	//初始化时右指针指向自己
	if (!T)
	{
		Thrt->lchild = Thrt;	//若树为空，则左指针也指向自己
	}
	else
	{
		Thrt->lchild = T;
		BiThrTree pre = Thrt;	//头结点左孩子指向根，pre初值指向头结点
		InThreading(T, pre);	//对以T为根的二叉树进行中序线索化
		pre->rchild = Thrt;		//算法结束后，pre为最右结点，pre的右线索指向头结点
		pre->RTag = 1;
		Thrt->rchild = pre;		//头结点的右线索指向pre
	}
}

void InOrderTraverse_Thr(BiThrTree T, void(*myPrint)(TElemType)) {
	//T指向头结点，头结点的左链指向根结点
	//中序遍历二叉索引树T的非递归算法，对每个元素直接输出
	BiThrNode* p = T->lchild;		//p指向根结点
	while (p!=T)					//空树或者遍历结束时，p==T
	{
		while (p->RTag==0)
		{
			p = p->lchild;			//沿左孩子向下
			myPrint(p->data);		//访问左子树为空的结点
			while (p->RTag == 1 && p->rchild != T)
			{
				p = p->rchild;		//沿右线索访问后继结点
			}
			p = p->rchild;			//转向右子树
		}
	}
}

void Select(HuffmanTree HT, int n, int& s1,int& s2) {
	int i = 1;
	while (i <= n) {
		if (HT[i].parent==0)
		{
			s1 = i;
			s2 = i;
			break;
		}
		i++;
	}
	i = i + 1;
	while (i <= n)
	{
		if (HT[i].parent == 0)
		{
			if (HT[i].weight < HT[s1].weight)		//找到更小的
			{
				s2 = s1;
				s1 = i;
			}
			else if (s1 == s2 || (HT[i].weight < HT[s2].weight))
			{
				s2 = i;
			}
		}
		i++;
	}
}

void GreateHuffmanTree(HuffmanTree& HT, int n) {
	//构造哈夫曼树HT
	if (n<=1)
	{
		return;
	}
	int m = 2 * n - 1;
	//由于哈夫曼树中没有度为1的结点，则一颗有n个叶子结点的哈夫曼树共2n-1个度为1的结点
	HT = (HTNode*)malloc(sizeof(HTNode) * (m + 2));
	//0号单元未用，所以需要动态分配m+1个单元，HT[m]表示根结点
	if (HT==NULL)
	{
		return;
	}
	//将1~m号单元中的双亲、右孩子、左孩子的下标都初始化为0
	for (int i = 1; i <= m; i++)
	{
		HT[i].parent = 0;
		HT[i].lchild = 0;
		HT[i].rchlid = 0;
	}
	//输入前n个单元中叶子结点的权值
	printf("请依次输入结点权值用于构造哈夫曼树！\n");
	for (int i = 1; i <=n; i++)
	{
		HT[i].weight = intCin();
	}
	/*-----------初始化工作完成，下面开始创建哈夫曼树------------*/
	for (int i = n + 1; i <= m; i++)
	{//通过n-1次的选择、删除、合并来创建哈夫曼树
		//在HT[k](1<=k<=i-1)中选择两个其双亲域为0且权值最小的结点，返回它们在HT中的序号s1和s2
		int s1, s2;
		Select(HT, i - 1, s1, s2);
		//得到新结点i,从森林中删除s1，s2（双亲域改了也就删除了），将s1和s2的双亲域由0改为i
		HT[s1].parent = i;
		HT[s2].parent = i;
		HT[i].lchild = s1;
		HT[i].rchlid = s2;									//s1、s2分别作为i的左右孩子
		HT[i].weight = HT[s1].weight + HT[s2].weight;		//i的权值为左右孩子权值之和
	}
}

int GetPower(HuffmanTree HT, int n) {
	int sum = 0;
	for (int i = 1; i <= (2 * n - 2); i++)
	{
		sum += HT[i].weight;
	}
	printf("%d\n", sum);
	return sum;
}