#pragma once
#include<stdio.h>
#include<stdlib.h>
#include"../DataStructure/Person.h"

typedef Person QElemType;
#define MAXSIZE 100

//----队列的顺序存储结构------
typedef struct
{
	QElemType* base;				//存储空间的基地址
	int front;						//头指针
	int rear;						//尾指针
}SqQueue;

//循环队列的初始化
bool InitQueue(SqQueue& Q);

//求循环队列的长度
int QueueLength(SqQueue Q);

//循环队列的入队
bool EnQueue(SqQueue& Q, QElemType e);

//循环队列的出队
bool DeQueue(SqQueue& Q, QElemType& e);

//取循环队列的队头元素
bool GetHead(SqQueue Q,QElemType& e);