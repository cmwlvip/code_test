#pragma once
#include<stdio.h>
#include<stdlib.h>
#include"../DataStructure/Person.h"

//--------链栈的存储结构--------
typedef struct StackNode
{
	ElemType data;
	struct StackNode* next;
}StackNode,*LinkStack; 

//由于栈的主要操作是在栈顶插入和删除元素，显然以链表的头部作为栈顶是最方便的
//而且没有必要像单链表那样为了操作方便附加【头结点】

//链栈的初始化
void InitStack(LinkStack& S);

//链栈的入栈
bool Push(LinkStack& S,ElemType& e);

//链栈的出栈
bool Pop(LinkStack& S,ElemType& e);

//取栈顶元素
bool GetTop(LinkStack S, ElemType& e);