#include"LinkStack.h"

void InitStack(LinkStack& S) {
	//构造一个空栈S,栈顶指针置空(一定能成功)
	S = NULL;
}

bool Push(LinkStack& S, ElemType& e) {
	//在栈顶插入元素e
	StackNode* p = (StackNode*)malloc(sizeof(StackNode));		//生成新的结点
	if (p==NULL)
	{
		return false;
	}
	p->data = e;					//将新结点的数据域设置为e
	p->next = S;					//将新结点插入栈顶
	S = p;							//修改栈顶指针为p
	return true;
}

bool Pop(LinkStack& S,ElemType& e) {
	//删除栈顶元素，用e返回其值
	if (S==NULL)
	{
		return false;				//栈空
	}
	e = S->data;					//将栈顶元素赋给e
	StackNode* p = S;				//用p临时保存栈顶元素，以备释放空间
	S = S->next;					//修改栈顶指针
	free(p);						//释放原栈顶空间
	return true;
}

bool GetTop(LinkStack S, ElemType& e) {
	//返回S的栈顶元素，不修改栈顶指针
	if (S!=NULL)					//栈非空
	{
		e = S->data;				//返回栈顶元素的值，栈顶指针不变
		return true;
	}
	return false;
}
