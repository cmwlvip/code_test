#pragma once
#include<stdio.h>
#include<string.h>
#include <stdlib.h>
#include<iostream> //使用new
#include"../DataStructure/Person.h"
#include"../DataStructure/myFunction.h"

//单链表的存储结构
typedef struct LNode {
	ElemType data;				//结点的数据域
	LNode* next;				//结点的指针域
}LNode,*LinkList;

bool InitList(LinkList& L);

//在带头结点的单链表L中根据序号i获取元素的值，用e返回L中第i给元素的值
bool GetElem(LinkList& L, int i, ElemType& e);

//在带头结点的单链表L中查找值为e的元素
LNode* LocateElem(LinkList L, ElemType e, int(*myCompare)(ElemType, ElemType));

//带头结点的单链表L中的第i个位置插入值为e的新结点
bool ListInsert(LinkList& L, int i, ElemType e);

//在带头结点的单链表L中删除第i个元素
bool ListDelete(LinkList& L, int i);

//遍历
void TraverseList(LinkList L,void(*myPrint)(ElemType));

//前插法创建单链表(多传入了一个赋值函数)
void CreateList_HeadInsert(LinkList& L, int n, ElemType(*create)());

//后插法创建单链表(多传入了一个赋值函数)
void CreateList_TailInsert(LinkList& L, int n, ElemType(*create)());



//-------双向链表的存储结构-------
//(Double Linked List)
typedef struct DuLNode
{
	ElemType data;				//数据域
	struct DuLNode* prior;		//指向直接前驱
	struct DuLNode* next;		//指向直接后继
}DuLNode,*DuLinkList;

//带头结点的双链表L中的第i个位置插入值为e的新结点
bool ListInsert(DuLinkList& L, int i, ElemType e);

//在带头结点的双链表L中删除第i个元素
bool ListDelete(DuLinkList& L, int i);