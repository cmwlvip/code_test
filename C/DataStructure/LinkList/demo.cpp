#include<stdio.h>
#include <stdlib.h>
#include"LinkList.h"

void ListMenu() {
	printf("*******************************************\n");
	printf("**************单链表（线性表的链式存储）***\n");
	printf("**************0.退出***********************\n");
	printf("**************1.单链表的初始化*************\n");
	printf("**************2.插入一组元素(测试元素)*****\n");
	printf("**************3.单链表的按位查找***********\n");
	printf("**************4.单链表的按值查找***********\n");
	printf("**************5.单链表的插入（指定位序）***\n");
	printf("**************6.单链表的删除（按位）*******\n");
	printf("**************7.头插法建立单链表**********\n");
	printf("**************8.尾插法建立单链表**********\n");
	printf("**************10.遍历单链表***************\n");
}


int main() {
	LinkList L;		//先定义一个单链表(头节点)
	L = NULL;
	while (true)
	{
		ListMenu();
		printf("请输入您的选择：\n");
		int choice = intCin();
		switch (choice)
		{
		case 0:
			ExitSystem();//退出程序
			break;
		case 1: {
			
			if (L!=NULL)
			{
				printf("链表已经初始化，无需再次初始化！");

			}
			else {
				InitList(L);
				printf("初始化成功！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 2: {//插入一组测试元素
			//准备好5个数据
			char personName_array[5][16] = { "亚瑟","王昭君","貂蝉","孙悟空","张飞" };
			int age_array[5] = { 38,23,16,22,43 };
			for (int i = 0; i < 5; i++)
			{
				ElemType e = create_struct(personName_array[i], age_array[i]);
				ListInsert(L, 1, e);
			}
			printf("成功插入一组测试数据！\n");
			system("pause");
			system("cls");
		}
			  break;
		case 3: {
			printf("请输入查找位序！\n");
			int index = intCin();
			ElemType e=create_struct();
			if (GetElem(L,index,e)) {
				printf("查找成功！查找元素为：\n");
				PersonPrint(e);
			}
			else
			{
				printf("查找失败，位序不合法！");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 4: {//单链表的按值查找
			
			ElemType e = create_Input();
			if (LocateElem(L,e,PersonCompare)!=NULL)
			{
				printf("查找成功！\n");
			}
			else
			{
				printf("查找失败！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 5: {//单链表的插入
			printf("请输入插入位序！\n");
			int index = intCin();
			ElemType e = create_Input();
			if (ListInsert(L,index,e))
			{
				printf("插入成功！\n");
			}
			else
			{
				printf("插入失败！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 6: {
			printf("请输入待删除位序！\n");
			int index = intCin();
			if (ListDelete(L,index))
			{
				printf("删除成功！\n");
			}
			else
			{
				printf("删除失败，位序不合法！\n");
			}
			system("pause");
			system("cls");
		}
			  break;
		case 7: {
			printf("请输入元素个数：\n");
			int n = intCin();
			CreateList_HeadInsert(L, n,create_Input);
			system("pause");
			system("cls");
		}
			   break;
		case 8: {
			printf("请输入元素个数：\n");
			int n = intCin();
			CreateList_TailInsert(L, n, create_Input);
			system("pause");
			system("cls");
		}
			  break;
		case 10: {
			TraverseList(L,PersonPrint);
			system("pause");
			system("cls");
		}
			  break;
		default:
			printf("非法输入，请重新输入！\n");
			system("pause");
			system("cls");
			break;
		}
	}
	system("pause");
	return 0;
}