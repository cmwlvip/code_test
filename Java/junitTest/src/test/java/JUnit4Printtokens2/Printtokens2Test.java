package JUnit4Printtokens2;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.*;

public class Printtokens2Test extends TestCase {

    @Test
    //测试传入null值的情况，确保返回的BufferedReader是从系统输入流中读取的。
    public void testOpen_character_stream() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream("hello".getBytes());
        System.setIn(in);
        BufferedReader br = new Printtokens2().open_character_stream(null);
        assertTrue(br.ready());
    }
    @Test
    //测试传递存在的文件名的情况，确保返回的BufferedReader与该文件相关联。
    public void testOpen_character_stream_two() throws IOException {
        String fileName = "test.txt";
        // 创建一个测试文件并写入一些内容
        try (PrintWriter out = new PrintWriter(fileName)) {
            out.println("This is a test file");
        } catch (FileNotFoundException e) {
            fail("Failed to create test file");
        }
        BufferedReader br = new Printtokens2().open_character_stream(fileName);
        assertNotNull(br);
        assertTrue(br instanceof BufferedReader);
        assertTrue(br.ready());
        // 确保读取的内容与写入的内容相同
        try {
            String line = br.readLine();
            assertEquals("This is a test file", line);
        } catch(IOException e) {
            fail("Failed to read from test file");
        }
    }
    @Test
//    测试传递不存在的文件名的情况，确保在控制台上打印出一条错误消息并且抛出FileNotFoundException异常。
    public void testOpen_character_stream_three(){
        String fileName = "non_existent_file.txt";

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        BufferedReader br = new Printtokens2().open_character_stream(fileName);
        assertNull(br);

        // 确保错误消息已打印到控制台
//        Exception e=new FileNotFoundException(fileName);
//        StringWriter sw = new StringWriter();
//        PrintWriter pw = new PrintWriter(sw);
//        e.printStackTrace(pw);
//        String stackTrace = sw.toString();
        String expectedOutput = "The file " + fileName + " doesn't exists\n";
        assertEquals(expectedOutput, outContent.toString()+errContent.toString());
    }


//控制台测试
//  ByteArrayOutputStream outContent = new ByteArrayOutputStream();
//        System.setOut(new PrintStream(outContent));

    public void testGet_char() {
        String inputString = "0abcdef";
        BufferedReader br = new BufferedReader(new StringReader(inputString));
        int a=new Printtokens2().get_char(br);
        assertEquals(48,a);
    }

    public void testUnget_char() throws IOException {
        String inputString = "0abcdef";
        BufferedReader br = new BufferedReader(new StringReader(inputString));
        Printtokens2 test=new Printtokens2();
        test.get_char(br);
        System.out.println(br.read());//97
        System.out.println(br.read());//预期98
        test.unget_char(0,br);//回退1
        assertEquals(48,br.read());
    }

    public void testOpen_token_stream() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream("hello".getBytes());
        System.setIn(in);
        String fname=null;
        BufferedReader br = new Printtokens2().open_token_stream(fname);
        assertTrue(br.ready());
//        测试已有文件
        fname="test.txt";
        BufferedReader brTwo = new Printtokens2().open_token_stream(fname);
        // 确保读取的内容与写入的内容相同
        try {
            String line = brTwo.readLine();
            assertEquals("This is a test file", line);
        } catch(IOException e) {
            fail("Failed to read from test file");
        }
    }

    public void testGet_token() {
        String fname="test.txt";
        String inputString = "\"This is a test line\"";
        BufferedReader br = new BufferedReader(new StringReader(inputString));
        String result=new Printtokens2().get_token(br);
        assertEquals(inputString,result);

    }

    public void testIs_token_end() {
        //前一个参数为1，2
        boolean result=Printtokens2.is_token_end(1,-1);
        assertTrue(result);
        result=Printtokens2.is_token_end(1,59);
        assertFalse(result);//会强行跳出
        result=Printtokens2.is_token_end(0,1);
        assertFalse(result);
    }

    public void testToken_type() {
        int result=Printtokens2.token_type("and");
        assertEquals(Printtokens2.keyword,result);
    }

    public void testPrint_token() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        Printtokens2 test=new Printtokens2();
        String tok="and";
        test.print_token(tok);
        assertEquals("keyword,\"" + tok + "\".\n",outContent.toString());
    }

//    是否为分号 59
    public void testIs_comment() {
        boolean result=Printtokens2.is_comment(";");
        assertTrue(result);
        result=Printtokens2.is_comment("；");
        assertFalse(result);
    }

    public void testIs_keyword() {
        boolean result=Printtokens2.is_keyword("and");
        assertTrue(result);
    }

    public void testIs_char_constant() {
        boolean result=Printtokens2.is_char_constant("#");
        assertFalse(result);
        result=Printtokens2.is_char_constant("#test");
        assertTrue(result);
    }

    public void testIs_num_constant() {
        boolean result=Printtokens2.is_num_constant("123");
        assertTrue(result);
        result=Printtokens2.is_num_constant("1a2");
        assertFalse(result);
    }
    public void testIs_str_constant() {
        boolean result=Printtokens2.is_str_constant("\"\"");
        assertTrue(result);
        result=Printtokens2.is_str_constant("\"test\"");
        assertTrue(result);
    }

    public void testIs_identifier() {
        boolean result=Printtokens2.is_identifier("2a");
        assertFalse(result);
        result=Printtokens2.is_identifier("a2");
        assertTrue(result);
    }

    public void testUnget_error() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        BufferedReader br=new BufferedReader(new StringReader("test"));
        Printtokens2.unget_error(br);
        assertEquals("It can not get charcter\n",outContent.toString());
    }

    public void testPrint_spec_symbol() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String test="{";
        Printtokens2.print_spec_symbol(test);
        test=")";
        Printtokens2.print_spec_symbol(test);
        test="'";
        Printtokens2.print_spec_symbol(test);
        assertEquals("lparen.\n"+"rparen.\n"+"quote.\n",outContent.toString());
    }

    //   []()/',
    public void testIs_spec_symbol() {
        Character[] testArr={'[',']','(',')','/','`',','};
        boolean result = false;
        for (int i=0;i<testArr.length;i++){
            result=Printtokens2.is_spec_symbol(testArr[i]);
            assertTrue(result);
        }
        result=Printtokens2.is_spec_symbol('.');
        assertFalse(result);
    }

    public void testMain() throws IOException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        String test[]={"test1.txt"};
        Printtokens2.main(test);
        String expected="keyword,\"and\".\n" +
                "bquote.\n" +
                "keyword,\"and\".\n" +
                "identifier,\"id1\".\n" +
                "error,\"112A\".\n" +
                "character,\"a\".\n" +
                "numeric,123.\n" +
                "error,\"“123”\".\n" +
                "string,\"\"123\"\".\n";
        assertEquals(expected,outContent.toString());
    }
}
