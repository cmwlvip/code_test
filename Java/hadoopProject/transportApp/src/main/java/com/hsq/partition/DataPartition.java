package com.hsq.partition;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class DataPartition {
    public static void main(String[] args) throws Exception {
        if(args==null || args.length<2){
            throw new Exception("参数不足，需要两个参数！");
        }
        //1.新建配置对象，为配置对象设置文件系统
        Configuration conf =new Configuration();
        //2.设置Job属性
        Job job=Job.getInstance(conf,DataPartition.class.getName());
        job.setJarByClass(DataPartition.class);
        //3.设置数据输入路径
        Path inPath =new Path(args[0]);
        FileInputFormat.addInputPath(job,inPath);
        //4.设置Job执行的Mapper类
        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        //5.设置Job执行的Reducer类和输出K-V类型
        //设置分区
        job.setPartitionerClass(MyPartitioner.class);
//        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //设置reduce任务的个数
        job.setNumReduceTasks(2);

        //6.递归删除输出目录
        FileSystem.get(conf).delete(new Path(args[1]),true);
        //7.设置数据输出路径
        Path outPath=new Path(args[1]);
        FileOutputFormat.setOutputPath(job,outPath);
        //8.MapReduce作业完成后退出系统
        System.exit(job.waitForCompletion(true)?0:1);
    }
}
