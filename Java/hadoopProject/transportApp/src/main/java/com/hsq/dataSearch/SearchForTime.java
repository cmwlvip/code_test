package com.hsq.dataSearch;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;


//这里根据选择的时间段筛选可疑车辆，
public class SearchForTime {
    //需要查询开始的年月日 8为YYYYMMDD
    public static String startDay;
    //需要查询结束的年月日
    public  static String endDay;
    //需要查询开始的时间段,如凌晨0点
    public  static String startTime;
    //需要查询结束的时间段,如凌晨0点
    public static String endTime;
    //可疑频率,大于num为可疑,如:一个月出现20为可疑
    public static String num;

    public static void main(String[] args) throws Exception {
        if(args==null || args.length<7){
            throw new Exception("参数不足，需要多个参数！");
        }
        //初始化数据
        init(args);

        //1.新建配置对象，为配置对象设置文件系统
        Configuration conf =new Configuration();
//        conf.set("mapreduce.output.key.field.separator","");//设置分隔符为空

        //2.设置Job属性
        Job job=Job.getInstance(conf, SearchForTime.class.getName());
        job.setJarByClass(SearchForTime.class);
        //设置好变量

        job.getConfiguration().set("startDay",startDay);
        job.getConfiguration().set("endDay",endDay);
        job.getConfiguration().set("startTime",startTime);
        job.getConfiguration().set("endTime",endTime);
        job.getConfiguration().set("num", num);
        //3.设置数据输入路径
        Path inPath =new Path(args[5]);
        FileInputFormat.addInputPath(job,inPath);
        //4.设置Job执行的Mapper类
        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        //5.设置Job执行的Reducer类和输出K-V类型
        job.setReducerClass(MyReducerNew.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);


        //6.递归删除输出目录
        FileSystem.get(conf).delete(new Path(args[6]),true);
        //7.设置数据输出路径
        Path outPath=new Path(args[6]);
        FileOutputFormat.setOutputPath(job,outPath);
        //8.MapReduce作业完成后退出系统
        System.exit(job.waitForCompletion(true)?0:1);
    }
    static void init(String[] arr) throws IOException {
        //设置好时间段
        startDay= arr[0];
        endDay= arr[1];
        startTime= arr[2];
        endTime= arr[3];
        //设置好可疑出现次数
        num= arr[4];
        System.out.println("********************************************************");
        System.out.println("查询从"+startDay+"到"+endDay+"期间 "+startTime+"-"+endTime+"时间段可疑车辆");
        System.out.println("活跃频率大于:"+num);
        System.out.println("********************************************************");
    }
}
