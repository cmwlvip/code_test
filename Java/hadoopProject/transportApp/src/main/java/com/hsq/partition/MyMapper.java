package com.hsq.partition;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text,Text, Text> {
    Text k=new Text();
    Text v=new Text();
    @Override
    protected void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {
        String line=value.toString();
        String[] arr=line.split("\t");
        k.set(arr[0]);
        v.set(arr[1]+"\t"+arr[2]+"\t"+arr[3]+"\t"+arr[4]);
        context.write(k,v);


    }
}
