package com.hsq.partition;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class MyPartitioner extends Partitioner<Text,Text> {

    @Override
    public int getPartition(Text text, Text value, int i) {
        String key=text.toString();
        if (key.startsWith("201909")){
            return 0;
        }
        //other 检测是否有其他月份
        return 1;
    }
}
