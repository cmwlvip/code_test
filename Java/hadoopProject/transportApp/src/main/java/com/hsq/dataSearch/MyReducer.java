package com.hsq.dataSearch;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MyReducer extends Reducer<Text, Text,Text,Text> {
    Integer num;
    public static Map<String, String> roadDataList = new HashMap<>();
    //    转换路径,将卡口id转化为具体卡口名称
    static String getRoute(String deviceId){
        String direction="";
        String info="非管辖范围卡口("+deviceId+")"+direction;

        if (roadDataList.containsKey(deviceId)){
            return roadDataList.get(deviceId);
        }
        else{
            if(deviceId.endsWith("01")){
                direction="东向西";
            } else if (deviceId.endsWith("02")) {
                direction = "西向东";
            } else if (deviceId.endsWith("03")) {
                direction = "南向北";
            } else if (deviceId.endsWith("04")) {
                direction="北向南";
            }else{
                direction="其他";
            }
        }
        return info;
    }


    @Override
    protected  void setup(Context context) throws IOException {
        Configuration conf=context.getConfiguration();
        num= Integer.valueOf(conf.get("num"));
        conf.set("fs.defaultFS", "hdfs://hsq01:9000");
        FileSystem fs = FileSystem.get(conf);
        Path filePath = new Path("/transport.csv");
        BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(filePath)));
        String line;
        while ((line = reader.readLine()) != null) {
            roadDataList.put(line.split(",")[1],line.split(",")[2]);
        }
    }

    @Override
    protected void reduce(Text key,Iterable<Text> values,Context context) throws IOException, InterruptedException {
        Integer counter=0;

        List<MyData> dataList=new ArrayList<>();
        for(Text value:values){
            String line=value.toString();
            dataList.add(new MyData(line));
            counter+=1;
        }
        //为可疑车辆
        if (counter>num){
            Collections.sort(dataList, (o1, o2) -> {
                try {
                    Date date1 = o1.getDate(o1.passingTime);
                    Date date2 = o2.getDate(o2.passingTime);
                    return date1.compareTo(date2); // 时间降序
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            });

            Text info=new Text("可疑车辆:\t"+key+"\t出现次数:\t"+counter);
            context.write(new Text(),info);
            int count=0;
            //输出轨迹
            for(MyData data:dataList){
                count+=1;
                Text outInfo=new Text(data.toString());
                context.write(new Text(String.valueOf(count)),outInfo);//从上往下写
            }
            context.write(new Text(),new Text("********************************************************"));
            //清空列表
            dataList.clear();
        }
        dataList.clear();
    }
    class MyData {
        String passingTime;
        String roadId;
        String deviceId;

        public MyData(String line){
            String[] arr=line.split("\t");
            this.passingTime=arr[0];
            this.roadId=arr[2];
            this.deviceId=""+arr[3]+arr[1];
        }
        public Date getDate(String passingTime) throws ParseException {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = formatter.parse(passingTime);
            return date;
        }
        @Override
        public String toString(){
            try {
                return "车辆于"+getDate(passingTime)+" 从 "+getRoute(deviceId)+"经过,道路编号("+roadId+")";
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
