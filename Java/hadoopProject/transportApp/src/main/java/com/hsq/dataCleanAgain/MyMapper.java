package com.hsq.dataCleanAgain;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text, LongWritable, Text>{
    Text k=new Text();
    private static final Text textValue = new Text();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        context.write(k,v);
        //以行为单位，对数据进行处理
        String line=value.toString();
        String[] lineArr=line.split("\t");
        String passingTime=lineArr[0];
        String vehicleId=lineArr[1];
        String direction=lineArr[2];
        String roadId=lineArr[3];
        String deviceId=lineArr[4];
        textValue.set(passingTime+"\t"+vehicleId+"\t"+direction+"\t"+roadId+"\t"+deviceId);
        context.write(key, textValue);
//        context.write(k,NullWritable.get());
//        if (checkVehicleId(vehicleId)){
//            //过车时间 车牌号 方向 道路编号 设备编号
//            k.set(passingTime+"\t"+vehicleId+"\t"+direction+"\t"+roadId+"\t"+deviceId);
////            k.set(passingTime+"\t"+vehicleId+"\t"+roadId);
//            context.write(k,NullWritable.get());
//        }
    }
    //用于删除无车牌号车辆
    public boolean checkVehicleId(String vehicleId){
        if (vehicleId.equals("00000000")){
            return false;
        }
        return true;
    }
}