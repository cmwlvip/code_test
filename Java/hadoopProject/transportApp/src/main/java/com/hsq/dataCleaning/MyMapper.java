package com.hsq.dataCleaning;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text,Text, NullWritable>{
    Text k=new Text();
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        context.write(k,v);
        //以行为单位，对数据进行处理
        String line=value.toString();
        String[] lineArr=line.split("\001");
        String deviceId=lineArr[1];
        String direction=lineArr[3];
        String roadId=lineArr[4];
        String passingTime=lineArr[7];
        String vehicleId=lineArr[51];
        vehicleId=this.checkVehicleId(vehicleId);
        //过车时间 车牌号 方向 道路编号 设备编号
        k.set(passingTime+"\t"+vehicleId+"\t"+direction+"\t"+roadId+"\t"+deviceId);
        context.write(k,NullWritable.get());
    }
    public String checkVehicleId(String vehicleId){
        if (vehicleId.equals("")){
            return "00000000";
        }
        return vehicleId;
    }
}
