package com.hsq.dataSearch;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.*;

public class MyReducerNew extends Reducer<Text, Text,Text,Text> {
    Integer num;
    @Override
    protected  void setup(Context context) throws IOException {
        Configuration conf=context.getConfiguration();
        num= Integer.valueOf(conf.get("num"));
    }
    @Override
    protected void reduce(Text key,Iterable<Text> values,Context context) throws IOException, InterruptedException {
        Integer counter=0;

        List<MyData> dataList=new ArrayList<>();
        for(Text value:values){
            String line=value.toString();
            dataList.add(new MyData(line));
            counter+=1;
        }
        //为可疑车辆
        if (counter>num){
            //输出轨迹
            for(MyData data:dataList){
                Text outInfo=new Text(data.toString());
                //车牌
                context.write(new Text(key),outInfo);//从上往下写
            }
            //清空列表
            dataList.clear();
        }
        dataList.clear();
    }
    class MyData {
        String passingTime;
        String roadId;
        String deviceId;

        public MyData(String line){
            String[] arr=line.split("\t");
            this.passingTime=arr[0];
            this.roadId=arr[2];
            this.deviceId=""+arr[3]+arr[1];
        }
        @Override
        public String toString(){
            return passingTime+"\t"+deviceId;
        }
    }
}
//输出样式
//川A7J52T	20190906040830	42050300510002000402