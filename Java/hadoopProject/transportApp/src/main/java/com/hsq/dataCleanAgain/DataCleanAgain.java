package com.hsq.dataCleanAgain;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class DataCleanAgain {
    public static void main(String[] args) throws Exception {
        if(args==null || args.length<3){
            throw new Exception("参数不足，需要三个参数！");
        }
        //1.新建配置对象，为配置对象设置文件系统
        Configuration conf =new Configuration();
        //2.设置Job属性
        Job job=Job.getInstance(conf,DataCleanAgain.class.getName());
        job.setJarByClass(DataCleanAgain.class);
        job.getConfiguration().setStrings("mapreduce.reduce.shuffle.memory.limit.percent", "0.15");
        //3.设置数据输入路径
//        for (int i=5;i<10;i++){
//            String temp="/part-r-00000_"+i;
//            Path inPath =new Path(temp);
//            FileInputFormat.addInputPath(job,inPath);
//        }
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileInputFormat.addInputPath(job, new Path(args[1]));
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Text.class);

        //4.设置Job执行的Mapper类
        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);

        //5.设置Job执行的Reducer类和输出K-V类型
        job.setReducerClass(MyReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        //6.递归删除输出目录
        FileSystem.get(conf).delete(new Path(args[2]),true);
        //7.设置数据输出路径
        Path outPath=new Path(args[2]);
        FileOutputFormat.setOutputPath(job,outPath);
        //8.MapReduce作业完成后退出系统
        System.exit(job.waitForCompletion(true)?0:1);
    }
}
