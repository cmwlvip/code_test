package com.hsq.dataCleanAgain;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MyReducer extends Reducer<LongWritable, Text, NullWritable, Text> {
    private static final Text textValue = new Text();

    public void reduce(LongWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            textValue.set(value.toString());
            context.write(NullWritable.get(), textValue);
        }
    }
}