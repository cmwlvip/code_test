package com.hsq.dataToHDFS;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class DataToHDFS {
    public static void main(String[] args) throws IOException {
        putFile();
    }
    //上传文件到HDFS
    public  static void putFile() throws IOException{
        Configuration conf =new Configuration();
        conf.set("fs.defaultFS","hdfs://192.168.56.201:9000");
        conf.set("dfs.replication","1");
        FileSystem fs=FileSystem.get(conf);
        //文件上传到HDFS上的位置
        Path p=new Path("/");
        for(int i=5;i<10;i++){
            String temp="file:///G:/大数据清洗/part-r-00000_"+i;
            Path p2=new Path(temp);
            //从本地（Windows系统）上传文件到HDFS
            fs.copyFromLocalFile(p2,p);
        }
//        Path p2=new Path("file:///G:/大数据清洗/part-r-00000_1");

        fs.close();
    }
}
