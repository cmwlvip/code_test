package com.hsq.dataSearch;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable, Text,Text, Text> {
    Text k=new Text();
    Text v=new Text();
    int startDay;
    int endDay;
    int startTime;
    int endTime;
    @Override
    protected  void setup(Mapper.Context context){
        Configuration conf=context.getConfiguration();
        startDay= Integer.parseInt(conf.get("startDay"));
        endDay= Integer.parseInt(conf.get("endDay"));
        startTime= Integer.parseInt(conf.get("startTime"));
        endTime=Integer.parseInt(conf.get("endTime"));
    }
    @Override
    protected void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {
        String line=value.toString();
        String[] arr=line.split("\t");

        int date= Integer.parseInt(arr[0].substring(0,8));
        int time= Integer.parseInt(arr[0].substring(8,10));

        //在查询时间内
        if(date>=startDay && date<=endDay && time>=startTime && time<=endTime && !arr[1].equals("00000000")){
            //key为车牌号 并且是符号要求时间段
            k.set(arr[1]);
            v.set(arr[0]+"\t"+arr[2]+"\t"+arr[3]+"\t"+arr[4]);
            context.write(k,v);
        }
    }
}