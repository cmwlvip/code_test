package com.hsq.everyMonthMaxWeather;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class MyReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void reduce(Text key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
        int max = -932;
        for (Text t : value) {
            int temperature = Integer.parseInt(t.toString());
            if (temperature > max && temperature != 9999 ) {
                max = temperature;
            }
        }
        context.write(key, new Text(max + ""));

    }
}
