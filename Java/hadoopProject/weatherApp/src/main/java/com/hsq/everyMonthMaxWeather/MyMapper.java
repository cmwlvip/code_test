package com.hsq.everyMonthMaxWeather;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable,Text, Text,Text> {
    Text k=new Text();
    Text v=new Text();
    @Override
    protected void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {
        //以行为单位，对数据进行处理
        String line=value.toString();
        //空气温度
        String  temperature=line.substring(87,92);
        temperature=this.checkTemperature(temperature);
        String date=line.substring(15,21);//201801
        k.set(date);
        v.set(temperature);
        context.write(k,v);

    }
    public String checkTemperature(String str){
        Long temperature=Long.valueOf(str.substring(1));
        if (str.charAt(0)=='+' && temperature<=618){
            return  temperature.toString();
        }
        else if(str.charAt(0)=='-' && temperature<=932){
            return  "-"+temperature;
        }
        return "9999";
    }

}
