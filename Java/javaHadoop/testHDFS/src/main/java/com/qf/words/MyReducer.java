package com.qf.words;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MyReducer extends Reducer<Text, IntWritable,Text,IntWritable> {
    @Override
    protected void reduce(Text key,Iterable<IntWritable> values,Context context) throws IOException, InterruptedException {
        //1.定义一个计数器
        Integer counter = 0;
        //2.迭代数组，将输出的K-V对存入context
        for (IntWritable value:values){
            counter+=value.get();
        }
        context.write(key,new IntWritable(counter));
    }
}