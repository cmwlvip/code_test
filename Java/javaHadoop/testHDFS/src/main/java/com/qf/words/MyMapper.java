package com.qf.words;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<LongWritable,Text,Text, IntWritable> {
    Text word =new Text();
    IntWritable one =new IntWritable(1);
    @Override
    protected void map(LongWritable key,Text value,Context context) throws IOException, InterruptedException {
        //1.以行为单位，对数据进行处理
        String line=value.toString();
        //2.以空格为分隔符，对单词进行拆分
        String[] words=line.split(" ");
        //3.迭代数组，将输出的K-V对存入context
        for (String s:words){
            word.set(s);
            context.write(word,one);
        }
    }
}

