//package com.qf;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.io.IntWritable;
//import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
//import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
//
//import java.io.IOException;
//
//public class MyApp {
//    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
//        //1.新建配置对象，为 配置对象设置文件系统
//        Configuration conf=new Configuration();
//        conf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
//        //2.设置Job属性
//        Job job=Job.getInstance(conf);  //通过Configuration获得Job实例
//        job.setJobName("MyApp");    //为Job命名
//        job.setJarByClass(MyApp.class); //为Job运行设置主类
//        //3.设置数据输入路径
//        Path inPath =new Path(args[0]);
//        FileInputFormat.addInputPath(job,inPath);
//        //4.设置Job执行的Mapper类和输出K-V类型
//        job.setMapperClass(MyMapper.class);
//        job.setMapOutputKeyClass(Text.class);
//        job.setMapOutputValueClass(IntWritable.class);
//        //5.设置执行的Reducer类和输出K-V类型
//        job.setReducerClass(MyReducer.class);
//        job.setOutputKeyClass(Text.class);
//        job.setMapOutputValueClass(IntWritable.class);
//        //6.设置数据输出路径
//        Path outPath=new Path(args[1]);
//        FileOutputFormat.setOutputPath(job,outPath);
//        //7.MepReduce作业完成后退出系统
//        System.exit(job.waitForCompletion(true)?0:1);
//
//    }
//}
