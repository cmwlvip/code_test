package com.qf.operateHDFS;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class OperateHDFS {
    public static void main(String[] args) throws IOException {
//        writeToHDFS();
//        readHDFSFile();
        putFile();
    }
    //将数据写入HDFS文件
    public static  void writeToHDFS() throws IOException {
        //创建配置文件对象
        Configuration conf = new Configuration();
        //给配置文件设置HDFS文件默认入口
        conf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        //通过传入的配置参数得到FileSystem
        FileSystem fs = FileSystem.get(conf);
        //获取HDFS上的 /1.txt 的绝对路径，/1.txt 是存在的也可以是不存在的
        Path p = new Path("/1.txt");
        //FileSystem 通过 create() 方法获得输出流（FSDataOutputStream）
        FSDataOutputStream fos = fs.create(p, true, 1024);
        //通过输出流将内容写入 1.txt 文件
        fos.write("这是我在window用java API下写入的".getBytes());
        //关闭输出流
        fos.close();
    }
    //读取HDFS文件
    public static void readHDFSFile() throws IOException {
        //创建配置对象
        Configuration conf = new Configuration();
        //设置HDFS文件系统的网络地址和端口号
        conf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        //通过配置获取文件系统
        FileSystem fs = FileSystem.get(conf);
        //获取HDFS上的 /1.txt 的绝对路径
        Path p = new Path("/1.txt");
        //通过 FileSystem 的open() 方法获得数据输入流
        FSDataInputStream fis = fs.open(p);
        //分配 1024 字节的内存给 buf （分配1024个字节的缓冲区）
        byte[] buf = new byte[1024];
        int len = 0;
        //循环读取文件到内容到缓冲区，读到文件末尾结束（结束标识符为-1）
        while ((len = fis.read(buf)) != -1) {
            //输出读取的文件内容到控制台
            System.out.println(new String(buf, 0, len));
        }
    }
    //上传文件到HDFS
    public  static void putFile() throws IOException{
        Configuration conf =new Configuration();
        conf.set("fs.defaultFS","hdfs://192.168.56.201:9000");
        FileSystem fs=FileSystem.get(conf);
        //文件上传到HDFS上的位置
        Path p=new Path("/");
        //待上传文件1.sh在Windows系统的绝对路径，此处需要提前在Windows系统下D盘下新建1.sh文件，并写入 “文件上传成功！”
        Path p2=new Path("file:///D:/1.sh");
        //从本地（Windows系统）上传文件到HDFS
        fs.copyFromLocalFile(p2,p);
        fs.close();
    }
}
