package com.hsq;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;

public class SimpleApp {
    public static void main(String[] args) {
        //默认HDFS,所以加上file:///
        String logFile = "file:///mysoft/spark-3.3.2-bin-hadoop3//README.md"; // Should be some file on your system
        SparkSession spark = SparkSession.builder().appName("Simple Application").getOrCreate();
        Dataset<String> logData = spark.read().textFile(logFile).cache();

        long numAs = logData.filter((FilterFunction<String>) s -> s.contains("a")).count();
        long numBs = logData.filter((FilterFunction<String>) s -> s.contains("b")).count();

        System.out.println("包含a的行数为: " + numAs + ", 包含b的行数为: " + numBs);

        spark.stop();
    }
}

//bin/spark-submit --master spark://hsq01:7077,hsq02:7077,hsq03:7077 --class "com.hsq.SimpleApp" testSpark-1.0-SNAPSHOT.jar