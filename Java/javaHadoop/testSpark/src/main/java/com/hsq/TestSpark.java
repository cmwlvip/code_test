package com.hsq;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;

public class TestSpark {
    //Spark程序必须做的第一件事是创建一个JavaSparkContext对象，它告诉Spark 如何访问集群。
    public static void test(){
        SparkConf conf=new SparkConf()
                .setAppName("tesstApp")
                .setMaster("spark://192.168.56.201:7077")
                .set("spark.driver.host","192.168.56.1")
                .setJars(new String[]{("testSpark/target/testSpark-1.0-SNAPSHOT.jar")})
                ;
        JavaSparkContext sc =new JavaSparkContext(conf);
        //弹性分布式数据集 （RDD）
        List<Integer> data= Arrays.asList(1,1,1,2,3,4,5,1,2,3,4,5,6,7,4);
        JavaRDD<Integer> distData=sc.parallelize(data);
//        JavaRDD<String> lines = sc.textFile("data.txt");
        JavaRDD lines=distData;
        JavaPairRDD<String, Integer> pairs = lines.mapToPair(s -> new Tuple2(s, 1));
        JavaPairRDD<String, Integer> counts = pairs.reduceByKey((a, b) -> a + b);
        System.out.println(counts.collect());
        counts.foreach(new VoidFunction<Tuple2<String, Integer>>() {
            @Override
            public void call(Tuple2<String, Integer> stringIntegerTuple2) throws Exception {
                System.out.println("数字："+String.valueOf(stringIntegerTuple2._1())+" 个数为："+stringIntegerTuple2._2);
            }
        });
    }

    public static void main(String[] args) {
        test();
    }
}
