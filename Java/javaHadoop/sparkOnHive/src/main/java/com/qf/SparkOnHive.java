package com.qf;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.Tuple2;

public class SparkOnHive {
    //该函数打包后可在spark集群上运行
    //测试
    public static void test(){
        SparkConf conf=new SparkConf()
//                .setMaster("spark://192.168.56.201:7077")
                .setAppName("SparkOnHive");
        SparkSession sparkSession= SparkSession
                .builder()
                .config(conf)
                .config("hive.metastore.uris","thrift://192.168.56.201:9083")
                .config("spark.sql.warehouse.dir","hdfs://192.168.56.201:9000/user/hive/warehouse")
                .enableHiveSupport()
                .getOrCreate();
        sparkSession.sql("use transport");
        Dataset<Row> df=sparkSession.sql("show tables");
        df.show();
        sparkSession.close();
    }
    public static void calculatePart(){
        SparkConf conf=new SparkConf()
                .setAppName("SparkOnHivePart");
        SparkSession sparkSession= SparkSession
                .builder()
                .config(conf)
                //设置hive远程连接
                .config("hive.metastore.uris","thrift://192.168.56.201:9083")
                //设置hive数据位置
                .config("spark.sql.warehouse.dir","hdfs://192.168.56.201:9000/user/hive/warehouse")
                .enableHiveSupport()
                .getOrCreate();
        sparkSession.sql("use transport");
        //将卡口表加载到hive表中
//        sparkSession.sql("load data inpath '/transport/transport.csv' into table transport");
        //从刚刚建立的表格进行查询（部门名称，时间）
        Dataset<Row> sqlDF =sparkSession.sql("select partName,startTime from transport");
        //对数据进行映射，因为时间有年月日，这里把年份映射出来
        Dataset<String> stringsDS = sqlDF.map(
                (MapFunction<Row, String>) row -> row.get(0)+","+String.valueOf(row.get(1)).split("-")[0]+"",
                Encoders.STRING());
        //对数据进行统计
        Long num= stringsDS.count();
        //结果以JavaRDD形式保存
        JavaRDD lines=stringsDS.javaRDD();
        //把数据变成key value 键值对
        JavaPairRDD<String, Integer> pairs = lines.mapToPair(s -> new Tuple2(s, 1));
        //相同的key相加
        JavaPairRDD<String, Integer> counts = pairs.reduceByKey((a, b) -> a + b);
        //对数据进行排序
        JavaPairRDD<String,Integer> sortCounts=counts.sortByKey();
        System.out.println(sortCounts.collect());
        //计算占比
        JavaPairRDD<String, String> result=sortCounts.mapToPair(s->new Tuple2<String,String>(s._1,String.valueOf(s._2*1.0/num)));
        //收集结果
        System.out.println(result.collect());

        //把JavaPairRDD变回JavaRDD数据
        JavaRDD<Row> rowRDD = result.map(tuple ->
                RowFactory.create(String.valueOf(tuple._1()).split(",")[0],String.valueOf(tuple._1()).split(",")[1], tuple._2()));
        // 创建 SQLContext 对象
        SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sparkSession);
        //建立表格，有三列
        StructType schema = new StructType(new StructField[]{
                new StructField("partName", DataTypes.StringType, false, Metadata.empty()),
                new StructField("year", DataTypes.StringType, false, Metadata.empty()),
                new StructField("proportion", DataTypes.StringType, false, Metadata.empty())
        });
        Dataset<Row> dataFrame = sqlContext.createDataFrame(rowRDD, schema);
//        写入数据
        dataFrame.write().mode(SaveMode.Overwrite).saveAsTable("test1");
//        各个交警大队不同年份建设的卡口占比；
        sparkSession.sql("select * from test1 order by proportion desc").show();
//        哪个交警大队的设备旧设备最多；
        sparkSession.sql("select * from test1 where year='2014' order by proportion desc").show();
        sparkSession.close();
    }

    public static void calculateRoad(){
        SparkConf conf=new SparkConf()
                .setAppName("SparkOnHiveRoad");
        SparkSession sparkSession= SparkSession
                .builder()
                .config(conf)
                //设置hive远程连接
                .config("hive.metastore.uris","thrift://192.168.56.201:9083")
                //设置hive数据位置
                .config("spark.sql.warehouse.dir","hdfs://192.168.56.201:9000/user/hive/warehouse")
                .enableHiveSupport()
                .getOrCreate();
        sparkSession.sql("use transport");
        //将卡口表加载到hive表中
//        sparkSession.sql("load data inpath '/transport/transport.csv' into table transport");
        //从刚刚建立的表格进行查询（部门名称，时间）
        Dataset<Row> sqlDF =sparkSession.sql("select roadId,startTime from transport");
        //对数据进行映射，因为时间有年月日，这里把年份映射出来
        Dataset<String> stringsDS = sqlDF.map(
                (MapFunction<Row, String>) row -> row.get(0)+","+String.valueOf(row.get(1)).split("-")[0]+"",
                Encoders.STRING());
        //对数据进行统计
        Long num= stringsDS.count();
        //结果以JavaRDD形式保存
        JavaRDD lines=stringsDS.javaRDD();
        //把数据变成key value 键值对
        JavaPairRDD<String, Integer> pairs = lines.mapToPair(s -> new Tuple2(s, 1));
        //相同的key相加
        JavaPairRDD<String, Integer> counts = pairs.reduceByKey((a, b) -> a + b);
        //对数据进行排序
        JavaPairRDD<String,Integer> sortCounts=counts.sortByKey();
        System.out.println(sortCounts.collect());
        //计算占比
        JavaPairRDD<String, String> result=sortCounts.mapToPair(s->new Tuple2<String,String>(s._1,String.valueOf(s._2*1.0/num)));
        //收集结果
        System.out.println(result.collect());

        //把JavaPairRDD变回JavaRDD数据
        JavaRDD<Row> rowRDD = result.map(tuple ->
                RowFactory.create(String.valueOf(tuple._1()).split(",")[0],String.valueOf(tuple._1()).split(",")[1], tuple._2()));
        // 创建 SQLContext 对象
        SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sparkSession);
        //建立表格，有三列
        StructType schema = new StructType(new StructField[]{
                new StructField("roadId", DataTypes.StringType, false, Metadata.empty()),
                new StructField("year", DataTypes.StringType, false, Metadata.empty()),
                new StructField("proportion", DataTypes.StringType, false, Metadata.empty())
        });
        Dataset<Row> dataFrame = sqlContext.createDataFrame(rowRDD, schema);
//        写入数据
        dataFrame.write().mode(SaveMode.Overwrite).saveAsTable("test2");
//        各个交警大队不同年份建设的卡口占比；
        sparkSession.sql("select * from test2 order by proportion desc").show();
//        哪个交警大队的设备旧设备最多；
        sparkSession.sql("select * from test2 where year='2020' order by proportion desc").show();
        sparkSession.close();
    }
    public static void main(String[] args) {
//        test();
        calculatePart();
    }
}
