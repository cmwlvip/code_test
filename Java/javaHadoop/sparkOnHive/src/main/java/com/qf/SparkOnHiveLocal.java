package com.qf;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class SparkOnHiveLocal {
    //尝试在Java上远程实现SparkOnHive,没有成功
    //Spark version:3.3.2
    //Hive version:3.1.2
    //Hadoop version:3.3.4
    public static void test(){
        System.setProperty("HADOOP_USER_NAME","root");
        SparkConf conf=new SparkConf()
//                .setMaster("local[*]")
                .setMaster("spark://192.168.56.201:7077")
//                .set("spark.submit.deployMode","client")
//                .setJars(new String[]{("D:\\Code\\Code\\Java\\javaHadoop\\sparkOnHive\\target\\sparkOnHive-1.0-SNAPSHOT.jar")})
                .setJars(new String[]{("sparkOnHive/target/sparkOnHive-1.0-SNAPSHOT.jar")})
//                .set("spark.driver.host","192.168.56.1")
                .setAppName("SparkOnHiveLocal");
        SparkSession sparkSession= SparkSession
                .builder()
                .config(conf)
//                .config("spark.jars","D:\\Code\\Code\\Java\\javaHadoop\\sparkOnHive\\target\\sparkOnHive-1.0-SNAPSHOT.jar")
                .config("spark.sql.warehouse.dir","hdfs://192.168.56.201:9000/user/hive/warehouse")
                .config("hive.metastore.uris","thrift://192.168.56.201:9083")
                .config("spark.driver.host","192.168.56.1")
                .enableHiveSupport()
                .getOrCreate();
        sparkSession.sql("SET hive.metastore.warehouse.dir=hdfs://192.168.56.201:9000/user/hive/warehouse");
        sparkSession.sql("use transport");
        Dataset<Row> df=sparkSession.sql("show tables");
        df.show();
        sparkSession.close();
    }
    public static void main(String[] args) {
        test();
    }
}

