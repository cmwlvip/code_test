package com.hsq.kafka.producer;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class CustomProducerCallbackPartitions {
    public static void main(String[] args) throws InterruptedException {
        //1. 创建kafka 生产者的配置对象
        Properties properties = new Properties();
        String topicName="first";
        //2. 给kafka 配置对象添加配置信息：bootstrap.servers
        //连接集群 bootstrap.servers
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"hsq01:9092");
        // key,value 序列化（必须）：key.serializer，value.serializer
        //指定对应的key和value的序列化类型 key.serializer
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        //properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer .class.getName());//和上面语句等价
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
        // 添加自定义分区器
        properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG,MyPartitioner.class);
        //3. 创建 kafka 生产者对象
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);
        //4. 调用 send 方法发送消息
        for (int i=0;i<5000;i++){
            int num=(int)(Math.random()*7);
            // 指定数据发送到 1 号分区， key 为空 IDEA 中 ctrl + p 查看参数）
            kafkaProducer.send(new ProducerRecord<>(topicName,num,"","test\t" + i), new Callback() {
            // 依次指定 key 值为 a,b,f ，数据 key 的 hash 值与 3 个分区求余，分别发往 1 、 2 、 0
//            kafkaProducer.send(new ProducerRecord<>(topicName,"f","hsq\t" + i), new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e==null){
                        System.out.println("主题："+recordMetadata.topic()+"分区："+recordMetadata.partition());
                    }
                }
            });
            Thread.sleep(2);
        }
//        for (int i=0;i<5;i++){
//            // 指定数据发送到 0 号分区， key 为空 IDEA 中 ctrl + p 查看参数）
//            kafkaProducer.send(new ProducerRecord<>(topicName,0,"","test\t" + i), new Callback() {
//                @Override
//                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
//                    if (e==null){
//                        System.out.println("主题："+recordMetadata.topic()+"分区："+recordMetadata.partition());
//                    }
//                }
//            });
//        }
//        for (int i=0;i<5;i++){
//            // 指定数据发送到 2 号分区， key 为空 IDEA 中 ctrl + p 查看参数）
//            kafkaProducer.send(new ProducerRecord<>(topicName,2,"","test\t" + i), new Callback() {
//                @Override
//                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
//                    if (e==null){
//                        System.out.println("主题："+recordMetadata.topic()+"分区："+recordMetadata.partition());
//                    }
//                }
//            });
//        }
        //5. 关闭资源
        kafkaProducer.close();
    }
}