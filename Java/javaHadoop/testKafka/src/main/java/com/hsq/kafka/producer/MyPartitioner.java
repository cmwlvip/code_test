package com.hsq.kafka.producer;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

public class MyPartitioner implements Partitioner {
    //topic 主题
    //key 消息的 key
    //keyBytes 消息的 key 序列化后的字节数组
    //value 消息的 value
    //valueBytes 消息的 value 序列化后的字节数组
    //cluster 集群元数据可以查看分区信息
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        // 获取消息
        String msgValue = value.toString();
        // 创建 partition
        int partition;
        // 判断消息是否包含test
        if(msgValue.contains("test")){
            partition = 0;
        }else {
            partition=1;
        }
        // 返回分区号
        return partition;
    }
    // 关闭资源
    @Override
    public void close() {

    }
    // 配置方法
    @Override
    public void configure(Map<String, ?> map) {

    }
}
