package com.hsq.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class CustomProducerCustomProducer {
    public static void main(String[] args) {
        //1. 创建kafka 生产者的配置对象
        Properties properties = new Properties();
        String topicName="first";
        //2. 给kafka 配置对象添加配置信息：bootstrap.servers
        //连接集群 bootstrap.servers
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"hsq01:9092");
        // key,value 序列化（必须）：key.serializer，value.serializer
        //指定对应的key和value的序列化类型 key.serializer
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        //properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer .class.getName());//和上面语句等价

        // 设置 acks
        properties.put(ProducerConfig.ACKS_CONFIG , "all");
        // 重试次数 retries ，默认是 int 最大值， 2147483647
        properties.put( ProducerConfig.RETRIES_CONFIG ,3);

        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
        //3. 创建 kafka 生产者对象
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);
        //4. 调用 send 方法发送消息
        for (int i=0;i<5;i++){
            kafkaProducer.send(new ProducerRecord<>(topicName,"test\t"+i));
        }
        //5. 关闭资源
        kafkaProducer.close();
    }
}