package com.qf.kafkaConsumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class CustomConsumer {

    static List<String> flagArr=new ArrayList<>();
    static List<Record> recordArr=new ArrayList<>();
    //日期标识
    static String date="20181001";
    public static void main(String[] args) {
        //配置
        Properties properties=new Properties();

        //连接
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"hsq02:9092");

        //反序列化org.apache.kafka.common.serialization.StringDeserializer
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        //配置消费者组id
        properties.put(ConsumerConfig.GROUP_ID_CONFIG,"test");



        //创建一个消费者
        KafkaConsumer<String,String> kafkaConsumer=new KafkaConsumer<String, String>(properties);

        //订阅主题
        ArrayList<String> topics=new ArrayList<>();
        topics.add("first");
        kafkaConsumer.subscribe(topics);

        //消费数据
        while (true){
            ConsumerRecords<String, String> consumerRecords=kafkaConsumer.poll(Duration.ofSeconds(1));
            for (ConsumerRecord<String,String> consumerRecord:consumerRecords){

                String temp=consumerRecord.value();
                //换天了
                while (!temp.split(" ")[0].equals(date)){
                    //打印前一天结果
                    for (Record record:recordArr){
                        System.out.println(record);
                    }
                    //清空列表
                    flagArr.clear();
                    recordArr.clear();
                    //变更日期为今天
                    date=temp.split(" ")[0];
                }
                //确保标识存在
                if (!flagArr.contains(temp)){
                    flagArr.add(temp);
                    recordArr.add(new Record(temp));
                }
                //遍历记录数组，进行加一
                for (Record record:recordArr){
                    if (record.getFlag().equals(temp)){
                        record.add();
                        break;
                    }
                }
//                System.out.println(consumerRecord);
//                System.out.println(consumerRecord.value());
            }
        }

    }
    static class Record{
        private long sum;
        private String flag;

        public Record(String flag){
            this.flag=flag;
            this.sum=0;
        }

        public String getFlag(){
            return  flag;
        }
        public long getSum(){
            return sum;
        }
        public void add(){
            this.sum+=1;
        }
        //得到记录
        public String getDate(){
            return flag.split(" ")[0];
        }
        public String getAreaId(){
            return flag.split(" ")[1];
        }
        @Override
        public String toString(){
            String[] arr=flag.split(" ");
            return "日期："+this.getDate()+"管辖区："+this.getAreaId()+"过车总数为："+this.getSum()+"每小时过车平均："+this.getSum()/24;
        }
    }


}
