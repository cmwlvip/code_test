package com.qf.cafkaProducerExample;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class IoTDataConsumer {
    private static final String TOPIC_NAME = "iot-data";
    private static List<Data> dataList=new ArrayList<>();

    public static void main(String[] args) throws IOException, ParseException {

        BufferedReader reader = new BufferedReader(new FileReader("D:/0000000/000000_0/000000_0"));
        String line;
        while ((line = reader.readLine()) != null) {
            //对于每行数据，我们使用split()方法将其拆分成设备过车时间、辖区编号。
            String[] parts = line.split("\001");
            String time= parts[7];
            String areaId=parts[55];
            Data data=new Data(time,areaId);
            dataList.add(data);
            System.out.println(data.getTime()+"\t"+data.getAreaId());
        }

        System.out.println("遍历完成");


        dataList.sort(Comparator.comparing(Data::getDate));
//        for (Data data: dataList){
//            System.out.println(data.getTime());
//        }
        // 找到最早的过车时间 yyyyMMddHHmmss
//        Date earliestTime = new Date(Long.MAX_VALUE);
//        for (Data data : dataList) {
//            if (data.getDate().before(earliestTime)) {
//                earliestTime = data.getDate();
//            }
//        }
//        System.out.println("找到的最早时间为:"+earliestTime.toString());
//        System.out.println(dataList.get(0).getTime());
        //1. 创建kafka 生产者的配置对象
        Properties properties = new Properties();
        // 2. 给kafka 配置对象添加配置信息：bootstrap.servers
        //连接集群 bootstrap.servers
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"hsq01:9092");

        // key,value 序列化（必须）：key.serializer，value.serializer
        //指定对应的key和value的序列化类型 key.serializer
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());//和上面语句等价
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());


//        props.put(ConsumerConfig.GROUP_ID_CONFIG, "iot-group");

        //创建 kafka 生产者对象
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);

//        for (int i=0;i<5;i++){
//            kafkaProducer.send(new ProducerRecord<>("first",i+"\t"));
//        }
        for (Data data: dataList){
            String temp=data.getDateString()+" "+data.getAreaId();
            kafkaProducer.send(new ProducerRecord<>("first",temp));
        }
        kafkaProducer.close();

//        //使用subscribe()方法订阅了一个名为iot-data的Kafka主题
//        consumer.subscribe(Collections.singletonList(TOPIC_NAME));


////我们使用Java 8的日期和时间API计算出时间延迟，以便模拟过车时间。
////如果时间延迟大于零，则创建一个Kafka生产者，并将该数据发送到iot-data主题中。
//            LocalDateTime eventTime = LocalDateTime.parse(timestamp, DateTimeFormatter.ISO_DATE_TIME);
//            LocalDateTime now = LocalDateTime.now();
//            Duration delay = Duration.between(eventTime, now);
//            if (delay.getSeconds() > 0) {
//                Producer<String, String> producer = createKafkaProducer();
//                ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC_NAME, deviceId, line);
//                producer.send(record);
//                producer.close();
//            }
//        }

    }
    static class Data{
        //过车时间 [7]
        private String time;

        //辖区编码 [55]
        private String areaId;

        private Date date;

        public Data(String time,String areaId) throws ParseException {
            this.time=time;
            this.areaId=areaId;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = dateFormat.parse(time);
            this.date=date;
        }

        public String getTime(){
            return time;
        }

        public String getAreaId(){
            return areaId;
        }
        public Date getDate(){
            return date;
        }
        public String getDateString(){
            String dateString=time.substring(0,8);
            return dateString;
        }
    }
}
