package com.qf;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
//import org.apache.hadoop.hbase.HColumnDescriptor;//已弃用
//import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;

public class UseHbase {
    public static void createTable(){
        //创建Hbase连接对象
        Configuration conf= HBaseConfiguration.create();
        //获取zookeeper配置
        conf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        conf.set("hbase.zookeeper.quorum", "192.168.56.201,192.168.56.202,192.168.56.203");
        try{
            //建立与Hbase的连接
            Connection conn = ConnectionFactory.createConnection(conf);
            Admin admin=conn.getAdmin();
            //设置表名称
            TableName tableName=TableName.valueOf("transport");
            System.out.println("连接：" + conn + "-HMaster:"+admin);
            //判断表是否存在，如果存在就删除
            if(admin.tableExists(tableName)){
                if(admin.isTableEnabled(tableName)){
                    admin.disableTable(tableName);
                }
                admin.deleteTable(tableName);
            }
            //创建HTableDescriptor对象，并添加表名称
            //`org.apache.hadoop.hbase.HTableDescriptor' 已被弃用
//        HTableDescriptor table= new HTableDescriptor(tableName);
            //创建HColumnDescriptor对象，并添加列簇名称
            //'org.apache.hadoop.hbase.HColumnDescriptor' 已被弃用
//        HColumnDescriptor cf1=new HColumnDescriptor("cf1");
//        HColumnDescriptor cf2=new HColumnDescriptor("cf2");
            ColumnFamilyDescriptor columnFamily1= ColumnFamilyDescriptorBuilder
                    .newBuilder(Bytes.toBytes("cf1"))
                    .build();
            ColumnFamilyDescriptor columnFamily2=ColumnFamilyDescriptorBuilder
                    .newBuilder(Bytes.toBytes("cf2"))
                    .build();
            TableDescriptor table=TableDescriptorBuilder
                    .newBuilder(tableName)
                    .setColumnFamily(columnFamily1)
                    .setColumnFamily(columnFamily2)
                    .build();
            //创建表
            admin.createTable(table);
            TableName[] tableNames=admin.listTableNames();
            //查看所有的表
            for (TableName tablesName:tableNames){
                System.out.println(tablesName);
            }
            conn.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void addData(){
        //获取Hadoop相关配置
        Configuration hadoopConf = new Configuration();
        hadoopConf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        Configuration conf = HBaseConfiguration.create();
        //获取zookeeper配置
        conf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        conf.set("hbase.zookeeper.quorum", "192.168.56.201,192.168.56.202,192.168.56.203");
        try{
            FileSystem fs=FileSystem.get(hadoopConf);
            Path path=new Path("/transport/transport.csv");
            FSDataInputStream in =fs.open(path);
            BufferedReader br=new BufferedReader(new InputStreamReader(in,"GBK"));
//            BufferedReader br=new BufferedReader(new InputStreamReader(in));
            String line;
            // 配置HBase连接
            Connection conn = ConnectionFactory.createConnection(conf);
            Table table = conn.getTable(TableName.valueOf("transport"));
            br.readLine();
            int n=1;
            while((line=br.readLine())!=null){
                String[] arr=line.split(",");
                // 创建一个Put实例
                Put put = new Put(Bytes.toBytes(String.valueOf(n)));
                // 添加列族和列名、值
                put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("roadId"), Bytes.toBytes(arr[0]));
                put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("deviceId"), Bytes.toBytes(arr[1]));
                put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("deviceName"), Bytes.toBytes(arr[2]));
                put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("departmentId"), Bytes.toBytes(arr[3]));
                put.addColumn(Bytes.toBytes("cf1"), Bytes.toBytes("departmentName"), Bytes.toBytes(arr[4]));

                put.addColumn(Bytes.toBytes("cf2"), Bytes.toBytes("x"), Bytes.toBytes(arr[5]));
                put.addColumn(Bytes.toBytes("cf2"), Bytes.toBytes("y"), Bytes.toBytes(arr[6]));
                put.addColumn(Bytes.toBytes("cf2"), Bytes.toBytes("time"), Bytes.toBytes(arr[7]));
                try{
                    put.addColumn(Bytes.toBytes("cf2"), Bytes.toBytes("other"), Bytes.toBytes(arr[8]));
                } catch (Exception e){
                    put.addColumn(Bytes.toBytes("cf2"), Bytes.toBytes("other"), Bytes.toBytes("null"));
                }
                table.put(put);
                n++;
                System.out.println(arr[0]+"\t"+arr[1]+"\t"+arr[2]+"\t"+arr[3]+"\t"+arr[4]+"\t"+arr[5]+"\t"+arr[6]+"\t"+arr[7]);
            }
            table.close();
            conn.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void readData(){
        //获取Hadoop相关配置
        Configuration hadoopConf = new Configuration();
        hadoopConf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        Configuration conf = HBaseConfiguration.create();
        //获取zookeeper配置
        conf.set("fs.defaultFS", "hdfs://192.168.56.201:9000");
        conf.set("hbase.zookeeper.quorum", "192.168.56.201,192.168.56.202,192.168.56.203");
        try{
            // 配置HBase连接
            Connection conn = ConnectionFactory.createConnection(conf);
            //获取表对象
            Table table = conn.getTable(TableName.valueOf("transport"));
            //创建Scan对象
            Scan scan=new Scan();
            //通过扫描器得到结果集
            ResultScanner rs =table.getScanner(scan);
//            //得到迭代器
//            Iterator<Result> it= rs.iterator();
//            printData(it);
            printNeedData(rs, "cf2".getBytes(),Bytes.toBytes("time"));
            table.close();
            conn.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    //迭代输出每行的所有数据
    public static void printData(Iterator<Result> it){
        while (it.hasNext()){
            Result next=it.next();
            List<Cell> cells=next.listCells();
            for (Cell cell:cells){
                String row=Bytes.toString(CellUtil.cloneRow(cell));
                String cf=Bytes.toString(CellUtil.cloneFamily(cell));
                String qualifier=Bytes.toString(CellUtil.cloneQualifier(cell));
                String value=Bytes.toString(CellUtil.cloneValue(cell));
                System.out.println(row+","+cf+":"+qualifier+","+value);
            }
        }
    }
    //输出指定簇、列
    public static void  printNeedData(ResultScanner rs,byte[] columnFamily, byte[] qualifier) throws IOException {
        for (Result result = rs.next(); result != null; result = rs.next()) {
            String date = new String(result.getValue(columnFamily, qualifier));
            System.out.println(date);
        }
    }
    public static void main(String[] args) {
//        createTable();
//        addData();
        readData();
    }
}