package acwing01;

import java.util.Scanner;

public class Knapsack {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int V = cin.nextInt();
        int[] v = new int[N + 1];
        int[] w = new int[N + 1];
        for (int i = 1; i < N + 1; i++) {
            v[i] = cin.nextInt();
            w[i] = cin.nextInt();
        }
        int[][] cell = new int[N + 1][V + 1];
        for (int i = 1; i < N + 1; i++) {
            for (int j = 1; j < V + 1; j++) {
                if (j >= v[i]) {
                    cell[i][j] = Math.max(cell[i - 1][j - v[i]] + w[i], cell[i - 1][j]);
                } else {
                    cell[i][j] = cell[i - 1][j];
                }
            }
        }
        System.out.println(cell[N][V]);
    }
}
